# NEWS

## x.x.x (unreleased)

This section holds an overview about unreleased updates/features/fixes and is not meant to be
merged into master.
Please pick the staged features that are selected for release and create an appropriate release entry.


## 2.6.1 (2025-02-15)

- Fixed missing tenant roles in contact role select.
- Use role name as fallback if description is empty.
- Fixed organization admin cannot update his own organizations.

## 2.6.0 (2025-02-18)

### Improvements

- Possibility to limit allowed contacts' email domains for an organisation.
- Descriptions for roles when editing a contact.
- Ability to moving contacts between organisations.
- New filtering options in "My Node"
- Improvements in network rules handling.

### Fixes

- Fixed phone validation regex.
- Fixed clearing fields on role changes.
- Fixed contact export compatibility with Excel format.
- Other fixes.

## 2.5.0 (2024-11-25)

### Improvements
- Netobject rules: applied UI improvements, added identifier and comments as optional fields.
- Applied classification_identifier as optional intelmq query paramter.
- Configurable audit log output channels. (database, stdout, stderr)

### Fixes

- Allow query value 'any' as rule match for classification and feed properties in intelmq lookup.
- Added missing tenant role list update for preselected organisations in new contacts.
- Fixed /intelmq/lookup with missing feed_name query param.
- Fixed non updating organisation search in netobject create.

### Other

- Cypress integration in development and CI setup.
- Implementation of E2E tests for general application functionality.
- Dependency updates.

## 2.4.0 (2024-07-30)

- Introduction of restricted admin contacts - currently only for portal admins.
- Correction of sensitive line feed check on OpenPGP public key and S/MIME certificate upload.
- Added auto approval on notobject create for roles portal- and tenantadmin.
- Applied organisation name and role list to contact export.
- Applied autocomplete search for taxonomy and feed selection in netobject rules.

## 2.3.0 (2024-07-24)

- Hierarchical classification and feed selection.
- Applied the saving of feed codes and either-or alternative to feed name in intelmq lookup.
- Integrated configuration of interval and feed states for netobject rules.
- Integrated netobject claim by organisation for portal and tenant admins.
- Integrated ripe data conflict check and display on claim resolution.
- Allowed netobject claim on suborganisations for organisation admins.
- Applied ripe data import mechanism to dev-util script.
- Keycloak setup and client package update to latest version 25.0.1
- Fallback to display contacts mail address if no name or label attributes are provided.
- Applied config option to enable/disable local ripe data in intelmq lookup queries.

> :warning: **The database migrations will update existing records for `classification_type` and `feed_name`!**

#### Update instructions

Due to the addition of foreign keys for the tables (`feed_provider 1:n feed_name` / `classification_taxonomy 1:n classification_type`),
the application checks whether entries already exist for the n-side of the relations when executing the migrations.
If this is the case, a category with the value `'unknown'` is automatically created on the 1-side to which all existing entries are assigned.
A manual update of the foreign keys in the respective tabs of the 1-side is therefore required for the correct mapping of the relations.

## 2.2.0 (2024-07-01)

### [PR 43](https://gitlab.com/intevation/tuency/tuency/-/merge_requests/43)

- Fixed tenant role mapping by resolving contacts organisation tenants on backend
- Fixed inconsitent pagination search labels by resolving text over type property
- Moved contact expiry check to command and applied config to control its scheduler with cron syntax
- Fix for php-unit-tests ci job and documentation updates

### [PR 42](https://gitlab.com/intevation/tuency/tuency/-/merge_requests/42)

- Added contact role configuration per tenant/constituency.
- Applied missing role filter to contact export.
- Applied full name search to contact text filter.
- Updated deployment example and docs.
- Applied filter for expired contacts and marking them with a label in main and detail view.

### [PR 41](https://gitlab.com/intevation/tuency/tuency/-/merge_requests/41)

- Applied freetext search for organisation names (Main and parent level)
- Contact expiry notifications are sent per organization admin and contain information on which organization,
how many contacts and which roles are affected.

### [PR 40](https://gitlab.com/intevation/tuency/tuency/-/merge_requests/40)

- Fixed orga search for items that are not on the current list.
- Applied option to only confirm contact data if inputs have not been touched.
- Implemented notification delay for reminder mails and email verified check
- Applied non-editable contact view and sharable contact links
- Fixed deprecated string variable style in migrations
- Switching from maildev to mailhog for local smtp testing (incompatibility docker version > v26.0.0)
- Fixed deprecated string variable style in migrations
- Included config and database folder for php style check and fixed errors

## 2.1.0 (2024-03-18)

- Fixing missing username/fullname display in top menu bar.
- Checked and documented required viewPort attribute for SVGs in client themes.
- Applied missing tuency managemnt client in backend .env examples.
- Applied configurable global admin mail contact.
- Reworked general mail notification structure and added claim create notification.
- Fixing CSP headers in local environment for yarn development.
- Applied role filter to contact overview.
- Implemented searchable dropdowns for dynamic contact fields.
- Integrated organisation links for contact and netobject quick access.
- FE hotfix to allow PKCS7 S/MIME Certs.
- Adding customizable notification mail subjects.
- Restructuring and cleanup of the tuency laravel config.
- Made page main title configurable in client-config.js and adapted page title display
- Cleaned up unused old configs from keycloak client plugin
- Fixed: Always initialize recipients with admin mail on claim create
- Replace cron with entrypoint script to preserve logs on the Docker output.
- Updating keycloak setup and client package to latest version 24.0.1.
- Organisation overview presentation and usability improvements.
- Adapted claimobject routing and organisation links to use query params.

## 2.0.1 (2024-03-05)

Fixed release pipeline.

## 2.0.0 (2024-03-05)

This major release contains the following updates:
- Dockerized development setup
- Keycloak update to v22.04
- Change of the OICD-Auth integration by officially recommended node and laravel packages
- Replacement of scito keycloak admin packages to own oicd service implementation
- Removal of openresty and lua based authentication
- Laravel Upgrade to v10.x
- PDF upload filename randomization and extension check
- Security header adaptions
- Integration and switch Laravel AuditLog
- Automated docker image builds over Gitlab CI

Due to breaking changes in the general setup, as well as in the authentication and logging mechanism, it is not recommended to update an existing setup from v1.x to v2.x.
The most convenient way to upgrade is to deploy a new instance of Tuency v2.x and perform a data migration.

In the [/deployment/README.md](./deployment/README.md) you can find intructions to deploy a production ready Tuency instance.

### Keycloak extensions

In the previous versions, a separate logging and keycloak api were maintained as java extensions in addition to the keycloak tuency theme.
These currently still exist in the repository, but are not yet adapted to the current Keycloak version. This results in the following restrictions:

| Extension                                    | Description                                                                      |
|----------------------------------------------|----------------------------------------------------------------------------------|
| [keycloak_theme](./keycloak_theme/README.md) | The login ui works as usual. The adaptation of the account ui to the Keycloak API Changes > v18.0 is currently still open, but is planned for an update at a later date. For the time being, the default Keycloak account ui must therefore be used. |
| [keycloak_api](./keycloak_api/README.md) | It is checked whether the theming of the clients can be integrated elsewhere. The use of the extension is not yet planned and documented. |
| [keycloak_api](./keycloak_logging/README.md) | It is checked whether the relevant logs (user account changes) can be collected via stdout/stderr. The use of this extension is currently not planned. |

## 1.1.0 (2021-07-28)

### Upgrading from 1.0.0

General upgrade instructions are in the various `README.md` files.

#### PHP backend

##### Dependencies

The `scito/keycloak-admin` has been updated to a new revision of the
`dev-execute-actions-email` branch, so a composer `install` is required.

##### Configuration

###### Auditlog

Tuency now logs requests and, optionally, changes. Both are written to
the log channel `auditlog` which can be configured in
`backend/config/logging.php`. Changes are also logged into a database
table.

By default no changes are logged. Which changes are to be logged and in
what details can be configured in `backend/config/tuency.php` where the
`auditlog` setting determines the tables and columns whose changes
should be logged.

Tuency also records which user last change a row in the database in many
tables. This can be switched on/off with the `set_updated_by` settging
in `backend/config/tuency.php`.

There's a related extension for Keycloak, see below.

##### Database

This upgrade has several database migrations, two of which may need some
manual action:

###### User information

How user information is stored in the PostgreSQL database has changed
and while the migration takes care of most of the required changes,
there's one aspect it cannot handle: Users that are in Keycloak but have
never been added to the PostgreSQL database need to be added manually.
This can be done with the new artison console command
`tuency:compare-users`. Run without arguments to list the users which are
in PostgreSQL or in Keycloak but no the other:

```sh
php artisan tuency:compare-users
```

Run it once with the `--add-to-db` option to add the users that are in
Keycloak but not yet the PostgreSQL database to the PostgreSQL database:

```sh
php artisan tuency:compare-users --add-to-db
```

###### FQDN constraints

The database has an additional constraint on FQDNs. The FQDNs must now
be lower case. The migration converts existing data but after conversion
the already existing uniqueness constraint might be violated. A typical
error message when this happens is

    duplicate key value violates unique constraint "fqdn_organisation_id_fqdn_unique"

To determine whether your database is affected or to determine which
tuples violate the constraint, you can use:

```sql
SELECT array_agg(ROW(fqdn, fqdn_id)) FROM fqdn
 GROUP BY lower(fqdn), organisation_id
HAVING count(*) > 1;
```

The easiest way to resolve this is to delete one of the duplicate FQDNs.
Since tuency is not used in production yet, deleting items should not be
much of a problem. Deleting them using the web-interface is probably
best, because that way any related information will automatically be
deleted as well.


#### Client

##### Dependencies

Many dependencies have been updated.


#### Keycloak

There's a new extension for Keycloak in `keycloak_logging/` that handles
the Keycloak part of the audit log. See the `README.md` in that
subdirectory for details.


#### Updating running docker deployment

Due to changes to the backend start script, an update of running backend containers is required.
To upgrade the script in a running container, e.g. `tuency_backend`, use the following commands:

```bash
cd /path/to/tuency/repo/
docker cp deployment/backend/start.sh tuency_backend:/usr/src/tuency-installer/
```

Additionally a keycloak logging extension has been added which needs to be deployed into the keycloak container.
To add the extension to a keycloak container, e.g. tuency_keycloak, use the following commands:

```bash
cd /path/to/tuency/repo/
docker cp keycloak_logging tuency_keycloak:/usr/src/tuency-logging
docker exec -ti tuency_keycloak bash
cd /usr/src/tuency-logging
mvn clean package
cp target/tuency-keycloak-logging-extension-jar-with-dependencies.jar /opt/keycloak-11.0.3/standalone/deployments/
```

The extension should then be loaded automatically.
For further configuration, see the [logging extension readme file](keycloak_logging/README.md).



## 1.0.0 (2021-06-17)

### Upgrading from 0.9.0

General upgrade instructions are in the various `README.md` under
`deployment/`.


#### PHP backend

##### New dependency

The backend now requires the PHP module bcmath (Debian package:
php-bcmath)

The Docker files have been updated.

##### Database

This update contains some database migrations, none of which modify or
delete existing data, so applying them should not be a problem.

##### Email

The PHP backend will now send mails in some cases. This requires some
configuration, which is described in the section *Mail Configuration* in
`backend/README.md`. See also *Customizing email notifications* in
`deployment/backend/README.md`.


#### RIPE Importer

The RIPE importer has to be updated and invoked with the right options
to support tuency fully. See the section *RIPE Data* in
`backend/README.md`.


#### nginx configuration

The files used with nginx have been updated. Particularly noteworthy are
the following files:

 - `deployment/webserver/authenticate.lua`
 - `deployment/webserver/check_auth.lua`
 - `deployment/webserver/nginx.conf`
 - `deployment/webserver/tuency_server_block`

The docker files have been updated. See section *Update* in
`deployment/webserver/README.md`.
