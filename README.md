# Tuency

A web application helping CERTs to enable members of their constituency
to self-administrate how they get warnings related to their network objects
(IP addresses, IP ranges, autonomous systems, domains).

Tuency will enable [IntelMQ](https://github.com/certtools/intelmq/)
to access the contact data and send out warnings
and that a list is exported for people to use the contact information.

Tuency is part of the
[IntelMQ Ecosystem](https://intelmq.readthedocs.io/en/maintenance/user/ecosystem.html#constituency-portal-tuency)
as successor of https://github.com/certat/do-portal
see
[CERT.at's Blogentry 2020-10-27](https://cert.at/en/blog/2020/10/development-of-the-constituency-portal-20)
for more background.

Status: feature-complete
(with version 1.1 having successfully concluding the initial development
 contract and handover to nic.at.)

## Setup Documentation

CERT.at's user documentation for their instance [explains the concepts of the software](https://tuency.cert.at/docs/02_concept/).

### Development

The local development setup is provided as an all-in-one docker compose project.
Refer to the [Tuency development guide](./docs/DEVELOPMENT.md) for installation instructions.

### Production

A set of Docker Compose projects and service configuration instructions are available for a sample production setup.
See [Example Production Deployment](deployment/README.md) for further information.

## Screenshots

![Netobjects](docs/images/netobjects.png)

![Claim resolution](docs/images/claim_resolution.png)

![Organisations](docs/images/organisations.png)

![Contacts](docs/images/contacts.png)

![My node](docs/images/mynode.png)

![Tags](docs/images/tags.png)

## License

Tuency as complete package is Free Software
under GNU Affero General Public License v >= 3.0
without warranty.

```
SPDX-License-Identifier: AGPL-3.0-or-later

SPDX-FileCopyrightText: 2021 nic.at GmbH <https://nic.at>
Software-Engineering: 2021 Intevation GmbH <https://intevation.de>
```

Tuency contains third party Free Software components under compatible licenses,
please see the README.md files and the files itself for more details.

This project was partially funded by the CEF framework
![Co-financed by the Connecting Europe Facility of the European Union](docs/cef_logo.png)
