# README for the backend of tuency

## General

The main development setup is documented in the projects [README](../README.md).
For interacting with the backend containers composer and artisan tools please read the instructions for the [dev-util](../docs/dev-util.md) script.

### Configuration of contact roles and their fields

The contact roles are configured in [config/tuency.php](config/tuency.php). The
instructions for this are also there.

### Mail Configuration

Using the `.env` file the mail server used for serving notifications or reminder mails can be configured.
The development setup (see `.env.example`) sends mails per default to mailhog, a smtp mailtrap service included in the compose project.
The respective entries are:

```bash
MAIL_MAILER=...
MAIL_HOST=...
MAIL_PORT=...
MAIL_USERNAME=...
MAIL_PASSWORD=...
MAIL_ENCRYPTION=...
MAIL_FROM_ADDRESS=...
MAIL_FROM_NAME=...
```

#### Contact expiry reminder

Contact expiry reminders are an automated workflow that notifies administrators about outdated contact information
within the organisations they manage.  
The following configuration options are available:

```bash
# Defines when and how often the reminder
# workflow is triggered as cron schedule.
# (Default: every day at 7 AM)
REMINDER_EXECUTION_CRON="0 7 * * *"

# Day interval when contacts need to be updated.
# (Admins will be notified about expired contacts)
MAIL_CONTACT_EXPIRE_INTERVAL=1

# Day interval when expiry notifications are
# resent.
MAIL_CONTACT_NOTIFICATION_INTERVAL=3
```

To activate the reminder execution (driving the laravel scheduler) a periodic job is required.  
Please note that this job is not triggered automatically in the development setup.  
For local / development run, look at the solutions below. To use the scheduler in a productive environment (separate container)
refer to the [backend deployment example](../deployment/backend/docker-compose.yml).

#### Call the reminder command manually

```bash
# Executes the reminder workflow once.
docker-compose exec -it -u www-data backend bash -c "php artisan tuency:check-reminder"
```

#### Call the scheduler manually

```bash
# Executes the scheduler once and triggers the reminder workflow
# if it matches the REMINDER_EXECUTION_CRON config.
docker-compose exec -it -u www-data backend bash -c "php artisan schedule:run"
```

#### Use host cron

```bash
# Executes the scheduler periodically and triggers the reminder workflow
# if it matches the REMINDER_EXECUTION_CRON config.
* * * * * /usr/bin/docker compose -f /<tuency-backend-path>/docker-compose.yml exec -T -u www-data backend bash -c "php artisan schedule:run >> /dev/null 2>&1"
```


### Mail Templates

Mails will be sent using blade templates located at `resources/views/emails`.
As the mails are customized per constituency, each constituency needs its own set of mail templates.
E.g. mails for the constituency "example" will be sent using the templates `configreminder_example.blade.php`, `claimupdated_example.blade.php` etc.
If the constituency name contains `.` characters, these need to be replaced by `_`, e.g.:
Mails for the constituency `example.org` will be sent using the template `configreminder_example_org.blade.php`.

### Mail Subjects

The notification mail subjects can be adapted in the backends Tuency configuration [./config/tuency.php](./config/tuency.php).

## Debugging

 * The development server logs to `storage/logs/laravel.log`.

 * If `APP_DEBUG` is `true`, when errors occur, the HTTP response
   includes some error information such as PHP tracebacks.

## RIPE Data

Tuency can work with data imported into its database from RIPE with the
ripe importer from
[intelmq-certbund-contact](https://github.com/Intevation/intelmq-certbund-contact).
The RIPE data has to be imported into the same database that tuency uses
for the other netobjects, rules, etc. because the RIPE data can be
associated with the data managed by the users of tuency.


At the time of writing, tuency requires changes to the importer that
have not been included in a release, but which are available in the
`wip-import-routes` branch. In addition to the documentation that comes
with the importer on how to use it, the following tuency specific
information is necessary:

### Route Information

Run the importer with the `--import-route-data` option, because tuency
makes use of the routing information from RIPE and it's not imported by
default.

### Cleanup of RIPE related data

Run the importer with the following option:

    --before-commit-command='SELECT tuency_ripe_cleanup();'

The database function `tuency_ripe_cleanup` performs some cleanup of
data that is managed by tuency and relates to data updated by the
importer.


## LICENSE

The initial version of the backend code was created with `laravel new`.
Laravel's license is identical to the expat license which the FSF
considers GNU GPL compatible, so it should be fine using this as a starting
point for software licensed under GNU AGPL.

The text of Laravel's license can be found in `../LICENSES/MIT.txt`,
substituting "Taylor Otwell" as copyright holder.
