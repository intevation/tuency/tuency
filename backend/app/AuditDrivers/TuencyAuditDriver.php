<?php

namespace App\AuditDrivers;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use OwenIt\Auditing\Contracts\Audit;
use OwenIt\Auditing\Contracts\Auditable;
use OwenIt\Auditing\Drivers\Database;
use OwenIt\Auditing\Exceptions\AuditingException;
use OwenIt\Auditing\Models\Audit as DefaultAuditModel;

/**
 * Extends the existing database audit driver to also trite
 */
class TuencyAuditDriver extends Database
{
    /**
     * Perform an audit.
     *
     * @param Auditable $model
     *
     * @return Audit
     * @throws AuditingException
     */
    public function audit(Auditable $model): Audit
    {
        $implementation = Config::get('audit.implementation', DefaultAuditModel::class);

        foreach (config('audit.drivers.tuency.channels', []) as $auditChannel) {
            if ($auditChannel === 'stdout') {
                $this->writeStdOut(json_encode($model->toAudit()));
            }

            if ($auditChannel === 'stderr') {
                $this->writeStdErr(json_encode($model->toAudit()));
            }

            if ($auditChannel === 'database') {
                $implementation = parent::audit($model);
            }
        }

        return new ($implementation);
    }

    /**
     * Remove older audits that go over the threshold.
     *
     * @param Auditable $model
     *
     * @return bool
     */
    public function prune(Auditable $model): bool
    {
        if (in_array('database', config('audit.drivers.tuency.channels', []))) {
            return parent::prune($model);
        }

        return false;
    }

    /**
     * Writes a line to php stdout.
     *
     * @param string $line
     * @return void
     */
    private function writeStdOut(string $line): void
    {
        $this->writeToStream('php://stdout', $line);
    }

    /**
     * Writes a line to php stderr.
     *
     * @param string $line
     * @return void
     */
    private function writeStdErr(string $line): void
    {
        $this->writeToStream('php://stderr', $line);
    }

    /**
     * Writes a new line to an output stream.
     *
     * @param string $stream
     * @param string $line
     * @return void
     */
    private function writeToStream(string $stream, string $line): void
    {
        $out = fopen($stream, 'w');
        fwrite($out, $this->toSyslogString($line) . PHP_EOL);
        fclose($out);
    }

    /**
     * Creates a standard syslog sting and appends the message.
     *
     * @param string $message
     * @return string
     */
    private function toSyslogString(string $message): string
    {
        $timestamp = Carbon::now()->format('M d Y H:i:s');
        $hostname = $_SERVER['HTTP_HOST'];
        $facility = "Tuency-Audit";
        return $timestamp . ' ' . $hostname . ' ' . $facility . ' ' . $message;
    }
}
