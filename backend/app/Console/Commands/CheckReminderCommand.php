<?php

namespace App\Console\Commands;

use App\Http\Controllers\MailController;
use Illuminate\Console\Command;

class CheckReminderCommand extends Command
{
    /**
     * @var MailController
     */
    private MailController $mailController;
    public function __construct()
    {
        parent::__construct();
        $this->mailController = new MailController();
    }

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tuency:check-reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Executes the reminder workflow for expired contacts.';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->mailController->checkForReminder();
    }
}
