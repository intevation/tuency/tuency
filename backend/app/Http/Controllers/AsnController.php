<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2021 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2021 Intevation GmbH <https://intevation.de>
 *
 * Author: 2021 Bernhard Herzog <bernhard.herzog@intevation.de>
 */

namespace App\Http\Controllers;

use App\Models\Asn;
use App\Models\Organisation;
use App\Models\OrganisationAutomatic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class AsnController extends Controller
{
    use HandlesClaims;

    /**
     * List the ASNs associated with an organisation
     */
    public function index(Organisation $organisation)
    {
        $this->logRequest();

        return $organisation->asns;
    }

    /**
     * Create a new ASN associated with an organisation.
     */
    public function store(Request $request, Organisation $organisation)
    {
        $validated = $request->validate(['asn' => 'int']);

        $this->logRequest($validated);

        $asn = Asn::create([
            'asn' => $validated['asn'],
            'organisation_id' => $organisation->organisation_id,
            'approval' => $this->getInitialApprovalState(),
        ]);

        $this->notifyNetobjectCreate($organisation, ['type' => 'ASN'], $asn->approval);

        return $asn;
    }

    /**
     * Update an ASN.
     *
     * This is only relevant for the approval process, so the only field that
     * can be changed is 'approval' and only tenant- and portaladmins are
     * allowed to change it.
     */
    public function update(Request $request, Organisation $organisation, Asn $asn)
    {
        return $this->updateNetObject($request, $organisation, $asn);
    }

    public function getConflictingNetObjects(ASN $asn)
    {
        return Asn::whereKeyNot($asn)
            ->where('organisation_id', '<>', $asn->organisation_id)
            ->where('asn', $asn->asn)
            ->where('approval', '=', 'approved')
            ->with('organisation')
            ->get();
    }

    /**
     * Remove an asn
     */
    public function destroy(Organisation $organisation, Asn $asn)
    {
        $this->logRequest();
        $asn->delete();
    }

    /**
     * Return claimed ASNs that conflict with a given ASN claim.
     *
     * Two ASN claims conflict with one another if the refer to the same ASN.
     */
    public function conflicts(Organisation $organisation, Asn $asn)
    {
        Gate::authorize('manage-claims');

        $this->logRequest();

        // The route does not use scoped resolution, so we have to check this
        // explicitly.
        abort_unless($asn->organisation_id === $organisation->getKey(), 404);

        $asns = Asn::whereKeyNot($asn)
            ->where('asn', $asn->asn)
            ->where('approval', '<>', 'denied')
            ->with('organisation.tenants')
            ->get();

        return [
            'tuency' => $this->processConflicts($asns, 'asn'),
            'ripe' => $this->getRipeConflicts($asn->asn)
        ];
    }

    /**
     * Queries ripe data for conflicting asn.
     *
     * @param string $asn
     * @return array
     */
    private function getRipeConflicts(string $asn): array
    {
        return OrganisationAutomatic::forAsn($asn)
            ->get()
            ->toArray();
    }
}
