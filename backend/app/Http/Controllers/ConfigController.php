<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2020 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2020 Intevation GmbH <https://intevation.de>
 *
 * Author: 2021 Magnus Schieder <magnus.schieder@intevation.de>
 */

namespace App\Http\Controllers;

use App\Models\ClassificationTaxonomy;
use App\Models\FeedProvider;
use App\Services\TuencyConfigService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class ConfigController extends Controller
{
    private TuencyConfigService $configService;
    private $contactRoles;
    private $contactRoleMapping;
    private $netobjectRules;

    public function __construct(TuencyConfigService $configService)
    {
        $this->configService = $configService;

        $contactRoles = $this->configService->getContactRolesByLoggedInUser();
        if ($contactRoles === null) {
            Log::error("tuency.contact_roles not set");
            abort(500);
        }

        $contactRoleMapping = $this->configService->getContactRoleMapping();
        if ($contactRoleMapping === null) {
            Log::error("tuency.tenant_contact_roles not set");
            abort(500);
        }

        $contactFilds = config('tuency.contact_fields');
        if ($contactFilds === null) {
            Log::error("tuency.contact_fields not set");
            abort(500);
        }

        $netobjectRules = config('tuency.netobjects.rules');
        if ($netobjectRules === null) {
            Log::error("tuency.netobjects.rules not set");
            abort(500);
        }

        $roleForms = [];
        foreach ($contactRoles as $roleName => $roleConfig) {
            $roleFromFields = [];
            foreach ($roleConfig as $fieldsName => $required) {
                if ($fieldsName == 'description') {
                    continue;
                }
                $fields = [
                    'name' => $fieldsName,
                    'label' => $contactFilds[$fieldsName]['label'],
                    'required' => $required,
                ];
                if (array_key_exists('type', $contactFilds[$fieldsName])) {
                    $fields['type'] = $contactFilds[$fieldsName]['type'];
                }
                if (array_key_exists('hint', $contactFilds[$fieldsName])) {
                    $fields['hint'] = $contactFilds[$fieldsName]['hint'];
                }
                if (array_key_exists('dropdown', $contactFilds[$fieldsName])) {
                    $dropdownFields = [];
                    foreach ($contactFilds[$fieldsName]['dropdown'] as $value => $label) {
                        array_push(
                            $dropdownFields,
                            [
                                'label' => $label,
                                'value' => $value,
                            ]
                        );
                    }
                    $fields['dropdown'] = $dropdownFields;
                }
                array_push($roleFromFields, $fields);
            };
            $roleForm = [
                'name' => $roleName,
                'fields' => $roleFromFields,
            ];

            if (array_key_exists('description', $roleConfig)) {
                $roleForm['description'] = $roleConfig['description'];
            }

            array_push($roleForms, $roleForm);
        };

        $this->contactRoles = $roleForms;
        $this->contactRoleMapping = $contactRoleMapping;
        $this->netobjectRules = $netobjectRules;
        $this->netobjectRules['classifications'] = ClassificationTaxonomy::with('types')->get()->toArray();
        $this->netobjectRules['feed']['providers'] = FeedProvider::with('feeds')->get()->toArray();
    }

    /**
     * Return basic config and user values (to use on our SPA).
     */
    public function index()
    {
        $user = Auth::user();

        return [
            "username" => $user->getUsername(),
            "roles" => $user->getGroups()[0],
            "groups" => $user->resolveGroups(),
            "fullname" => $user->getFullname(),
            "contact_roles" => $this->contactRoles,
            "contact_role_mapping" => $this->contactRoleMapping,
            "netobject_rules" => $this->netobjectRules
        ];
    }
}
