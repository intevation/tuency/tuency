<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2020, 2021 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2021 Intevation GmbH <https://intevation.de>
 *
 * Author: 2021 Bernhard Herzog <bernhard.herzog@intevation.de>
 */

namespace App\Http\Controllers;

use App\Models\Organisation;
use App\Models\Fqdn;
use App\Models\FqdnRule;
use App\Http\Requests\RuleFormRequest;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\DB;

class FqdnRuleController extends Controller
{
    /**
     * Retrieve all rules associated with an FQDN.
     */
    public function index(Organisation $organisation, Fqdn $fqdn)
    {
        $this->logRequest();

        return $fqdn->rules()->with([
            'classificationTaxonomy',
            'classificationType',
            'feedProvider',
            'feedName',
            'contacts',
        ])->orderBy('fqdn_rule_id')->get();
    }

    /**
     * Create a new rule associated with a fqdn
     */
    public function store(
        RuleFormRequest $request,
        Organisation $organisation,
        Fqdn $fqdn
    ) {
        $validated = $request->validated();
        $validated['fqdn_id'] = $fqdn->fqdn_id;

        $this->logRequest($validated);

        return DB::transaction(function () use ($validated) {
            $rule = FqdnRule::create($validated);

            if (array_key_exists('contacts', $validated)) {
                $rule->contacts()->sync($validated['contacts']);
            }

            return $rule;
        });
    }

    /**
     * Retrieve a specific rule
     */
    public function show(
        Organisation $organisation,
        Fqdn $fqdn,
        FqdnRule $rule
    ) {
        $this->logRequest();
        return $rule;
    }

    /**
     * Update a rule.
     */
    public function update(
        RuleFormRequest $request,
        Organisation $organisation,
        Fqdn $fqdn,
        FqdnRule $rule
    ) {
        $validated = $request->validated();

        $this->logRequest($validated);

        return DB::transaction(function () use ($rule, $validated) {
            $rule->update($validated);

            if (array_key_exists('contacts', $validated)) {
                $rule->contacts()->sync($validated['contacts']);
            }

            return $rule;
        });
    }

    /**
     * Remove a rule.
     */
    public function destroy(
        Organisation $organisation,
        Fqdn $fqdn,
        FqdnRule $rule
    ) {
        $this->logRequest();
        $rule->delete();
    }
}
