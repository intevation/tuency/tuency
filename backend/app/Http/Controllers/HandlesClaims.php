<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2021 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2021 Intevation GmbH <https://intevation.de>
 *
 * Author: 2021 Bernhard Herzog <bernhard.herzog@intevation.de>
 */

namespace App\Http\Controllers;

use App\Models\Organisation;
use App\Models\Tenant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;

trait HandlesClaims
{
    /**
     * Notifies tenant admins and global mail contact about a new claim create.
     *
     * @param Organisation $organisation
     * @param array $mailPayload
     * @param string $approval
     * @return void
     */
    public function notifyNetobjectCreate(
        Organisation $organisation,
        array $mailPayload,
        string $approval = "pending"
    ): void {
        // Suppress claim notification if already approved.
        if ($approval === "approved") {
            return;
        }

        $mailController = new MailController();

        foreach ($organisation->tenants as $tenant) {
            $mailController->notifyClaimCreate($tenant, $mailPayload);
        }
    }

    /**
     * Handle claim updates
     *
     * Updates only affect the approval status. Portal- and tenant-admins may
     * set it to any of the allowed values. Orgaadmins can change it to
     * 'pending' so that they can re-claim a netobject when the claim was
     * denied for some reason.
     *
     * This method handles all the processing for an update of a netobject
     * including validating and logging (logging assumes that the class has a
     * logRequest method) the request and performing the update and sending
     * notification mails.
     *
     * Classes using this trait will usually only have to delegate to this
     * method in their update method.
     */
    public function updateNetObject(
        Request $request,
        Organisation $organisation,
        $netObject
    ) {
        $validated = $request->validate([
            'approval' => "in:pending,approved,denied|required"
        ]);
        $this->logRequest($validated);

        $approval = $validated['approval'];

        // All users who can modify the net-object can set it's approval
        // status to 'pending' but only those who can manage-claims can set it
        // to other values.
        if ($approval !== 'pending') {
            Gate::authorize('manage-claims');
        }

        $toNotify = DB::transaction(function () use ($organisation, $netObject, $approval) {
            $toNotify = collect([$organisation->getKey() => $organisation]);

            $denyConflicting = $netObject->approval !== 'approved' && $approval === 'approved';


            $netObject->update(['approval' => $approval]);

            if ($denyConflicting) {
                $conflicting = $this->getConflictingNetObjects($netObject);
                foreach ($conflicting as $obj) {
                    Log::debug('HandlesClaims::updateNetObject: denying conflicting claim', [
                        'id' => $netObject->getKey(),
                    ]);
                    $obj->update(['approval' => 'denied']);
                    $org = $obj->organisation;
                    $conflicting->put($org->getKey(), $org);
                }
            }

            return $toNotify;
        });

        $mailController = new MailController();

        foreach ($toNotify as $org) {
            try {
                $mailController->notifyClaimOwners($org);
            } catch (\Exception $e) {
                Log::error("Sending claim notification mail failed");
            }
        }

        return $netObject;
    }

    /**
     * For a given netobject, determine other conflicting ones
     *
     * Conflicting netobjects in this sense are netobjects that are already
     * approved and should be denied if the given netobject is to be approved.
     * E.g. For an ASN, the conflicting objects are objects with the same ASN.
     * For a network address, the conflicting ones are the ones contained
     * within it.
     *
     * The objects in the returned laravel collection should have the
     * corresponding organisations already loaded, ideally.
     */
    public function getConflictingNetObjects($netObject)
    {
        return collect([]);
    }

    /**
     * Postprocess claim conflict information.
     */
    public function processConflicts($netObjects, $netObjectKey)
    {
        $allowedTenants = Tenant::assignableBy(Auth::user())->get();

        $conflicts = [];
        foreach ($netObjects as $net) {
            $org = null;
            if (! $net->organisation->tenants->intersect($allowedTenants)->isEmpty()) {
                $org = [
                    'name' => $net->organisation->name,
                ];
            }
            $conflicts[] = [
                $netObjectKey => $net->getAttribute($netObjectKey),
                'approval' => $net->approval,
                'organisation' => $org,
                'tenants' => $net->organisation->tenants->map(function ($tenant) {
                    return [
                        'tenant_id' => $tenant->getKey(),
                        'name' => $tenant->name,
                    ];
                }),
            ];
        }

        return $conflicts;
    }

    /**
     * Determines weather the initial approval state is pending or approved.
     *
     * The net object is automatically "approved" if an approved parent object exists
     * or the request was performed by a "portaladmin" or "tenantadmin" role.
     * Otherwise, it is "pending" and must be reviewed for approval.
     *
     * @param bool $hasApprovedParent
     * @return string
     */
    private function getInitialApprovalState(bool $hasApprovedParent = false): string
    {
        $approvalState = 'pending';

        if ($hasApprovedParent || Auth::user()->isPortalAdmin() || Auth::user()->isTenantAdmin()) {
            $approvalState = 'approved';
        }

        return $approvalState;
    }
}
