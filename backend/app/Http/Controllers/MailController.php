<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2020 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2020 Intevation GmbH <https://intevation.de>
 *
 */

namespace App\Http\Controllers;

use App\Mail\ClaimCreated;
use App\Mail\ClaimUpdated;
use App\Mail\ConfigReminder;
use App\Models\Organisation;
use App\Models\Tenant;
use App\Services\ContactService;
use App\Services\KeycloakAccountService;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;

/**
 * Controller handling mails send by the backend
 */
class MailController extends Controller
{
    /**
     * @var ContactService
     */
    private ContactService $contactService;

    /**
     * @var KeycloakAccountService
     */
    private KeycloakAccountService $kcService;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->contactService = new ContactService();
        $this->kcService = new KeycloakAccountService();
    }

    /**
     * Checks if reminder mails have to be sent and sends them.
     */
    public function checkForReminder(): void
    {
        Log::debug("Checking for expired contacts");
        $contacts = $this->contactService->getUnnotifiedExpiredContacts([
            'contact_id',
            'organisation_id',
            'roles'
        ]);

        if ($contacts->isEmpty()) {
            Log::debug("No expired contacts found");
            return;
        }

        Log::debug("Found " . count($contacts) . " outdated contacts with expired notification interval.");
        $expiryByOrganisation = $this->getOrganisationContactExpiryInfo($contacts->toArray());

        foreach ($expiryByOrganisation as $organisationId => $expiryInfo) {
            $organisation = Organisation::find($organisationId);
            $expiryInfo['organisation_name'] = $organisation->name;

            $this->notifyExpiredContacts($organisation, $expiryInfo);
        }
    }

    /**
     * Notifies organisation admins about expired contacts.
     *
     * @param Organisation $organisation
     * @param array $expirationData
     *
     * @return void
     */
    private function notifyExpiredContacts(Organisation $organisation, array $expirationData): void
    {
        foreach ($this->getOrganisationAdminIds($organisation) as $admin) {
            Log::debug("Checking {$admin} for organisation id {$organisation->organisation_id}");
            $kcUser = $this->kcService->getUser($admin);

            // Don't send reminder to admins with unverified emails or disabled accounts.
            if (empty($kcUser->emailVerified) || empty($kcUser->enabled)) {
                Log::debug('Skipping admin due to unverified email or disabled account: ' . $kcUser->username);
                continue;
            }

            Log::debug("Admin account is valid. Preparing expiration notification.");
            $adminContactData = $this->prepareKcUserRecipientData($kcUser);

            $tenant = $this->getFirstTenant($organisation);

            try {
                Mail::to($adminContactData['to-address'])->send(new ConfigReminder(
                    $adminContactData['to-name'],
                    str_replace(".", "_", $tenant["name"]),
                    $tenant['from_address'],
                    $tenant['from_name'],
                    $tenant['reply_to'],
                    $expirationData
                ));
            } catch (\Exception $e) {
                Log::warning("Failed to send expiry notification mail: " . $e);
            }

            // Updating 'latest_expiry_warning' without eloquent to bypass update events and timestamps.
            DB::table('contact')
                ->whereIn('contact_id', $expirationData['contact_ids'])
                ->update(['latest_expiry_warning' => Carbon::now()]);
        }
    }

    /**
     * Returns a set of organisation admins keycloak ids.
     * If no direct admin can be found, the parent organisations will be checked recursively.
     *
     * @param Organisation $organisation
     * @return array|Collection
     */
    private function getOrganisationAdminIds(Organisation $organisation): array|Collection
    {
        //Get admin ids
        $admins = DB::table('organisation_user')
        ->where('organisation_id', $organisation->getKey())
        ->pluck('keycloak_user_id');
        //If there no parents, get parent orga
        if (count($admins) == 0) {
            if (!$organisation->parent_id) {
                Log::debug("No Admin found for organisation with id {$organisation->getKey()}.");
                return array();
            }
            $parent = Organisation::find($organisation->parent_id);
            return $this->getOrganisationAdminIds($parent);
        }
        return $admins;
    }

    /**
     * Returns a collection of tenant administrators.
     *
     * @param Tenant $tenant
     * @return Collection
     */
    private function getTenantAdmins(Tenant $tenant): Collection
    {
        return DB::table('tenant_user')
            ->where('tenant_id', $tenant->getKey())
            ->distinct()
            ->pluck('keycloak_user_id');
    }

    /**
     * Returns the first tenant that is assigned a given organisation.
     *
     * If the organisation has a parent (suborganisation passed),
     * the function will resolve the relation recursively.
     *
     * @param Organisation $organisation
     * @return mixed
     */
    private function getFirstTenant(Organisation $organisation): mixed
    {
        if (!$organisation->parent_id) {
            return $organisation->tenants->toArray()[0];
        } else {
            $parent = Organisation::find($organisation->parent_id);
            return $this->getFirstTenant($parent);
        }
    }

    /**
     * Send email notifications that a claim has been updated
     */
    public function notifyClaimOwners(Organisation $orga): void
    {
        $orga_id = $orga->getKey();
        $admins = DB::table('organisation_user')
                    ->where('organisation_id', $orga_id)
                    ->pluck('keycloak_user_id');

        $mails = [];
        $tenants = $orga->tenants;

        foreach ($admins as $admin) {
            $kcUser = $this->kcService->getUser($admin);
            $recipientData = $this->prepareKcUserRecipientData($kcUser);

            foreach ($tenants as $tenant) {
                $mail = array(
                    "to_address" => $recipientData['to-address'],
                    "to_name" => $recipientData['to-name'],
                    "constituency" => str_replace(".", "_", $tenant["name"]),
                    "from_address" => $tenant->from_address,
                    "from_name" => $tenant->from_name,
                    "reply_to" => $tenant->reply_to,
                );
                array_push($mails, $mail);
            }
        }
        //Send mails
        foreach ($mails as $mail) {
            try {
                $this->sendClaimUpdated(
                    $mail["to_address"],
                    $mail["to_name"],
                    $mail["constituency"],
                    $mail["from_address"],
                    $mail["from_name"],
                    $mail["reply_to"]
                );
            } catch (\Exception $e) {
                Log::warning("Sending mail failed: " . $e);
            }
        }
    }

    /**
     * Sends a mail notification for a netobject claim create to all tenant admins
     * and the global admin notification mail.
     *
     * @param Tenant $tenant
     * @param array $mailData
     * @return void
     */
    public function notifyClaimCreate(Tenant $tenant, array $mailData): void
    {
        $recipients[] = $this->getDefaultAdminRecipientData();

        if ($admins = $this->getTenantAdmins($tenant)->toArray()) {
            $tenantAdminsIds =  array_merge(array_values($admins));

            foreach ($tenantAdminsIds as $tenantAdmin) {
                $kcUser = $this->kcService->getUser($tenantAdmin);
                $recipients[] = $this->prepareKcUserRecipientData($kcUser);
            }

            foreach ($recipients as $recipient) {
                try {
                    $this->sendClaimCreated(
                        $recipient['to-address'],
                        $recipient['to-name'],
                        str_replace(".", "_", $tenant->name),
                        $tenant->from_address,
                        $tenant->from_name,
                        $tenant->reply_to,
                        $mailData
                    );
                } catch (\Exception $e) {
                    Log::error('Unable to send notification for claim create.' . $e);
                }
            }
        }
    }

    /**
     * Sends a mail notifying a new claim was created.
     *
     * @param string $recipientAddress
     * @param string $recipientName
     * @param string $constituency
     * @param string|null $fromAddress
     * @param string|null $fromName
     * @param string|null $replyToAddress
     * @param array|null $mailData
     * @return void
     */
    private function sendClaimCreated(
        string $recipientAddress,
        string $recipientName,
        string $constituency,
        string $fromAddress = null,
        string $fromName = null,
        string $replyToAddress = null,
        array $mailData = null
    ): void {
        Mail::to($recipientAddress)
            ->send(new ClaimCreated(
                $recipientName,
                $constituency,
                $fromAddress,
                $fromName,
                $replyToAddress,
                $mailData
            ));
    }

    /**
     * Send a mail that a users claim has been updated
     * Parameters:
     *  - email: User email address to send reminder to
     *  - fullname: Full user name
     *  - constituency: Sending constituency name
     *  - from_address: Sending address
     *  - from_name: Sender name
     *  - reply_to: Reply to address
     */
    private function sendClaimUpdated(
        string $email,
        string $fullname,
        string $constituency,
        string $from_address = null,
        string $from_name = null,
        string $reply_to = null
    ) {
        Mail::to($email)->send(new ClaimUpdated($fullname, $constituency, $from_address, $from_name, $reply_to));
    }

    /**
     * Returns the prepared recipient data for a keycloak user as array.
     *
     * @param mixed $kcUser
     * @return array
     */
    private function prepareKcUserRecipientData(mixed $kcUser): array
    {
        $data = [];

        $data['to-name'] = $kcUser->username ?? $kcUser->email;

        // Replace the recipient name with fill name if present.
        if (isset($kcUser->firstName, $kcUser->lastName)) {
            $data['to-name'] = $kcUser->firstName . ' ' . $kcUser->lastName;
        }

        $data['to-address'] = $kcUser->email;

        return $data;
    }

    /**
     * Returns the default tuency mail notification recipient data.
     *
     * @return array
     */
    private function getDefaultAdminRecipientData(): array
    {
        return [
            'to-name' => 'Tuency Administrator',
            'to-address' => config('tuency.notifications.mail.general.admin_notification_mail')
        ];
    }

    /**
     * Determines the count and affected roles of expired contacts by
     * organisation id.
     *
     * @param array $contacts
     * @return array
     */
    private function getOrganisationContactExpiryInfo(array $contacts): array
    {
        $expiryInfo = [];

        foreach ($contacts as $contact) {
            if (! array_key_exists($contact['organisation_id'], $expiryInfo)) {
                $expiryInfo[$contact['organisation_id']] = [
                    'expiry_count' => 1,
                    'affected_roles' => json_decode($contact['roles'], false),
                    'contact_ids' => [$contact['contact_id']],
                ];
            } else {
                $expiryInfo[$contact['organisation_id']]['expiry_count']++;
                $expiryInfo[$contact['organisation_id']]['affected_roles'] = array_unique(
                    array_merge(
                        $expiryInfo[$contact['organisation_id']]['affected_roles'],
                        json_decode($contact['roles'])
                    )
                );
                $expiryInfo[$contact['organisation_id']]['contact_ids'][] = $contact['contact_id'];
            }
        }

        return $expiryInfo;
    }
}
