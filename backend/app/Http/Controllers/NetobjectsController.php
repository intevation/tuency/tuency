<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2021 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2021 Intevation GmbH <https://intevation.de>
 *
 * Author: 2021 Bernhard Herzog <bernhard.herzog@intevation.de>
 */

namespace App\Http\Controllers;

use App\Models\Asn;
use App\Models\Fqdn;
use App\Models\Network;
use App\Models\Organisation;
use App\Models\RipeHandle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class NetobjectsController extends Controller
{
    /**
     * List netobjects
     *
     * With the optional query parameter 'ancestor', which if given must be
     * the ID of an organisation, the result is restricted to claims of that
     * organisation and its direct and indirect suborganisations.
     *
     * With the optional query parameter 'tenant', which if given must be the
     * ID of an tenant, the result is restricted to claims of organisations
     * that are associated with that tenant.
     *
     * The optional parameter approval restricts the result according to
     * approval status. The value should one of the strings 'pending`,
     * 'approved', 'denied' or 'any'. 'any' means no restriction on approval
     * status. The default is 'pending'.
     *
     * The optional parameter kind restricts the result to one of the kinds of
     * netobjects. Supported values are 'asn', 'network', 'fqdn',
     * 'ripe_handle'. When omitted, all kinds are listed.
     *
     * The result is a JSON object with one attribute for each kind of
     * netobject. The corresponding values are arrays of the corresponding
     * kind of netobject or null if the kind is not the one given with the
     * kind parameter.
     */
    public function index(Request $request)
    {
        $allKinds = ['asn', 'network', 'fqdn', 'ripe_handle'];

        $validated = $request->validate([
            'ancestor' => 'int|nullable',
            'tenant' => 'int|nullable',
            'approval' => 'in:any,pending,approved,denied',
            'kind' => [Rule::in($allKinds)],
        ]);

        $this->logRequest($validated);

        if (!array_key_exists('approval', $validated)) {
            $validated['approval'] = 'pending';
        }

        if (array_key_exists('kind', $validated)) {
            $kinds = [$validated['kind']];
        } else {
            $kinds = $allKinds;
        }

        return [
            'asns' => in_array('asn', $kinds) ? $this->fetchAsns($validated) : null,
            'networks' => in_array('network', $kinds) ? $this->fetchNetworks($validated) : null,
            'fqdns' => in_array('fqdn', $kinds) ? $this->fetchFqdns($validated) : null,
            'ripe_handles' => in_array('ripe_handle', $kinds) ? $this->fetchRipeHandles($validated) : null,
        ];
    }

    protected function fetchAsns($validated)
    {
        $approval = $validated['approval'];

        return Asn::query()
            ->joinSub(
                Organisation::querySubHierarchy(
                    Auth::user(),
                    $validated['ancestor'] ?? null,
                    $validated['tenant'] ?? null,
                )->select('organisation.organisation_id')->distinct(),
                'organisations',
                function ($join) {
                    $join->on(
                        'asn.organisation_id',
                        '=',
                        'organisations.organisation_id'
                    );
                }
            )->when(
                $approval !== 'any',
                function ($query) use ($approval) {
                    return $query->where('approval', $approval);
                }
            )->with('organisation')
            ->get();
    }

    protected function fetchNetworks($validated)
    {
        $approval = $validated['approval'];

        return Network::query()
            ->joinSub(
                Organisation::querySubHierarchy(
                    Auth::user(),
                    $validated['ancestor'] ?? null,
                    $validated['tenant'] ?? null,
                )->select('organisation.organisation_id')->distinct(),
                'organisations',
                function ($join) {
                    $join->on(
                        'network.organisation_id',
                        '=',
                        'organisations.organisation_id'
                    );
                }
            )->when(
                $approval !== 'any',
                function ($query) use ($approval) {
                    return $query->where('approval', $approval);
                }
            )->with('organisation')
            ->get();
    }

    protected function fetchFqdns($validated)
    {
        $approval = $validated['approval'];

        return Fqdn::query()
            ->joinSub(
                Organisation::querySubHierarchy(
                    Auth::user(),
                    $validated['ancestor'] ?? null,
                    $validated['tenant'] ?? null,
                )->select('organisation.organisation_id')->distinct(),
                'organisations',
                function ($join) {
                    $join->on(
                        'fqdn.organisation_id',
                        '=',
                        'organisations.organisation_id'
                    );
                }
            )->when(
                $approval !== 'any',
                function ($query) use ($approval) {
                    return $query->where('approval', $approval);
                }
            )->with('organisation')
            ->get();
    }

    protected function fetchRipeHandles($validated)
    {
        $approval = $validated['approval'];

        return RipeHandle::query()
            ->joinSub(
                Organisation::querySubHierarchy(
                    Auth::user(),
                    $validated['ancestor'] ?? null,
                    $validated['tenant'] ?? null,
                )->select('organisation.organisation_id')->distinct(),
                'organisations',
                function ($join) {
                    $join->on(
                        'ripe_org_hdl.organisation_id',
                        '=',
                        'organisations.organisation_id'
                    );
                }
            )->when(
                $approval !== 'any',
                function ($query) use ($approval) {
                    return $query->where('approval', $approval);
                }
            )->with('organisation')
            ->get();
    }
}
