<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2020, 2021 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2021 Intevation GmbH <https://intevation.de>
 *
 * Author: 2021 Bernhard Herzog <bernhard.herzog@intevation.de>
 */

namespace App\Http\Controllers;

use App\Models\Organisation;
use App\Models\OrganisationAutomatic;
use App\Models\NetworkAutomatic;
use App\Models\NetworkAutomaticRule;
use App\Models\RipeHandle;
use App\Http\Requests\RuleFormRequest;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class NetworkAutomaticRuleController extends Controller
{
    /**
     * Retrieve all rules associated with an NETWORK.
     */
    public function index(
        Organisation $organisation,
        RipeHandle $ripe_handle,
        OrganisationAutomatic $ripe_organisation,
        NetworkAutomatic $network
    ) {
        $this->logRequest();
        return $network->rules()->with([
            'classificationTaxonomy',
            'classificationType',
            'feedProvider',
            'feedName',
            'contacts',
        ])->orderBy('network_automatic_rule_id')->get();
    }

    /**
     * Create a new rule associated with a network
     */
    public function store(
        RuleFormRequest $request,
        Organisation $organisation,
        RipeHandle $ripe_handle,
        OrganisationAutomatic $ripe_organisation,
        NetworkAutomatic $network
    ) {
        $validated = $request->validated();
        $this->logRequest($validated);
        $validated['address'] = $network->address;
        $validated['import_source'] = $network->import_source;
        $validated['organisation_id'] = $organisation->organisation_id;

        return DB::transaction(function () use ($validated) {
            $rule = NetworkAutomaticRule::create($validated);

            if (array_key_exists('contacts', $validated)) {
                $rule->contacts()->sync($validated['contacts']);
            }

            return $rule;
        });
    }

    /**
     * Retrieve a specific rule
     */
    public function show(
        Organisation $organisation,
        RipeHandle $ripe_handle,
        OrganisationAutomatic $ripe_organisation,
        NetworkAutomatic $network,
        NetworkAutomaticRule $rule
    ) {
        $this->logRequest();
        return $rule;
    }

    /**
     * Update a rule.
     */
    public function update(
        RuleFormRequest $request,
        Organisation $organisation,
        RipeHandle $ripe_handle,
        OrganisationAutomatic $ripe_organisation,
        NetworkAutomatic $network,
        NetworkAutomaticRule $rule
    ) {
        $validated = $request->validated();
        $this->logRequest($validated);

        return DB::transaction(function () use ($rule, $validated) {
            $rule->update($validated);

            if (array_key_exists('contacts', $validated)) {
                $rule->contacts()->sync($validated['contacts']);
            }

            return $rule;
        });
    }

    /**
     * Remove a rule.
     */
    public function destroy(
        Organisation $organisation,
        RipeHandle $ripe_handle,
        OrganisationAutomatic $ripe_organisation,
        NetworkAutomatic $network,
        NetworkAutomaticRule $rule
    ) {
        $this->logRequest();
        $rule->delete();
    }
}
