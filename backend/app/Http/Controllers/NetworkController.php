<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2021 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2021 Intevation GmbH <https://intevation.de>
 *
 * Author: 2021 Bernhard Herzog <bernhard.herzog@intevation.de>
 */

namespace App\Http\Controllers;

use App\Http\Controllers\MailController;
use App\Models\Network;
use App\Models\NetworkAutomatic;
use App\Models\Organisation;
use App\Models\OrganisationAutomatic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;
use IPTools\Network as IPToolsNetwork;

class NetworkController extends Controller
{
    use HandlesClaims;

    /**
     * List the networks associated with an organisation
     */
    public function index(Organisation $organisation)
    {
        $this->logRequest();
        return $organisation->networks;
    }

    /**
     * Create a new network associated with an organisation.
     *
     * The address parameter should be a IPv4 or IPV6 network address in CIDR
     * notation. This method parses that CIDR value and stores a value that is
     * reconstructed from that parsed value. This can result in a slightly
     * different string being returned from tuency. E.g. if there given CIDR
     * has non-zero bits to the right of the netmask, those bits will be set
     * to zero before storing the data.
     *
     * The subnetof parameter can be used to create a new network that is a
     * subnet of an already existing, approved network. In this case the new
     * network is created in an already approved state. The subnetof parameter
     * is optional, but when given, should be the ID of another network
     * associated with the same organisation. The new network must be a subnet
     * of the existing network.
     */
    public function store(Request $request, Organisation $organisation)
    {
        $validator = Validator::make($request->all(), [
            'address' => 'string|required',
            'subnetof' => 'int|nullable'
        ]);

        $parsedNetwork = null;

        $validator->after(function ($validator) use ($organisation, &$parsedNetwork) {
            $validated = $validator->validated();
            $address = $validated['address'];

            try {
                $parsedNetwork = IPToolsNetwork::parse($address);
            } catch (\Exception $e) {
                $validator->errors()->add(
                    'address',
                    "Invalid CIDR format: {$e->getMessage()}"
                );
            }

            $parentNetworkId = $validated['subnetof'] ?? null;
            if (!is_null($parentNetworkId)) {
                $parentNetwork = $organisation->networks->find($parentNetworkId);
                if (is_null($parentNetwork)) {
                    $validator->errors()->add(
                        'subnetof',
                        'The network given by subnetof does not exist'
                    );
                } else {
                    $parentAddress = IPToolsNetwork::parse($parentNetwork->address);
                    if ($parentNetwork->approval !== 'approved') {
                        $validator->errors()->add(
                            'subnetof',
                            'The network given by subnetof has not been approved'
                        );
                    }
                    if ($parentAddress->getIP()->getVersion() !== $parsedNetwork->getIP()->getVersion()) {
                        $validator->errors()->add(
                            'subnetof',
                            'The network given by subnetof has a different version than the address',
                        );
                    }
                    if (!$this->isSubnet($parentAddress, $parsedNetwork)) {
                        $validator->errors()->add(
                            'subnetof',
                            'The network given by subnetof does not contain the address'
                        );
                    }
                }
            }
        });

        $validated = $validator->validate();
        $this->logRequest($validated);

        $network = Network::create([
            'address' => $parsedNetwork->getCIDR(),
            'organisation_id' => $organisation->organisation_id,
            'approval' => $this->getInitialApprovalState(isset($validated['subnetof'])),
        ]);

        $this->notifyNetobjectCreate($organisation, ['type' => 'Network'], $network->approval);

        return $network;
    }

    /**
     * Check that $subnet is a subnet of $network.
     */
    public function isSubnet($network, $subnet)
    {
        return ($subnet->getPrefixLength() > $network->getPrefixLength())
            && (strcmp($subnet->getFirstIP()->inAddr(), $network->getFirstIP()->inAddr()) >= 0)
            && (strcmp($subnet->getLastIP()->inAddr(), $network->getLastIP()->inAddr()) <= 0);
    }

    /**
     * Update a network.
     */
    public function update(Request $request, Organisation $organisation, Network $network)
    {
        return $this->updateNetObject($request, $organisation, $network);
    }

    public function getConflictingNetObjects(Network $network)
    {
        return Network::whereKeyNot($network)
            ->where('organisation_id', '<>', $network->organisation_id)
            ->where('address', '<<=', $network->address)
            ->where('approval', '=', 'approved')
            ->with('organisation')
            ->get();
    }

    /**
     * Remove a network
     */
    public function destroy(Organisation $organisation, Network $network)
    {
        $this->logRequest();
        $network->delete();
    }

    /**
     * Return claimed networks that conflict with a given network claim.
     *
     * Two networks claims conflict with one another if one contains the
     * other. For each conflicting network claim this method returns the
     * address, the tenants to which the network belongs and the name of the
     * organisations to which it belongs, if the requesting user belongs to
     * one of the organisation's tenants.
     */
    public function conflicts(Organisation $organisation, Network $network)
    {
        Gate::authorize('manage-claims');

        $this->logRequest();

        // The route does not use scoped resolution, so we have to check this
        // explicitly.
        abort_unless($network->organisation_id === $organisation->getKey(), 404);

        $networks = Network::whereKeyNot($network)
            ->where('address', '&&', $network->address)
            ->where('approval', '<>', 'denied')
            ->with('organisation.tenants')
            ->get();

        return [
            'tuency' => $this->processConflicts($networks, 'address'),
            'ripe' => $this->getRipeConflicts($network->address)
        ];
    }

    /**
     * Queries ripe data for conflicting network addresses.
     *
     * @param string $networkAddress
     * @return array
     */
    private function getRipeConflicts(string $networkAddress): array
    {
        return NetworkAutomatic::where('address', '&&', $networkAddress)
            ->with('organisations:name')
            ->get()
            ->toArray();
    }
}
