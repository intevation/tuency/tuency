<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2020 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2020 Intevation GmbH <https://intevation.de>
 *
 * Author: 2021 Magnus Schieder <magnus.schieder@intevation.de>
 */

namespace App\Http\Controllers;

use App\Models\PDF;
use App\Models\Contact;
use Illuminate\Http\Request;

class PDFController extends Controller
{
    /*
     * Display a PDF.
     */

    public function show(PDF $pdf)
    {
        if (Contact::checkAuthorisation($pdf->contact_id)) {
            $content = base64_decode($pdf->pdf);
            return response($content)->header('Content-Type', 'application/pdf');
        }
        abort(403);
    }

    /*
     * Remove a pdf.
     */

    public function destroy(PDF $pdf)
    {
        if (Contact::checkAuthorisation($pdf->contact_id)) {
            $pdf->delete();
        } else {
            abort(403);
        }
    }
}
