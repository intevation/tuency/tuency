<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2020, 2021 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2021 Intevation GmbH <https://intevation.de>
 *
 * Author: 2021 Bernhard Herzog <bernhard.herzog@intevation.de>
 */

namespace App\Http\Controllers;

use App\Models\Organisation;
use App\Models\RipeHandle;

class RipeHandleNetworkController extends Controller
{
    /**
     * Retrieve the automatically managed networks associated with a ripe
     * handle that has been claimed by an organisation if that handle has been
     * approved. For other handles that have not yet been approved, the result
     * is always an empty list.
     *
     * The result is a list of the RIPE organisations for the RIPE handle
     * which among others have an attribute 'networks' which has a list of all
     * the networks associated with that organisation.
     */
    public function index(Organisation $organisation, RipeHandle $ripe_handle)
    {
        $this->logRequest();

        if ($ripe_handle->approval !== 'approved') {
            return [];
        }

        return $ripe_handle->ripeOrganisations()->with('networks')->get();
    }
}
