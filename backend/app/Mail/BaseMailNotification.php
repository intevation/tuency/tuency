<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

abstract class BaseMailNotification extends Mailable
{
    use Queueable;
    use SerializesModels;

    /**
     * @var string Recipient full name.
     */
    protected string $recipientName;

    /**
     * @var string Constituency name (DB "tenant.name" attribute).
     */

    protected string $constituency;

    /**
     * @var string|null Sender mail address.
     */
    protected ?string $fromAddress;

    /**
     * @var string|null Sender name
     */
    protected ?string $fromName;

    /**
     * @var string|null Reply to address.
     */
    protected ?string $replyToAddress;

    /**
     * Additional mail data for use in the mail template.
     *
     * @var array|null
     */
    protected ?array $mailData;

    /**
     * Create a new base notification instance.
     *
     * @return void
     */
    public function __construct(
        string $recipientName,
        string $constituency,
        string $fromAddress = null,
        string $fromName = null,
        string $replyToAddress = null,
        array $mailData = []
    ) {
        $this->recipientName = $recipientName;
        $this->constituency = $constituency;
        $this->fromAddress = $fromAddress;
        $this->fromName = $fromName;
        $this->replyToAddress = $replyToAddress;
        $this->mailData = $mailData;

        $this->checkMailAttributes();
    }

    /**
     * Checks the provided mail attributes and replaces missing values with defaults.
     *
     * @return void
     */
    protected function checkMailAttributes(): void
    {
        // Use the config default from-address if none is provided.
        if (empty($this->fromAddress)) {
            $this->fromAddress = config('mail.from.address');
        }

        // Set the from-name to the constituency name if none is provided.
        if (empty($this->fromName)) {
            $this->fromName = $this->constituency;
        }

        // Set the reply-to-address to the from address if none is provided.
        if (empty($this->replyToAddress)) {
            $this->replyToAddress = $this->fromAddress;
        }
    }

    /**
     * Writes a debug log for the mail about to send.
     *
     * @param string $viewName
     * @return void
     */
    protected function writeDebugLog(string $viewName): void
    {
        Log::debug(
            sprintf(
                "Using mail Template %s from %s(%s, reply to: %s) to %s",
                $viewName,
                $this->fromAddress,
                $this->fromName,
                $this->replyToAddress,
                $this->recipientName
            )
        );
    }
}
