<?php

namespace App\Mail;

class ClaimCreated extends BaseMailNotification
{
    public function build()
    {
        $viewName = 'emails.claimcreated_' . $this->constituency;
        $subject = config('tuency.notifications.mail.subjects.claim.created');

        // Fall back to default view.
        if (!view()->exists($viewName)) {
            $viewName = 'emails.claimcreated';
        }

        $this->writeDebugLog($viewName);

        return $this
            ->from($address = $this->fromAddress, $name = $this->fromName)
            ->replyTo($address = $this->replyToAddress, $name = $this->fromName)
            ->subject($subject)
            ->with([
                'fullname' => $this->recipientName,
                'mailData' => $this->mailData
            ])
            ->view($viewName);
    }
}
