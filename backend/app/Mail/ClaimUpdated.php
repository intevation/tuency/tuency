<?php

namespace App\Mail;

use Illuminate\Support\Facades\Log;

class ClaimUpdated extends BaseMailNotification
{
    /**
     * Build the message.
     *
     * The template used will be determined by the given constituency name.
     * E.g., a mail send by the constituency "myconstituency" will be using the template
     * "emails.configreminder_myconstituency"     *
     * The templates can be placed at resources/views/emails.
     *
     * @return $this
     */
    public function build(): static
    {
        $viewName = 'emails.claimupdated_' . $this->constituency;
        $subject = config('tuency.notifications.mail.subjects.claim.updated');

        // Fall back to default view.
        if (!view()->exists($viewName)) {
            $viewName = 'emails.claimupdated';
        }

        $this->writeDebugLog($viewName);

        return $this
            ->from($address = $this->fromAddress, $name = $this->fromName)
            ->replyTo($address = $this->replyToAddress, $name = $this->fromName)
            ->subject($subject)
            ->with([
                'fullname' => $this->recipientName
            ])
            ->view($viewName);
    }
}
