<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2021 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2021 Intevation GmbH <https://intevation.de>
 *
 * Author: 2021 Bernhard Herzog <bernhard.herzog@intevation.de>
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use OwenIt\Auditing\Auditable as AuditTrait;

class Asn extends Model implements Auditable
{
    use HasUpdatedBy;
    use AuditTrait;

    protected $table = 'asn';

    // Ideally, we would use the pair (organisation_id, asn) as the primary
    // key, but Eloquent does not support composite keys as primary keys, so
    // we also have a surrogate key.
    protected $primaryKey = 'asn_id';

    protected $fillable = ['asn', 'organisation_id', 'approval'];

    protected static function booted()
    {
        static::deleting(function ($asn) {
            // Remove associated rules. Call delete on each rule separately so
            // that model events are triggered.
            $asn->rules()->get()->each->delete();
        });
    }

    /**
     * An ASN belongs to one organisation.
     */
    public function organisation()
    {
        return $this->belongsTo(Organisation::class, 'organisation_id');
    }

    /** one-to-many relationship with rules
     *
     * Each ASN has 0 or more associated rules.
     */
    public function rules()
    {
        return $this->hasMany(AsnRule::class, 'asn_id');
    }
}
