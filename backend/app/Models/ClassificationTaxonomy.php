<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2021 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2021 Intevation GmbH <https://intevation.de>
 *
 * Author: 2021 Bernhard Herzog <bernhard.herzog@intevation.de>
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use OwenIt\Auditing\Auditable as AuditTrait;

class ClassificationTaxonomy extends Model implements Auditable
{
    use AuditTrait;

    protected $table = 'classification_taxonomy';

    protected $primaryKey = 'classification_taxonomy_id';

    // To keep things simple at the start, we omit the time stamps.
    public $timestamps = false;

    protected $fillable = ['name'];

    protected $visible = [
        'classification_taxonomy_id',
        'name',
        'types'
    ];

    public function types()
    {
        return $this->hasMany(ClassificationType::class, 'classification_taxonomy_id', 'classification_taxonomy_id');
    }
}
