<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2020, 2021 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2020 Intevation GmbH <https://intevation.de>
 *
 * Author: 2020 Bernhard Herzog <bernhard.herzog@intevation.de>
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\DB;
use OwenIt\Auditing\Contracts\Auditable;
use OwenIt\Auditing\Auditable as AuditTrait;

class ContactTag extends Model implements Auditable
{
    use HasFactory;
    use AuditTrait;

    // For consistency with the schema expected by the RIPE importer we
    // use the singular for the table name.
    protected $table = 'contact_tag';

    // For consistency with the schema expected by the RIPE importer we
    // use 'contacttag_id' instead of 'id'.
    protected $primaryKey = 'contact_tag_id';

    protected $fillable = ['name', 'visible'];



    protected static function booted()
    {
        static::deleting(function ($tag) {
            // when deleting a contact tag, remove the association with the
            // tenants and contacts, too.
            $tag->tenants()->sync([]);
            $tag->contacts()->sync([]);
        });
    }

    /** one-to-many relationship with tenants
     *
     * Each tag has 0 or more associated tenants.
     */
    public function tenants()
    {
        return $this->belongsToMany(
            Tenant::class,
            'contact_tag_tenant',
            'contact_tag_id',
            'tenant_id'
        )->using(ContactTagTenant::class)->withTimestamps();
    }

    /** many-to-many relation ship with contacts
     *
     * A contact may have any number of contact tags.
     */
    public function contacts()
    {
        return $this->belongsToMany(
            Contact::class,
            'contact_contact_tag',
            'contact_tag_id',
            'contact_id'
        )->using(ContactContactTag::class)->withTimestamps();
    }

    /**
     * Scope for querying tags for a user
     *
     * Portal admins can see all tags. Tenant admins can see the tags
     * associated with their tenants. All users, and hence in particular
     * orga-admins, can see tags that are marked as visible.
     */
    public function scopeForUser($query, User $user)
    {
        if ($user->isPortalAdmin()) {
            return $query;
        }

        return $query->where(function ($query) use ($user) {
            $query->when($user->isTenantAdmin(), function ($query) use ($user) {
                return $query->whereHas('tenants', function ($query) use ($user) {
                    return $query->whereExists(function ($query) use ($user) {
                        return $query->select(DB::raw(1))
                            ->from('tenant_user')
                            ->whereColumn('tenant_user.tenant_id', 'tenant.tenant_id')
                            ->where('keycloak_user_id', $user->getKeycloakUserId());
                    });
                });
            })->orWhere('visible', true);
        });
    }
}
