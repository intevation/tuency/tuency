<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2021 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2021 Intevation GmbH <https://intevation.de>
 *
 * Author: 2021 Bernhard Herzog <bernhard.herzog@intevation.de>
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use OwenIt\Auditing\Contracts\Auditable;
use OwenIt\Auditing\Auditable as AuditTrait;

class FqdnRule extends Rule implements Auditable
{
    use HasFactory;
    use HasUpdatedBy;
    use AuditTrait;

    protected $table = 'fqdn_rule';

    protected $primaryKey = 'fqdn_rule_id';

    protected $parent_id_column = 'fqdn_id';

    protected static function booted()
    {
        static::deleting(function ($rule) {
            // when deleting an FqdnRule, remove the association with the
            // contacts, too, but not the contacts themselves.
            $rule->contacts()->sync([]);
        });
    }

    /** one-to-many relationship with contacts
     *
     * Each rule has 0 or more associated contacts
     */
    public function contacts()
    {
        return $this->belongsToMany(
            Contact::class,
            'contact_fqdn_rule',
            'fqdn_rule_id',
            'contact_id'
        )->using(ContactFqdnRule::class)->withTimestamps();
    }

    /**
     * Each FqdnRule is associated with an Fqdn
     */
    public function fqdn()
    {
        return $this->belongsTo(Fqdn::class, 'fqdn_id');
    }

    /**
     * Each fqdn_id is indirectly associated with an organisation.
     * This method returns a relationship.
     */
    public function organisation()
    {
        return $this->fqdn->organisation();
    }

    /**
     * Determine the preferred abuse-c address
     */
    public function preferredAbuseC()
    {
        return $this->fqdn->organisation->preferredAbuseC();
    }
}
