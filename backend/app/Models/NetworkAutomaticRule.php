<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2021 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2021 Intevation GmbH <https://intevation.de>
 *
 * Author: 2021 Bernhard Herzog <bernhard.herzog@intevation.de>
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use OwenIt\Auditing\Contracts\Auditable;
use OwenIt\Auditing\Auditable as AuditTrait;

class NetworkAutomaticRule extends Rule implements Auditable
{
    use HasFactory;
    use HasUpdatedBy;
    use AuditTrait;

    // We use composite keys for the relationship with NetworkAutomatic
    use \Awobaz\Compoships\Compoships;

    protected $table = 'network_automatic_rule';

    protected $primaryKey = 'network_automatic_rule_id';

    public function getFillable()
    {
        $fillable = parent::getFillable();
        $fillable = array_merge($fillable, ['address', 'import_source', 'organisation_id']);
        return $fillable;
    }


    protected static function booted()
    {
        static::deleting(function ($rule) {
            // when deleting an FqdnRule, remove the association with the
            // contacts, too, but not the contacts themselves.
            $rule->contacts()->sync([]);
        });
    }

    /** one-to-many relationship with contacts
     *
     * Each rule has 0 or more associated contacts
     */
    public function contacts()
    {
        return $this->belongsToMany(
            Contact::class,
            'contact_network_automatic_rule',
            'network_automatic_rule_id',
            'contact_id'
        )->using(ContactNetworkAutomaticRule::class)->withTimestamps();
    }

    /**
     * A NetworkAutomaticRule has one organisation.
     */
    public function organisation()
    {
        return $this->belongsTo(Organisation::class, 'organisation_id');
    }
}
