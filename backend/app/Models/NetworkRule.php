<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2021 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2021 Intevation GmbH <https://intevation.de>
 *
 * Author: 2021 Bernhard Herzog <bernhard.herzog@intevation.de>
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use OwenIt\Auditing\Contracts\Auditable;
use OwenIt\Auditing\Auditable as AuditTrait;

class NetworkRule extends Rule implements Auditable
{
    use HasFactory;
    use HasUpdatedBy;
    use AuditTrait;

    protected $table = 'network_rule';

    protected $primaryKey = 'network_rule_id';

    protected $parent_id_column = 'network_id';

    protected static function booted()
    {
        static::deleting(function ($rule) {
            // when deleting an NetworkRule, remove the association with the
            // contacts, too, but not the contacts themselves.
            $rule->contacts()->sync([]);
        });
    }

    /** one-to-many relationship with contacts
     *
     * Each rule has 0 or more associated contacts
     */
    public function contacts()
    {
        return $this->belongsToMany(
            Contact::class,
            'contact_network_rule',
            'network_rule_id',
            'contact_id'
        )->using(ContactNetworkRule::class)->withTimestamps();
    }

    /**
     * Each NetworkRule is associated with an Network
     */
    public function network()
    {
        return $this->belongsTo(Network::class, 'network_id');
    }

    /**
     * Each NetworkRule is indirectly associated with an organisation.
     * This method returns a relationship.
     */
    public function organisation()
    {
        return $this->network->organisation();
    }

    /**
     * Determine the preferred abuse-c address
     */
    public function preferredAbuseC()
    {
        return $this->network->organisation->preferredAbuseC();
    }
}
