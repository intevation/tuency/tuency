<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2021 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2021 Intevation GmbH <https://intevation.de>
 *
 * Author: 2021 Bernhard Herzog <bernhard.herzog@intevation.de>
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrganisationAutomatic extends Model
{
    protected $table = 'organisation_automatic';

    protected $primaryKey = 'organisation_automatic_id';

    // The tables managed by the RIPE importer have timestamps that are
    // different from the ones Laravel expects.
    public $timestamps = false;

    protected $fillable = [
        'name',
        'ripe_org_hdl',
        'import_source',
        'import_time',
    ];

    protected $hidden = ['pivot'];

    /** one-to-many relationship with contacts
     *
     * Each organisation has 0 or more associated contacts.
     */
    public function contacts()
    {
        return $this->hasMany(
            ContactAutomatic::class,
            'organisation_automatic_id'
        );
    }

    /** one-to-many relationship with networks
     *
     * Each organisation has 0 or more associated networks
     */
    public function networks()
    {
        return $this->belongsToMany(
            NetworkAutomatic::class,
            'organisation_to_network_automatic',
            'organisation_automatic_id',
            'network_automatic_id'
        );
    }

    /**
     * Scope for querying organisations for a given ASN.
     */
    public function scopeForAsn($query, $asn)
    {
        return $query->join(
            'organisation_to_asn_automatic',
            'organisation_to_asn_automatic.organisation_automatic_id',
            '=',
            'organisation_automatic.organisation_automatic_id'
        )->where('asn', $asn);
    }
}
