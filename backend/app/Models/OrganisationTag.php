<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2020, 2021 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2020 Intevation GmbH <https://intevation.de>
 *
 * Author: 2020 Magnus Schieder <magnus.schieder@intevation.de>
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\DB;
use OwenIt\Auditing\Contracts\Auditable;
use OwenIt\Auditing\Auditable as AuditTrait;

class OrganisationTag extends Model implements Auditable
{
    use HasFactory;
    use AuditTrait;

    protected $table = 'organisation_tag';

    protected $primaryKey = 'organisation_tag_id';

    protected $fillable = ['name', 'visible'];

    protected static function booted()
    {
        static::deleting(function ($tag) {
            // when deleting a orga tag, remove the association with the
            // tenants and organisations, too.
            $tag->tenants()->sync([]);
            $tag->organisations()->sync([]);
        });
    }

    /** one-to-many relationship with tenants
     *
     * Each tag has 0 or more associated tenants.
     */
    public function tenants()
    {
        return $this->belongsToMany(
            Tenant::class,
            'organisation_tag_tenant',
            'organisation_tag_id',
            'tenant_id'
        )->using(OrganisationTagTenant::class)->withTimestamps();
    }

    /** many-to-many relationship with organisations
     *
     * A organisations may have any number of organisations tags.
     */
    public function organisations()
    {
        return $this->belongsToMany(
            Organisation::class,
            'organisation_organisation_tag',
            'organisation_tag_id',
            'organisation_id'
        )->using(OrganisationOrganisationTag::class)->withTimestamps();
    }

    /**
     * Scope for querying tags for a user
     *
     * Portal admins can see all tags. Tenant admins can see the tags
     * associated with their tenants. All users, and hence in particular
     * orga-admins, can see tags that are marked as visible.
     */
    public function scopeForUser($query, User $user)
    {
        if ($user->isPortalAdmin()) {
            return $query;
        }

        return $query->where(function ($query) use ($user) {
            $query->when($user->isTenantAdmin(), function ($query) use ($user) {
                return $query->whereHas('tenants', function ($query) use ($user) {
                    return $query->whereExists(function ($query) use ($user) {
                        return $query->select(DB::raw(1))
                            ->from('tenant_user')
                            ->whereColumn('tenant_user.tenant_id', 'tenant.tenant_id')
                            ->where('keycloak_user_id', $user->getKeycloakUserId());
                    });
                });
            })->orWhere('visible', true);
        });
    }
}
