<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2021 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2021 Intevation GmbH <https://intevation.de>
 *
 * Author: 2021 Bernhard Herzog <bernhard.herzog@intevation.de>
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;
use OwenIt\Auditing\Contracts\Auditable;
use OwenIt\Auditing\Auditable as AuditTrait;

/**
 * Custom pivot model for the Organisation <-> Tenant relationship
 *
 * The purpose of the model is to automatically update the timestamps on the
 * Organisation when the organisation is attached or detached to/from a
 * tenant.
 */
class OrganisationTenant extends Pivot implements Auditable
{
    use AuditTrait;

    protected $touches = ['organisation'];

    public function organisation()
    {
        return $this->belongsTo(Organisation::class, 'organisation_id');
    }
}
