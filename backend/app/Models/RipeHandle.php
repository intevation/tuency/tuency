<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2020 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2020 Intevation GmbH <https://intevation.de>
 *
 * Author(s):
 * 2021 Fadi Abbud <fadi.abbud@intevation.de>
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use OwenIt\Auditing\Auditable as AuditTrait;

class RipeHandle extends Model implements Auditable
{
    use HasUpdatedBy;
    use AuditTrait;

    protected $table = 'ripe_org_hdl';

    protected $primaryKey = 'ripe_org_hdl_id';

    protected $fillable = ['ripe_org_hdl','organisation_id', 'approval'];

    /**
     * A ripe_handle belongs to one organisation.
     */
    public function organisation()
    {
        return $this->belongsTo(Organisation::class, 'organisation_id');
    }

    public function ripeOrganisations()
    {
        return $this->hasMany(OrganisationAutomatic::class, 'ripe_org_hdl', 'ripe_org_hdl');
    }
}
