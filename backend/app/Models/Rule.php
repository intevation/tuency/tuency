<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2021 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2021 Intevation GmbH <https://intevation.de>
 *
 * Author: 2021 Bernhard Herzog <bernhard.herzog@intevation.de>
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Base class for the rule models.
 */
class Rule extends Model
{
    protected $fillable = [
        'classification_taxonomy_id',
        'classification_type_id',
        'feed_provider_id',
        'feed_name_id',
        'feed_status',
        'suppress',
        'interval_length',
        'interval_unit',
        'commentary',
        'classification_identifier'
    ];

    protected $attributes = [
        'classification_taxonomy_id' => -1,
        'classification_type_id' => -1,
        'feed_provider_id' => -1,
        'feed_name_id' => -1,
        'feed_status' => 'any',
        'suppress' => false,
        'interval_length' => 0,
        'interval_unit' => 'immediate',
    ];

    protected $parent_id_column = null;

    protected $has_abuse_c_flag = true;

    public function getFillable()
    {
        $fillable = parent::getFillable();
        if (!is_null($this->parent_id_column)) {
            $fillable = array_merge($fillable, [$this->parent_id_column]);
        }
        if ($this->has_abuse_c_flag) {
            $fillable = array_merge($fillable, ['abuse_c']);
        }
        return $fillable;
    }

    public function classificationTaxonomy()
    {
        return $this->belongsTo(
            ClassificationTaxonomy::class,
            'classification_taxonomy_id'
        );
    }

    public function classificationType()
    {
        return $this->belongsTo(
            ClassificationType::class,
            'classification_type_id'
        );
    }

    public function feedProvider()
    {
        return $this->belongsTo(FeedProvider::class, 'feed_provider_id');
    }

    public function feedName()
    {
        return $this->belongsTo(FeedName::class, 'feed_name_id');
    }

    /**
     * Determine whether the rule matches the selectors
     *
     * The rule matches when all selectors match. A selector matches if the
     * value in the rule is "any" or its value in the rule is equal to the one
     * in the $selectors array. For the selectors that are represented by
     * foreign keys the value is he name from the corresponding model.
     */
    public function matches($selector)
    {
        return (
            ($this->classification_taxonomy_id === -1
                || $selector['classification_taxonomy'] === 'any'
                || $selector['classification_taxonomy'] === $this->classificationTaxonomy->name)
            && ($this->classification_type_id === -1
                || $selector['classification_type'] === 'any'
                || $selector['classification_type'] === $this->classificationType->name)
            && ($this->feed_provider_id === -1
                || $selector['feed_provider'] === 'any'
                || $selector['feed_provider'] === $this->feedProvider->name)
            && ($this->feed_name_id === -1
                || (isset($selector['feed_name']) && $selector['feed_name'] === 'any')
                || (isset($selector['feed_name']) ?
                $selector['feed_name'] === $this->feedName->name :
                $selector['feed_code'] === $this->feedName->code))
            && ($this->feed_status === 'any'
                || $selector['feed_status'] === $this->feed_status)
            && ((empty($this->classification_identifier) && !isset($selector['classification_identifier']))
                || (isset($selector['classification_identifier']) &&
                    $selector['classification_identifier'] === $this->classification_identifier)));
    }
}
