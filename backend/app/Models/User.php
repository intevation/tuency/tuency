<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use KeycloakGuard\KeycloakGuard;
use OwenIt\Auditing\Contracts\Auditable;
use OwenIt\Auditing\Auditable as AuditTrait;

/**
 * Represents the laravel user model.
 *
 * The api:auth middleware will automatically match the JWTs "sub" attribute (Keycloak User UUID) against
 * the database user table and also stores an entry if the "keycloak_user_id" does not yet exist.
 * The decoded JWT is injected to this user model builds the base for determining the users abilities in this system.
 * (As seen in most of the group resolve functions.)
 *
 * For transient user objects (e.g. listing users and permissions in the node overview) the client roles
 * must be set manually from the information provided by KeycloakAccountService since decoded token is not available.
 * This can be done by using $transientGroups array.
 */
class User extends Authenticatable implements Auditable
{
    //use HasUpdatedBy; TODO: Cannot be used this way since it would create an infinity loop.
    use AuditTrait;

    protected $table = 'user';

    protected $primaryKey = 'keycloak_user_id';

    // The primary key is a string
    public $incrementing = false;
    protected $keyType = 'string';

    protected $fillable = [
        'keycloak_user_id',
        'updated_by'
    ];

    /**
     * @var array Group array for role resolution on transient user objects.
     */
    protected array $transientGroups;

    protected static function booted(): void
    {
        static::deleting(function ($user) {
            // Remove links to tenants and tags
            $user->tenants()->detach();
            $user->organisations()->detach();
        });
    }


    /**
     * A user can belong to many tenants.
     *
     * @return BelongsToMany
     */
    public function tenants(): BelongsToMany
    {
        return $this->belongsToMany(
            Tenant::class,
            'tenant_user',
            'keycloak_user_id',
            'tenant_id'
        )->using(TenantUser::class)->withTimestamps();
    }

    /**
     * A user can belong to many organisations.
     *
     * @return BelongsToMany
     */
    public function organisations(): BelongsToMany
    {
        return $this->belongsToMany(
            Organisation::class,
            'organisation_user',
            'keycloak_user_id',
            'organisation_id'
        )->using(OrganisationUser::class)->withTimestamps();
    }

    /**
     * Returns the keycloak user id.
     *
     * @return string
     */
    public function getKeycloakUserId(): string
    {
        return $this->keycloak_user_id;
    }

    /**
     * Allows to set the groups manually for a transient user object.
     * In this case "transient" means that the user object is required (e.g. for a users group/role resolution)
     * for an unauthenticated user where the groups cannot be determined over a JWT.
     *
     * @param $transientGroups
     * @return void
     */
    public function setTransientGroups($transientGroups): void
    {
        if (empty($this->token)) {
            $this->transientGroups = $transientGroups;
        }
    }

    /**
     * NOTE: The following functions apply keycloak token functionality to the
     * user model and refer to the injected decoded token from KeycloakGuard.
     * @see KeycloakGuard::user() (Line 117)
     */

    /**
     * Returns either the groups provided from the JWT for an authenticated user
     * or from the transientGroups variable when it is a transient user object.
     *
     * @return array
     */
    public function getGroups(): array
    {
        if (empty($this->token)) {
            return $this->transientGroups;
        }

        return $this->token->clientroles;
    }

    /**
     * Returns the tokens username.
     *
     * @return string
     */
    public function getUsername(): string
    {
        return $this->token->username;
    }

    /**
     * Returns the tokens client id.
     *
     * @return string
     */
    public function getClientId(): string
    {
        return $this->token->azp;
    }


    /**
     * Returns the tokens full name property or
     * the username, if not provided.
     *
     * @return string
     */
    public function getFullname(): string
    {
        return $this->token->name ?? $this->token->username;
    }

    /**
     * Return whether the user is a portal admin.
     */
    public function isPortalAdmin(): bool
    {
        return in_array('portaladmin', $this->getGroups());
    }

    /**
     * Return whether the user is a tenant admin.
     */
    public function isTenantAdmin(): bool
    {
        return in_array('tenantadmin', $this->getGroups());
    }

    /**
     * Return whether the user is an organisation admin.
     */
    public function isOrgaAdmin(): bool
    {
        return in_array('orgaadmin', $this->getGroups());
    }

    /**
     * Return details about the groups the user belongs to.
     *
     * This method performs database queries where necessary, so is not a
     * simple and cheap property accessor.
     *
     * The return value is an array with one element for each group. Each
     * group is an array with the following keys:
     *
     *  'level': The group level, one of 'portaladmin', 'tenantadmin',
     *           'orgaadmin'
     *
     *  'nodes': An array of the nodes the user is associated with at this
     *           level. Only present for 'tenantadmin' and 'orgaadmin'. Each
     *           element is an array with the following keys:
     *
     *            'name': The name of the node (e.g. organisation name)
     *            'node_id': The ID of the node (e.g. the organisation id)
     *
     *           A node describing an organisation has two additional keys:
     *
     *            'parents': An array with the names of the parent
     *                       organisations.
     *            'tenants': An array with the names of the tenants the
     *                       organisation belongs to.
     *
     * The organisations listed for an orgaadmin are those the user is
     * directly associated with. In particular, it does not include their
     * sub-organisations.
     *
     * Example result in JSON:
     *
     *     [
     *         {
     *             "level" : "orgaadmin",
     *             "nodes" : [
     *                 {
     *                     "node_id" : 1,
     *                     "name" : "An Organisation",
     *                     "parents" : [
     *                         "Parent Organisation"
     *                     ],
     *                     "tenants" : [
     *                         "Tenant A"
     *                     ]
     *                 }
     *             ]
     *         }
     *     ]
     */
    public function resolveGroups(): array
    {
        $groups = [];
        if ($this->isPortalAdmin()) {
            $groups[] = [
                'level' => 'portaladmin',
            ];
        }
        if ($this->isTenantAdmin()) {
            $tenants = DB::table('tenant_user')
                ->join('tenant', 'tenant.tenant_id', '=', 'tenant_user.tenant_id')
                ->where('keycloak_user_id', $this->getKeycloakUserId())
                ->select('tenant.tenant_id as node_id', 'tenant.name as name')
                ->get();
            $groups[] = [
                'level' => 'tenantadmin',
                'nodes' => $tenants,
            ];
        }
        if ($this->isOrgaAdmin()) {
            $organisations = Organisation::orgaAdmin($this->getKeycloakUserId())
                ->with('tenants:name')
                ->get();

            $groups[] = [
                'level' => 'orgaadmin',
                'nodes' => $organisations->map(function ($o) {
                    return [
                        'node_id' => $o->organisation_id,
                        'name' => $o->name,
                        'parents' => Organisation::getParents($o->organisation_id),
                        'tenants' => $o->tenants->map(function ($t) {
                            return ['name' => $t['name']];
                        }),
                    ];
                }),
            ];
        }
        return $groups;
    }
}
