<?php

namespace App\Providers;

use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Extends Laravel EloquentUserProvider to provide a custom
 * retrieve method that stores new users from keycloak into
 * the app db.
 */
class KeycloakUserProvider extends EloquentUserProvider
{
    /**
     * Receives the decoded token from robsontenorio KeycloakGuard
     * in order to store newly created users on keycloak side into
     * the application database.
     *
     * @param $decodedToken
     * @param $credentials
     *
     * @return Model|Builder
     */
    public function retrieveByKeycloakId($decodedToken, $credentials): Model|Builder
    {
        $credentialKey = array_key_first($credentials);
        $credentialValue = $credentials[$credentialKey];

        $model = $this->createModel();

        return $this->newModelQuery($model)->firstOrCreate([
                $credentialKey => $credentialValue,
            ]);
    }
}
