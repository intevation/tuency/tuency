<?php

namespace App\Services;

use App\Models\Tenant;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

/**
 * Serves functions to operate with the Tuency Laravel config.
 */
class TuencyConfigService
{
    /**
     * Seconds how long the tenant database config is cached.
     */
    private const TENANT_CACHE_SECONDS = 21600;

    /**
     * Returns an array of available contact roles for a specific tenant name.
     *
     * @param string $tenantName
     * @param bool $excludeGlobalRoles
     * @return array
     */
    public function getContactRolesByTenantName(string $tenantName, bool $excludeGlobalRoles = false): array
    {
        if (!$this->tenantNameExists($tenantName)) {
            Log::error('Requested tenant name ' . $tenantName . ' does not exists.');
            return [];
        }

        $tenantContactRoleList = $this->getTenantContactRoleMap($tenantName);

        if ($excludeGlobalRoles) {
            return $this->getContactRoles($tenantContactRoleList);
        }

        return array_merge($this->getContactRoles($tenantContactRoleList), $this->getGlobalContactRoles());
    }

    /**
     * Returns the contact role configuration for global roles.
     *
     * @return array
     */
    public function getGlobalContactRoles(): array
    {
        return $this->getContactRoles($this->getGlobalContactRoleMap());
    }

    /**
     * Returns an array of the contact role configuration.
     * For tenant mapping purposes a list of keys may be passed for filtering.
     *
     * @param array $filterKeys
     * @return array
     */
    public function getContactRoles(array $filterKeys = []): array
    {
        $contactRoles = config('tuency.contact_roles') ?? [];

        if (empty($filterKeys)) {
            return $contactRoles;
        }

        $filteredContactRoles = [];

        foreach ($filterKeys as $filterKey) {
            if ($contactRole = config('tuency.contact_roles.' . $filterKey)) {
                $filteredContactRoles[$filterKey] = $contactRole;
            } else {
                Log::error('Unable to find contact role for key: ' . $filterKey);
            }
        }

        return $filteredContactRoles;
    }

    /**
     * Returns an array of the contact role configuration where
     * client_role specific ones are removed, if the logged-in
     * user does not have the required client role assigned.
     *
     * @return array
     */
    public function getContactRolesByLoggedInUser(): array
    {
        $userGroups = Auth::user()->getGroups();
        $contactRoles = config('tuency.contact_roles') ?? [];
        $clientRoleMap = config('tuency.contact_role_mapping.client_roles') ?? [];

        // Reducing the client role map by the roles the user has assigned.
        foreach ($userGroups as $roleName) {
            unset($clientRoleMap[$roleName]);
        }

        // Remove the role sets without a user group match.
        foreach ($clientRoleMap as $clientRoleKey => $clientContactRoles) {
            foreach ($clientContactRoles as $clientContactRole) {
                unset($contactRoles[$clientContactRole]);
            }
        }

        return $contactRoles;
    }

    /**
     * Returns the contact role tenant mapping.
     *
     * @return array
     */
    public function getContactRoleMapping(): array
    {
        $contactRoleMapping = [];

        foreach (config('tuency.contact_role_mapping.tenants') ?? [] as $tenantKey => $tenantValues) {
            $contactRoleMapping['tenants'][$this->configKeyToTenantName($tenantKey)] = $tenantValues;
        }

        foreach (config('tuency.contact_role_mapping.client_roles') ?? [] as $roleKey => $roleValues) {
            if (in_array($roleKey, Auth::user()->getGroups())) {
                $contactRoleMapping['client_roles'][$roleKey] = $roleValues;
            }
        }

        $contactRoleMapping['abuse_roles'] = config('tuency.contact_role_mapping.abuse_roles', []);

        return $contactRoleMapping;
    }

    /**
     * Returns a list of contact roles that are available for all tenants.
     *
     * @return array
     */
    public function getGlobalContactRoleMap(): array
    {
        return config('tuency.contact_role_mapping.tenants.all') ?? [];
    }

    /**
     * Returns a list of contact roles that are mapped to a specific tenant.
     *
     * @param string $tenantName
     * @return array
     */
    public function getTenantContactRoleMap(string $tenantName): array
    {
        return config('tuency.contact_role_mapping.tenants.' . $this->tenantNameToConfigKey($tenantName), []);
    }

    /**
     * Returns the list of configured contact fields and allows
     * filtering by field type.
     *
     * @param array $filterTypes
     * @return array
     */
    public function getContactFields(array $filterTypes = []): array
    {
        $contactFields = config('tuency.contact_fields', []);

        if (empty($filterTypes)) {
            return $contactFields;
        }

        return array_filter($contactFields, function ($contactField) use ($filterTypes) {
            return in_array($contactField['type'], $filterTypes) ? $contactField : false;
        });
    }

    /**
     * Checks if a provided tenant name exists.
     *
     * @param string $tenantName
     * @return bool
     */
    public function tenantNameExists(string $tenantName): bool
    {
        return !empty($this->getTenantConfigByTenantName($tenantName));
    }

    /**
     * Returns the config for a specific tenant name.
     *
     * @param string $tenantName
     * @return array|mixed
     */
    public function getTenantConfigByTenantName(string $tenantName): array
    {
        return Cache::remember(
            'tuency_tenants.' . $this->tenantNameToConfigKey($tenantName),
            self::TENANT_CACHE_SECONDS,
            function () use ($tenantName) {
                return Tenant::where('name', $tenantName)->first([
                    'tenant_id',
                    'name',
                    'from_address',
                    'from_name',
                    'reply_to'
                ])->toArray();
            }
        ) ?? [];
    }

    /**
     * Returns an array of available tenant configs.
     *
     * NOTE: Since the tenants (constituencies) are stored in the database
     * the query result is CACHED for 6 hours.
     *
     * @return array
     */
    public function getTenantConfigs(): array
    {
        return Cache::remember('tuency_tenants', self::TENANT_CACHE_SECONDS, function () {
            return Tenant::all([
                'tenant_id',
                'name',
                'from_address',
                'from_name',
                'reply_to'
            ])->toArray();
        }) ?? [];
    }

    /**
     * Returns the current contact expiry date.
     *
     * @return Carbon
     */
    public function getCurrentContactExpiryDate(): Carbon
    {
        return Carbon::now()->subDays($this->getContactExpiryInterval());
    }

    /**
     * Returns the current contact expiry notification date.
     *
     * @return Carbon
     */
    public function getCurrentContactExpiryNotificationDate(): Carbon
    {
        return Carbon::now()->subDays($this->getContactExpiryNotificationInterval());
    }

    /**
     * Returns the configured contact expiry interval in days.
     *
     * @return int
     */
    public function getContactExpiryInterval(): int
    {
        return config('tuency.notifications.mail.general.contact_expire_interval', 1);
    }

    /**
     * Returns the configured contact expiry notification interval in days.
     *
     * @return int
     */
    public function getContactExpiryNotificationInterval(): int
    {
        return config('tuency.notifications.mail.general.contact_expire_notification_interval');
    }

    /**
     * HELPERS
     */

    /**
     * Prepares the tenant name as config key to access the teuncy config.
     *
     * @param string $tenantName
     * @return string
     */
    private function tenantNameToConfigKey(string $tenantName): string
    {
        return Str::replace('.', '_', $tenantName);
    }

    /**
     * Reverses the config key to a tenant name.
     *
     * @param string $configKey
     * @return string
     */
    private function configKeyToTenantName(string $configKey): string
    {
        return Str::replace('_', '.', $configKey);
    }
}
