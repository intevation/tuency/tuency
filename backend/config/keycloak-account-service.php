<?php

return [
    'url' => env('KEYCLOAK_ACCOUNT_MGMT_URL'),
    'realm' => env('KEYCLOAK_ACCOUNT_MGMT_REALM'),
    'client_id' => env('KEYCLOAK_ACCOUNT_MGMT_CLIENT_ID'),
    'client_secret' => env('KEYCLOAK_ACCOUNT_MGMT_CLIENT_SECRET')
];
