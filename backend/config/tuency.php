<?php

return [

    /*
    |--------------------------------------------------------------------------
    | API for queries by IntelMQ
    |--------------------------------------------------------------------------
    |
    | Configuration for the intelmq lookup endpoint.
    |
    */
    'intelmq_lookup' => [

        /*
        | Defines the query token for intelmq lookups. FIXME: The tokens should be handled over OAuth2.
        |
        | Use a good random number generator to generate the value. E.g.
        |   head -c 48 /dev/urandom | base64
        */
        'query_token' => env('INTELMQ_QUERY_TOKEN'),

        /*
        | Tells the logic for lookups with IP query (networks) whether the
        | local RIPE data (*_automatic tables) should also be searched for matches.
        */
        'include_ripe_data' => env('INTELMQ_QUERY_INCLUDE_RIPE', false),

        /*
        | Indicates the score that a qualified rule receives in the weighting for the best match.
        | The score is rated if the value of the query parameter is identical to that of the rule.
        */
        'rules' => [
            'scoring' => [
                'classification' => [
                    'taxonomy' => 1,
                    'type' => 1,
                    'identifier' => 5,
                ],
                'feed' => [
                    'provider' => 2,
                    'name' => 2,
                    'code' => 6,
                ]
            ]
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Contact expiry check
    |--------------------------------------------------------------------------
    |
    | Cron to specify when and how often the reminder workflow is triggered.
    |
    */
    'reminder_execution_cron' => env('REMINDER_EXECUTION_CRON', "0 7 * * *"),

    /*
    |--------------------------------------------------------------------------
    | Notification Settings
    |--------------------------------------------------------------------------
    |
    | Allows to set general notification settings such as global admin mail
    | and expire interval.
    | Further the message subjects (currently only email) can be edited.
    */

    'notifications' => [
        'mail' => [
            'general' => [

                /*
                | This mail is notified alongside other object administrators, for example, if:
                | - A netobject is created
                */
                'admin_notification_mail' => env('ADMIN_NOTIFICATION_MAIL', 'admin@example.com'),

                /*
                 | Interval to send information update reminder.
                 */
                'contact_expire_interval' => env('MAIL_CONTACT_EXPIRE_INTERVAL', 1),

                /*
                 | Delay to the last reminder sent.
                 */
                'contact_expire_notification_interval' => env('MAIL_CONTACT_NOTIFICATION_INTERVAL', 3),
            ],

            // Email notification subjects.
            'subjects' => [

                'claim' => [
                    'created' => 'Claim created',
                    'updated' => 'Claim updated'
                ],

                'config_reminder' => [
                    'contact_information_expired' => 'Contact information expired',
                ]
            ]
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Updated by (Set keycloak user id in edited record)
    |--------------------------------------------------------------------------
    |
    | Whether to automatically set the updated_by column of models that use
    | the HasUpdatedBy trait.
    |
    | The main reason this is configurable is that this allows us to switch
    | it off in the database seeder.
    */

    'set_updated_by' => true,

    /*
    |--------------------------------------------------------------------------
    | Netobjects
    |--------------------------------------------------------------------------
    |
    | Netobject configuration.
    |
    | - rules:
    | | Settings for creating/editing netobject rules.
    | |
    | +- interval:
    | |  Defines whether and which notification intervals are available for a
    | |  rule. This setting can also be turned off completely, in which case
    | |  the default value is automatically used.
    | |
    | +- feed:
    |   |
    |   +- status:
    |   |  Defines whether and which feed statuses are available for a rule.
    |   |  As only the "production" status currently exists, this setting
    |   |  is disabled and the default value is used.
    |
    */
    'netobjects' => [
        'rules' => [
            'interval' => [
                'enabled' => true,
                'default' => [
                    'unit' => 'immediate',
                    'length' => 1,
                ],
                'units' => [
                    'immediate',
                    'hours',
                    'days',
                    'weeks',
                    'month'
                ],
            ],
            'feed' => [
                'status' => [
                    'enabled' => false,
                    'default' => 'production',
                    'options' => [
                        'production',
                        'beta'
                    ]
                ]
            ],
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Add contact fields
    |--------------------------------------------------------------------------
    |
    | If fields are to be added that are not files (PDF), the fields must be
    | added in the DB in the table 'contacts'.
    | Please note that the field name 'description' is a globally reserved key
    | that can be used in the role declaration but will be ignored in the field
    | definition.
    |
    | In the array 'contact_fields' the contact fields are configured as
    | follows:
    |
    | 'contact_fields' => [
    |      'name_of_the_field' => [
    |          'type' => 'Field type', // name_of_the_field must be the name as in the DB.
    |          'label' => 'Label for the field', // What to display as the label for the field
    |          'hint' => 'A hint text', // Hint to be displayed under the field. (optional)
    |      ],
    |      'a_dropdow_field' => [
    |          'label' => 'Label for the field', // What to display as the label for the field
    |          'searchable' => <true|false>, // Enables text search on dropdown values stored in contact table.
    |          'dropdown' => [
    |               'value1' => 'Label for the value1', // The key (value1) is what is stored in the DB
    |               'wert2' => 'Label for wert2', # The value is what is displayed
    |          ],
    |          'type' => 'Field Type', # The type must be the type of the key
    |      ],
    | ]
    |
    | The following types can be specified:
    |  - text : keine Textfeld
    |  - text_area : Großes Textfeld
    |  - phone : Telefohnnummer (string)
    |  - integer : integer
    |  - s_mime_cert : s/mime certificate
    |  - openpgp_pubkey : openpgp public key
    |  - pdf : pdf Datei
    */

    'contact_fields' => [
        'first_name' => [
            'label' => 'First Name',
            'type' => 'text',
        ],
        'last_name' => [
            'label' => 'Last Name',
            'type' => 'text',
        ],
        'label' => [
            'label' => 'label',
            'type' => 'text',
        ],
        'email' => [
            'label' => 'Email',
            'type' => 'email',
        ],
        'phone_1' => [
            'label' => 'Phone 1',
            'hint' => 'Phone should match this format without spaces: +<country code><prefix><number>',
            'type' => 'phone',
        ],
        'phone_2' => [
            'label' => 'Phone 2',
            'hint' => 'Phone should match this format without spaces: +<country code><prefix><number>',
            'type' => 'phone',
        ],
        'phone_alarm_mobil' => [
            'label' => 'Phone Alarm Mobil',
            'hint' => 'Phone should match this format without spaces: +<country code><prefix><number>',
            'type' => 'phone',
        ],
        'phone_24_7' => [
            'label' => 'Phone 24/7',
            'hint' => 'Phone should match this format without spaces: +<country code><prefix><number>',
            'type' => 'phone',
        ],
        'street' => [
            'label' => 'Street',
            'type' => 'text',
        ],
        'zip' => [
            'label' => 'ZIP',
            'type' => 'integer',
        ],
        'location' => [
            'label' => 'Location',
            'type' => 'text',
        ],
        'country' => [
            'label' => 'Country',
            'type' => 'text',
            'searchable' => true,
            'dropdown' => [
                'de' => 'Germany',
                'at' => 'Austria',
                'ch' => 'Switzerland',
            ],
        ],
        'endpoint' => [
            'label' => 'Endpoint',
            'type' => 'text',
        ],
        's_mime_cert' => [
            'label' => 'S/MIME Cert',
            'type' => 's_mime_cert',
        ],
        'openpgp_pubkey' => [
            'label' => 'OpenPGP Pubkey',
            'type' => 'openpgp_pubkey',
        ],
        'atc_code_of_conduct' => [
            'label' => 'ATC Code of Conduct',
            'type' => 'pdf',
        ],
        'aec_code_of_conduct' => [
            'label' => 'AEC Code of Conduct',
            'type' => 'pdf',
        ],
        'document' => [
            'label' => 'Document',
            'type' => 'pdf',
        ],
        'commentary' => [
            'label' => 'Commentary',
            'type' => 'text_area',
        ],
        'format' => [
            'label' => 'Format',
            'type' => 'text',
            'dropdown' => [
                'dense_csv' => 'Dense CSV',
                'full_csv' => 'Full CSV',
                'json' => 'JSON',
            ],
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Add contact roles
    |--------------------------------------------------------------------------
    |
    | The contact roles are configured in the array 'contact_roles'.
    |
    | The name of the role is given as the key. The value is an array of
    | contact fields that the role should have as a key and the value is true
    | or false to determine if the field is required. (true = required)
    |
    | The field name 'description' is a globally reserved key that might be
    | used as optional attribute for a detailed role description.
    | The field content will be displayed in the contact settings when
    | the respective role is hoovered. If no description is applied the role
    | name will be displayed as fallback value.
    | The field content supports multiline. Use "\n" for line breaks and
    | make sure to wrap the content in double quotes so PHP interprets it
    | as line feed.
    |
    | 'contact_roles' => [
    |      'First contact role' => [
    |          'last_name' => true,
    |          'first_name' => false,
    |          'description' => "First role description.\n This is a new line."
    |      ],
    |      'Second contact role' => [
    |          'email' => true,
    |          'openpgp_pubkey' => false,
    |          'description' => 'Second role description.'
    |      ],
    | ]
    |
    | Please make sure to map the role keys in the 'contact_role_mapping'
    | sections. Without a mapping the role will be globally visible for
    | filtering but not usable for any contact create or update.
    |
    |
    | NOTE: For the clients contact title display the frontend will initially
    | check if there are first_name and/or last_name properties available.
    | If a role does not require any name attributes it is recommended
    | to apply a required "label" which will then replace the title.
    | If there is no label, the frontend finally tries to use the
    | email field.
    */

    'contact_roles' => [
        'Ansprechpartner Primaer' => [
            'first_name' => true,
            'last_name' => true,
            'email' => true,
            'phone_1' => true,
            'phone_2' => false,
            'street' => false,
            'zip' => false,
            'location' => false,
            'country' => false,
            's_mime_cert' => false,
            'openpgp_pubkey' => false,
            'aec_code_of_conduct' => true,
            'document' => false,
            'commentary' => false,
            'description' => "The primary contact person of an organization.",
        ],
        'Administrator Organisation' => [
            'first_name' => true,
            'last_name' => true,
            'email' => true,
            'phone_1' => false,
            'phone_2' => false,
            'street' => false,
            'zip' => false,
            'location' => false,
            'country' => false,
            's_mime_cert' => false,
            'openpgp_pubkey' => false,
            'aec_code_of_conduct' => false,
            'document' => false,
            'commentary' => false,
            'description' => "The administrator of an organization.",
        ],
        'Ansprechpartner Stellvertreter' => [
            'first_name' => true,
            'last_name' => true,
            'email' => true,
            'phone_1' => true,
            'phone_2' => false,
            'street' => false,
            'zip' => false,
            'location' => false,
            'country' => false,
            's_mime_cert' => false,
            'openpgp_pubkey' => false,
            'aec_code_of_conduct' => true,
            'document' => false,
            'commentary' => false,
            'description' => "A deputy for the primary contact person of an organization.",
        ],
        'Single Point of Contact (SpoC)' => [
            'label' => true,
            'email' => true,
            'phone_1' => false,
            'phone_2' => false,
            'street' => false,
            'zip' => false,
            'location' => false,
            'country' => false,
            's_mime_cert' => false,
            'openpgp_pubkey' => false,
            'aec_code_of_conduct' => true,
            'document' => false,
            'commentary' => false,
            'description' => "The Single Point of Contact is a central point in an organisation that serves\n " .
                             "as the first and often only contact for service requests and faults.",
        ],
        '24/7' => [
            'label' => true,
            'email' => false,
            'phone_1' => false,
            'phone_2' => false,
            'phone_24_7' => true,
            'street' => false,
            'zip' => false,
            'location' => false,
            'country' => false,
            's_mime_cert' => false,
            'openpgp_pubkey' => false,
            'aec_code_of_conduct' => true,
            'document' => false,
            'commentary' => false,
            'description' => "A contact that can be reached any time."
        ],
        'Krisenstab' => [
            'first_name' => false,
            'last_name' => false,
            'label' => false,
            'email' => false,
            'phone_1' => false,
            'phone_2' => false,
            'street' => false,
            'zip' => false,
            'location' => false,
            'country' => false,
            's_mime_cert' => false,
            'openpgp_pubkey' => false,
            'aec_code_of_conduct' => false,
            'document' => false,
            'commentary' => false,
        ],
        'ICS Security Technik' => [
            'first_name' => true,
            'last_name' => true,
            'email' => false,
            'phone_1' => false,
            'phone_2' => false,
            'street' => false,
            'zip' => false,
            'location' => false,
            'country' => false,
            's_mime_cert' => false,
            'openpgp_pubkey' => false,
            'aec_code_of_conduct' => false,
            'document' => false,
            'commentary' => false,
        ],
        'ICS Security Organisation' => [
            'first_name' => true,
            'last_name' => true,
            'email' => false,
            'phone_1' => false,
            'phone_2' => false,
            'street' => false,
            'zip' => false,
            'location' => false,
            'country' => false,
            's_mime_cert' => false,
            'openpgp_pubkey' => false,
            'aec_code_of_conduct' => false,
            'document' => false,
            'commentary' => false,
        ],
        'IT Security Technik' => [
            'first_name' => true,
            'last_name' => true,
            'email' => false,
            'phone_1' => false,
            'phone_2' => false,
            'street' => false,
            'zip' => false,
            'location' => false,
            'country' => false,
            's_mime_cert' => false,
            'openpgp_pubkey' => false,
            'aec_code_of_conduct' => false,
            'document' => false,
            'commentary' => false,
        ],
        'IT Security Organisation' => [
            'first_name' => true,
            'last_name' => true,
            'email' => false,
            'phone_1' => false,
            'phone_2' => false,
            'street' => false,
            'zip' => false,
            'location' => false,
            'country' => false,
            's_mime_cert' => false,
            'openpgp_pubkey' => false,
            'aec_code_of_conduct' => false,
            'document' => false,
            'commentary' => false,
        ],
        'Abuse Contact' => [
            'label' => true,
            'email' => true,
            'endpoint' => false,
            's_mime_cert' => false,
            'openpgp_pubkey' => false,
            'commentary' => false,
            'format' => true,
            'description' => "A contact to inform about abuse on related netobjects.\n " .
                             "As soon as an organization has one or more contacts with this role,\n " .
                             "they are automatically suggested for the notification destinations in netobject rules.",
        ],
        'SMS Alarmierung' => [
            'first_name' => true,
            'last_name' => true,
            'phone_alarm_mobil' => true,
            'commentary' => false,
        ],
        'Mitarbeiter Single Point of Contact (SpoC)' => [
            'first_name' => true,
            'last_name' => true,
            'email' => false,
            'phone_1' => false,
            'phone_2' => false,
            'street' => false,
            'zip' => false,
            'location' => false,
            'country' => false,
            's_mime_cert' => false,
            'openpgp_pubkey' => false,
            'aec_code_of_conduct' => false,
            'document' => false,
            'commentary' => false,
        ],
        'Mitarbeiter 24/7' => [
            'first_name' => true,
            'last_name' => true,
            'email' => false,
            'phone_1' => false,
            'phone_2' => false,
            'street' => false,
            'zip' => false,
            'location' => false,
            'country' => false,
            's_mime_cert' => false,
            'openpgp_pubkey' => false,
            'aec_code_of_conduct' => false,
            'document' => false,
            'commentary' => false,
        ],
        'Plenar Einladungen' => [
            'first_name' => true,
            'last_name' => true,
            'email' => true,
            'phone_1' => false,
            'phone_2' => false,
            'street' => false,
            'zip' => false,
            'location' => false,
            'country' => false,
            's_mime_cert' => false,
            'openpgp_pubkey' => false,
            'document' => false,
            'commentary' => false,
        ],
        'NIS-Kontaktstelle' => [
            'label' => true,
            'email' => true,
            'phone_1' => false,
            'phone_2' => false,
            'street' => false,
            'zip' => false,
            'location' => false,
            'country' => false,
            's_mime_cert' => false,
            'openpgp_pubkey' => false,
            'document' => false,
            'commentary' => false,
        ],
        'MISP Account' => [
            'first_name' => false,
            'last_name' => false,
            'label' => false,
            's_mime_cert' => false,
            'openpgp_pubkey' => false,
            'commentary' => false,
        ],
        'XYZ Member' => [
            'first_name' => true,
            'last_name' => true,
            'email' => false,
            'phone_1' => false,
            'phone_2' => false,
            'atc_code_of_conduct' => true,
            's_mime_cert' => false,
            'openpgp_pubkey' => false,
            'document' => false,
            'commentary' => false,
        ],
        'Generic' => [
            'first_name' => false,
            'last_name' => false,
            'label' => false,
            'email' => false,
            'phone_1' => false,
            'phone_2' => false,
            'street' => false,
            'zip' => false,
            'location' => false,
            'country' => false,
            's_mime_cert' => false,
            'openpgp_pubkey' => false,
            'document' => false,
            'commentary' => false,
            'format' => false,
        ],
        'MRK Contact' => [
            'first_name' => true,
            'last_name' => true,
            'email' => true,
            'phone_1' => true,
            'phone_2' => false,
            'street' => false,
            'zip' => false,
            'location' => false,
            'country' => false,
            's_mime_cert' => false,
            'openpgp_pubkey' => false,
            'aec_code_of_conduct' => false,
            'document' => false,
            'commentary' => false,
        ],
        'H20 CERT Contact' => [
            'first_name' => true,
            'last_name' => true,
            'email' => true,
            'phone_1' => true,
            'phone_2' => false,
            'street' => false,
            'zip' => false,
            'location' => false,
            'country' => false,
            's_mime_cert' => false,
            'openpgp_pubkey' => false,
            'aec_code_of_conduct' => false,
            'document' => false,
            'commentary' => false,
        ],
        'Wind CERT Contact' => [
            'first_name' => true,
            'last_name' => true,
            'email' => true,
            'phone_1' => true,
            'phone_2' => false,
            'street' => false,
            'zip' => false,
            'location' => false,
            'country' => false,
            's_mime_cert' => false,
            'openpgp_pubkey' => false,
            'aec_code_of_conduct' => false,
            'document' => false,
            'commentary' => false,
        ],
        'Portal-Admin Contact' => [
            'first_name' => true,
            'last_name' => true,
            'email' => true,
            'phone_1' => true,
            'phone_2' => false,
            'street' => false,
            'zip' => false,
            'location' => false,
            'country' => false,
            's_mime_cert' => false,
            'openpgp_pubkey' => false,
            'aec_code_of_conduct' => false,
            'document' => false,
            'commentary' => false,
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Contact role mapping
    |--------------------------------------------------------------------------
    |
    | The above configured contact role keys can either be mapped for
    | availability in all or specific tenants (constituencies), such as for
    | certain keycloak client roles.
    | Further, a role can be qualified as an abuse contact role.
    |
    | + tenants:
    | | Maps the general contact role availability for all or specific tenants.
    | | These roles will be available in the contact management as long
    | | the desired tenant availability matches the related tenant
    | | of the selected organisation of the contact.
    | |
    | + client_roles:
    | | Maps the client role specific contact role availability.
    | | If the logged-in user has the required client role for one or more of
    | | the defined roles, these roles will be able for selection in a
    | | separate create context. Further only users with this client role
    | | can view/edit/delete a contact with this role.
    | |
    | + abuse_roles:
    | | Maps if a contact role qualifies as abuse contact role.
    | | As soon as one or more contacts of an organisation have been assigned
    | | to an abuse role, only these contacts are suggested as destinations in
    | | netobject rules. Otherwise, any contact of the organisation can be used
    | | as a destination.
    */

    'contact_role_mapping' => [

        /*
         | Tenant role mapping.
         |
         | From a tenant perspective, the combination of global and tenant-specific
         | roles will be available for select.
         |
         | PLEASE NOTE: The tenant keys must match the tenant.name field in the
         | database whereby dots are NOT allowed and must be replaced with
         | underscores. E.g.: 'cert.mrk' => 'cert_mrk'
         */
        'tenants' => [

            // All tenants (Constituencies)
            'all' => [
                'Ansprechpartner Primaer',
                'Administrator Organisation',
                'Ansprechpartner Stellvertreter',
                'Single Point of Contact (SpoC)','24/7',
                'Krisenstab','ICS Security Technik',
                'ICS Security Organisation',
                'IT Security Technik',
                'IT Security Organisation',
                'Abuse Contact',
                'SMS Alarmierung',
                'Mitarbeiter Single Point of Contact (SpoC)','Mitarbeiter 24/7',
                'Plenar Einladungen',
                'NIS-Kontaktstelle',
                'MISP Account',
                'XYZ Member',
                'Generic',
            ],

            // Tenant (Constituency) specific
            'cert_mrk' => [
                'MRK Contact'
            ],

            'h20cert_mrk' => [
                'H20 CERT Contact'
            ],

            'windcert_mrk' => [
                'Wind CERT Contact'
            ]
        ],

        /*
         | Keycloak client role specific contact roles.
         |
         | NOTE: This configuration has been introduced to fulfill the requirements
         | of the ATC data-management standard which expects certain contacts and
         | roles to be only accessible by admin users.
         | In order to remain open for possible extensions, the configuration of
         | several client roles is already permitted. However, only contact roles
         | for the ‘portaladmin’ client role are currently taken into account.
         |
         */
        'client_roles' => [
            'portaladmin' => [
                'Portal-Admin Contact'
            ]
        ],

        /*
        | Abuse contact roles.
        |
        | By adding a role key to "abuse_roles" list, contacts with these roles are
        | automatically suggested as notification destinations within netobject rules.
        | Please note that contact roles may not be available for every tenant
        | due to the selected configuration in the above “tenants” section.
        | If no abuse role is available in the active tenant, all contacts of the
        | organization that owns the netobject are suggested.
        */
        'abuse_roles' => [
            'Abuse Contact',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Tuency seeders
    |--------------------------------------------------------------------------
    |
    | Seeding amount of tuency entities.
    */
    'seeders' => [
        'organisations' => [
            'count' => env('SEEDER_NUMBER_OF_ORGS', 10),

            'contacts' => [
                'count' => env('SEEDER_NUMBER_OF_CONTACTS_PER_ORGA', 3)
            ],

            'networks' => [
                'count' => env('SEEDER_NUMBER_OF_NETWORKS_PER_ORGA', 3),
                'rules' => [
                    'count' => env('SEEDER_NUMBER_OF_NETWORK_RULES_PER_NETWORK', 1)
                ]
            ]
        ],
    ]
];
