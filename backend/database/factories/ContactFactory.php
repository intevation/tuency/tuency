<?php

namespace Database\Factories;

use App\Models\Contact;
use Illuminate\Database\Eloquent\Factories\Factory;

class ContactFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Contact::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'email' => $this->faker->safeEmail,
            'last_name' => $this->faker->lastName,
            'first_name' => $this->faker->firstName,
            'phone_1' => $this->faker->regexify('[0-9]{11}'),
            'roles' => json_encode(['Generic']),
        ];
    }
}
