<?php

namespace Database\Factories;

use App\Models\Network;
use Illuminate\Database\Eloquent\Factories\Factory;

class NetworkFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Network::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    private $usedIps = [];

    public function definition()
    {
        $a = ['approval' => $this->faker->randomElement(['pending', 'approved'])];
        if (rand(0, 1) == 0) {
            $a['address'] = $this->ip4();
        } else {
            $a['address'] = $this->faker->unique()->ipv6();
        }
        return $a;
    }

    private function ip4()
    {
        do {
            $suffix = rand(8, 32);
            $block = '';
            $cidr = '';
            for ($i = 1; $i <= $suffix; $i++) {
                if ($i % 8 == 0) {
                    $block = $block . $this->faker->randomElement(['0', '1']);
                    if ($i != 32) {
                        $cidr = $cidr . bindec($block) . '.';
                    }
                    $block = '';
                } else {
                    $block = $block . $this->faker->randomElement(['0', '1']);
                }
            }

            if ($block != '') {
                while (strlen($block) < 8) {
                    $block = $block . '0';
                }
            }
            $cidr = $cidr . bindec($block) . '/' . $suffix;
        } while (in_array($cidr, $this->usedIps));
        array_push($this->usedIps, $cidr);
        return $cidr;
    }
}
