<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new Class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('organisation_tenant', function (Blueprint $table) {
            $table->foreignId('tenant_id')
                ->constrained('tenant', 'tenant_id');
            $table->index('tenant_id');
            $table->foreignId('organisation_id')
                ->constrained('organisation', 'organisation_id');
            $table->index('organisation_id');

            $table->unique(['tenant_id', 'organisation_id']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('organisation_tenant');
    }
};
