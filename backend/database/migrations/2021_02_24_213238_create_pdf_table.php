<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new Class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pdf', function (Blueprint $table) {
            $table->id('pdf_id');
            $table->text('name');
            $table->binary('pdf');
        });

        Schema::table('contact', function (Blueprint $table) {
            $table->dropColumn('pdf');
            $table->foreignId('pdf_id')
                ->nullable()
                ->constrained('pdf', 'pdf_id');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {

        Schema::table('contact', function (Blueprint $table) {
            $table->dropForeign('contact_pdf_id_foreign');
            $table->dropColumn('pdf_id');
            $table->binary('pdf')->nullable();
        });

        Schema::dropIfExists('pdf');
    }
};
