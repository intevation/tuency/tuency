<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new Class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create(
            'contact_tag',
            function (Blueprint $table) {
                $table->id("contact_tag_id");
                $table->text('name')->unique();
                $table->boolean('visible');
            }
        );

        Schema::create('contact_contact_tag', function (Blueprint $table) {
            $table->foreignId('contact_id')
                ->constrained('contact', 'contact_id');
            $table->foreignId('contact_tag_id')
                ->constrained('contact_tag', 'contact_tag_id');
            $table->unique(['contact_id', 'contact_tag_id']);
        });

        Schema::create('contact_tag_tenant', function (Blueprint $table) {
            $table->foreignId('contact_tag_id')
                ->constrained('contact_tag', 'contact_tag_id');
            $table->foreignId('tenant_id')
                ->constrained('tenant', 'tenant_id');
            $table->unique(['contact_tag_id', 'tenant_id']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('contact_tag_tenant');
        Schema::dropIfExists('contact_contact_tag');
        Schema::dropIfExists('contact_tag');
    }
};
