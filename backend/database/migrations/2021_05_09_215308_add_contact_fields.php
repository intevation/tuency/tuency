<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new Class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('contact', function (Blueprint $table) {
            DB::table('contact')->where('email', null)->update(['email' => '']);
            $table->text('email')->nullable($value = false)->change();
            DB::table('contact')->where('phone', null)->update(['phone' => '']);
            $table->text('phone')->nullable($value = false)->change();
            DB::table('contact')->where('uri', null)->update(['uri' => '']);
            $table->text('uri')->nullable($value = false)->change();
            DB::table('contact')->where('pgp', null)->update(['pgp' => '']);
            $table->text('pgp')->nullable($value = false)->change();
            DB::table('contact')->where('commentary', null)->update(['commentary' => '']);
            $table->text('commentary')->nullable($value = false)->change();

            $table->text('first_name')->default('');
            $table->text('role')->default('');
            $table->text('description')->default('');
            $table->text('phone_2')->default('');
            $table->text('tel_alarm_mobil')->default('');
            $table->text('telefon_24_7')->default('');
            $table->text('street')->default('');
            $table->integer('zip')->nullable();
            $table->text('location')->default('');
            $table->text('country')->default('');
            $table->text('s_mime')->default('');
        });

        Schema::table('pdf', function (Blueprint $table) {
            $table->text('type');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('contact', function (Blueprint $table) {
            $table->text('email')->nullable()->change();
            $table->text('phone')->nullable()->change();
            $table->text('uri')->nullable()->change();
            $table->text('pgp')->nullable()->change();
            $table->text('commentary')->nullable()->change();

            $table->dropColumn('first_name');
            $table->dropColumn('role');
            $table->dropColumn('description');
            $table->dropColumn('phone_2');
            $table->dropColumn('tel_alarm_mobil');
            $table->dropColumn('telefon_24_7');
            $table->dropColumn('street');
            $table->dropColumn('zip');
            $table->dropColumn('location');
            $table->dropColumn('country');
            $table->dropColumn('s_mime');
        });

        Schema::table('pdf', function (Blueprint $table) {
            $table->dropColumn('type');
        });
    }
};
