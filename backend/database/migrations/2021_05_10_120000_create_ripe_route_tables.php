<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new Class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        /* route_automatic.
         *
         * Laravel doesn't support the cidr type, so we create this with a raw
         * SQL statement.
         */
        DB::statement("
            CREATE TABLE route_automatic (
                route_automatic_id SERIAL PRIMARY KEY,
                address cidr NOT NULL,
                asn bigint NOT NULL,
                import_source VARCHAR(500) NOT NULL CHECK (import_source <> ''),
                import_time TIMESTAMP NOT NULL,

                UNIQUE (address, asn, import_source)
            );
        ");

        DB::statement("
            CREATE INDEX route_automatic_address_index ON route_automatic
                   USING gist (address inet_ops);
        ");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('route_automatic');
    }
};
