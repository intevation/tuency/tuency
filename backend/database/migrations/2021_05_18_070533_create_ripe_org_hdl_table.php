<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new Class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('ripe_org_hdl', function (Blueprint $table) {
            $table->id("ripe_org_hdl_id");
            $table->foreignId('organisation_id')
            ->constrained('organisation', 'organisation_id');
            $table->text('ripe_org_hdl');
            $table->enum('approval', ['pending', 'approved', 'denied']);
            $table->unique(['organisation_id', 'ripe_org_hdl']);
            $table->index('ripe_org_hdl');
            $table->index('approval');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('ripe_org_hdl');
    }
};
