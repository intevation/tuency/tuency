<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new Class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('contact', function (Blueprint $table) {
            $table->renameColumn('tel_alarm_mobil', 'phone_alarm_mobil');
            $table->renameColumn('telefon_24_7', 'phone_24_7');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('contact', function (Blueprint $table) {
            $table->renameColumn('phone_alarm_mobil', 'tel_alarm_mobil');
            $table->renameColumn('phone_24_7', 'telefon_24_7');
        });
    }
};
