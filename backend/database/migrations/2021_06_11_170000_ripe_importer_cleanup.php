<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new Class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('contact_network_automatic_rule', function (Blueprint $table) {
            $table->dropForeign(['network_automatic_rule_id']);
            $table->foreign('network_automatic_rule_id')
                ->references('network_automatic_rule_id')
                ->on('network_automatic_rule')
                ->onDelete('cascade');
        });

        DB::statement("
CREATE OR REPLACE FUNCTION tuency_ripe_cleanup()
RETURNS void AS
$$
BEGIN

DELETE FROM network_automatic_rule r
 WHERE NOT EXISTS
       (SELECT 1 FROM network_automatic na
                 JOIN organisation_to_network_automatic ona
                USING (network_automatic_id)
                 JOIN organisation_automatic oa
                USING (organisation_automatic_id)
         WHERE na.address = r.address
           AND na.import_source = r.import_source
           AND oa.ripe_org_hdl IN (SELECT h.ripe_org_hdl
                                     FROM ripe_org_hdl h
                                    WHERE h.organisation_id = r.organisation_id
				      AND h.approval = 'approved'));

END;
$$ LANGUAGE plpgsql;");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::statement("DROP FUNCTION tuency_ripe_cleanup();");

        Schema::table(
            'contact_network_automatic_rule',
            function (Blueprint $table) {
                $table->dropForeign(['network_automatic_rule_id']);
                $table->foreign('network_automatic_rule_id')
                    ->references('network_automatic_rule_id')
                    ->on('network_automatic_rule');
            }
        );
    }
};
