<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new Class extends Migration
{
    private static $tableNames = [
        'asn',
        'contact',
        'fqdn',
        'fqdn_rule',
        'global_rule',
        'network',
        'network_automatic_rule',
        'network_rule',
        'organisation',
        'organisation_rule',
        'pdf',
        'ripe_org_hdl',
        'user',
    ];

    /**
     * Run the migrations.
     */
    public function up(): void
    {
        foreach (static::$tableNames as $tableName) {
            Schema::table($tableName, function (Blueprint $table) {
                $table->uuid('updated_by')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        foreach (static::$tableNames as $tableName) {
            Schema::table($tableName, function (Blueprint $table) {
                $table->dropColumn('updated_by');
            });
        }
    }
};
