<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new Class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('changelog', function (Blueprint $table) {
            $table->id('changelog_id');
            $table->dateTime('changed_at')->useCurrent();
            $table->uuid('changed_by');
            $table->string('changed_table', 64);
            $table->bigInteger('changed_id');
            $table->char('action', 1);
            $table->jsonb('details');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('changelog');
    }
};
