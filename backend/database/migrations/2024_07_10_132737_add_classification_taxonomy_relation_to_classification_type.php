<?php

use App\Models\ClassificationTaxonomy;
use App\Models\ClassificationType;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('classification_type', function (Blueprint $table) {
            $table->foreignId('classification_taxonomy_id')
                ->default(-1)
                ->constrained('classification_taxonomy', 'classification_taxonomy_id');
        });

        $this->mapPreviousExistingTypesToUnknown();
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('classification_type', function (Blueprint $table) {
            $table->dropForeign('classification_type_classification_taxonomy_id_foreign');
            $table->dropColumn('classification_taxonomy_id');
        });
    }

    /**
     * Determines if there are existing entries for classification_types available.
     * If yes, a new taxonomy with name 'unknown' will be created and all existing types
     * are mapped to this taxonomy.
     *
     * @return void
     */
    private function mapPreviousExistingTypesToUnknown(): void
    {
        $previousTypes = ClassificationType::where('name', '!=', 'any')
        ->where('classification_taxonomy_id', '=', -1)->get();

        if ($previousTypes->count() > 0) {
            $unknownTaxonomy = ClassificationTaxonomy::updateOrCreate(['name' => 'unknown']);

            foreach ($previousTypes as $type) {
                $type->classification_taxonomy_id = $unknownTaxonomy->classification_taxonomy_id;
                $type->save();
            }
        }
    }
};
