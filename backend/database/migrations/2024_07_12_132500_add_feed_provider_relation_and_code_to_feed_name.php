<?php

use App\Models\FeedName;
use App\Models\FeedProvider;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('feed_name', function (Blueprint $table) {
            $table->foreignId('feed_provider_id')->default(-1)
                ->constrained('feed_provider', 'feed_provider_id');

            $table->text('code')->nullable()->unique();
        });

        $this->mapPreviousExistingFeedsToUnknown();
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('feed_name', function (Blueprint $table) {
            $table->dropForeign('feed_name_feed_provider_id_foreign');
            $table->dropColumn('feed_provider_id');

            $table->dropColumn('code');
        });
    }

    /**
     * Determines if there are existing entries for feed_names available.
     * If yes, a new feed_provider with name 'unknown' will be created and all existing feeds
     * are mapped to this provider.
     *
     * @return void
     */
    private function mapPreviousExistingFeedsToUnknown(): void
    {
        $previousFeeds = FeedName::where('name', '!=', 'any')
            ->where('feed_provider_id', '=', -1)->get();

        if ($previousFeeds->count() > 0) {
            $unknownProvider = FeedProvider::updateOrCreate(['name' => 'unknown']);

            foreach ($previousFeeds as $type) {
                $type->feed_provider_id = $unknownProvider->feed_provider_id;
                $type->save();
            }
        }
    }
};
