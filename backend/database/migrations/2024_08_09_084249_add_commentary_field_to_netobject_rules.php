<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('asn_rule', function (Blueprint $table) {
            $table->text('commentary')->nullable();
        });

        Schema::table('fqdn_rule', function (Blueprint $table) {
            $table->text('commentary')->nullable();
        });

        Schema::table('network_rule', function (Blueprint $table) {
            $table->text('commentary')->nullable();
        });

        Schema::table('organisation_rule', function (Blueprint $table) {
            $table->text('commentary')->nullable();
        });

        Schema::table('global_rule', function (Blueprint $table) {
            $table->text('commentary')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('asn_rule', function (Blueprint $table) {
            $table->dropColumn('commentary');
        });

        Schema::table('fqdn_rule', function (Blueprint $table) {
            $table->dropColumn('commentary');
        });

        Schema::table('network_rule', function (Blueprint $table) {
            $table->dropColumn('commentary');
        });

        Schema::table('organisation_rule', function (Blueprint $table) {
            $table->dropColumn('commentary');
        });

        Schema::table('global_rule', function (Blueprint $table) {
            $table->dropColumn('commentary');
        });
    }
};
