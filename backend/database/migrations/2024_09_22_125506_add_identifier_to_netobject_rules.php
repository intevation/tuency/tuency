<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('asn_rule', function (Blueprint $table) {
            $table->string('identifier', 100)->nullable();
        });

        Schema::table('fqdn_rule', function (Blueprint $table) {
            $table->string('identifier', 100)->nullable();
        });

        Schema::table('network_rule', function (Blueprint $table) {
            $table->string('identifier', 100)->nullable();
        });

        Schema::table('organisation_rule', function (Blueprint $table) {
            $table->string('identifier', 100)->nullable();
        });

        Schema::table('global_rule', function (Blueprint $table) {
            $table->string('identifier', 100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('netobject_rules', function (Blueprint $table) {
            Schema::table('asn_rule', function (Blueprint $table) {
                $table->dropColumn('identifier');
            });

            Schema::table('fqdn_rule', function (Blueprint $table) {
                $table->dropColumn('identifier');
            });

            Schema::table('network_rule', function (Blueprint $table) {
                $table->dropColumn('identifier');
            });

            Schema::table('organisation_rule', function (Blueprint $table) {
                $table->dropColumn('identifier');
            });

            Schema::table('global_rule', function (Blueprint $table) {
                $table->dropColumn('identifier');
            });
        });
    }
};
