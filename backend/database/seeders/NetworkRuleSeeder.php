<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Network;
use App\Models\NetworkRule;
use Illuminate\Support\Facades\DB;

class NetworkRuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Disable automatic updates of the updated_by column in the seeder.
        // The seeder does not run with a specific Keycloak user, so we cannot
        // meaningfully set that column.
        config(['tuency.set_updated_by' => false]);

        // Disable auditlog. The seeder does not run with a specific Keycloak
        // user, so logging wouldn't work.
        config(['tuency.auditlog' => []]);

        $numNetworkRules = config('tuency.seeders.organisations.networks.rules.count');
        foreach (Network::all() as $network) {
            $networkRules = NetworkRule::factory()
                ->count($numNetworkRules)
                ->for($network)
                ->create();

            // Add contacts to the NetworkRule.
            foreach ($networkRules as $networkRule) {
                $contactIds = $network->organisation->contacts()
                    ->pluck('contact_id')
                    ->toArray();

                // Random number of contacts to be appended.
                if (count($contactIds) < 3) {
                    $contactsNum = rand(1, count($contactIds));
                } else {
                    $contactsNum = rand(1, 3);
                }
                $contactIdKeays = array_rand($contactIds, $contactsNum);

                if ($contactsNum > 1) {
                    foreach ($contactIdKeays as $contactIdKeay) {
                        DB::table('contact_network_rule')
                            ->insert([
                                'network_rule_id' => $networkRule->network_rule_id,
                                'contact_id' => $contactIds[$contactIdKeay],
                            ]);
                    }
                } else {
                    DB::table('contact_network_rule')
                        ->insert([
                            'network_rule_id' => $networkRule->network_rule_id,
                            'contact_id' => $contactIds[$contactIdKeays],
                        ]);
                }
            }
        }
    }
}
