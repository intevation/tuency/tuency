<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2020, 2021 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2020, 2021 Intevation GmbH <https://intevation.de>
 *
 * @author  2020 Bernhard Herzog <bernhard.herzog@intevation.de>
 */

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Organisation;
use Illuminate\Support\Facades\DB;

class OrganisationSeeder extends Seeder
{
    /**
     * Create a few random organisations in the database.
     *
     * The number is taken from the environment variable SEEDER_NUMBER_OF_ORGS
     * defaulting to 10.
     *
     * @example  SEEDER_NUMBER_OF_ORGS=2 php artisan db:seed
     *
     * @return void
     */
    public function run()
    {
        // Disable automatic updates of the updated_by column in the seeder.
        // The seeder does not run with a specific Keycloak user, so we cannot
        // meaningfully set that column.
        config(['tuency.set_updated_by' => false]);

        // Disable auditlog. The seeder does not run with a specific Keycloak
        // user, so logging wouldn't work.
        config(['tuency.auditlog' => []]);

        $numOrgs = config('tuency.seeders.organisations.count');
        $numContacts = config('tuency.seeders.organisations.contacts.count');
        $numNetworks = config('tuency.seeders.organisations.networks.count');

        $orgs = Organisation::factory()
            ->times($numOrgs)
            ->hasContacts($numContacts)
            ->hasNetworks($numNetworks)
            ->create();

        // Gets tenants Ids.
        $tenants = DB::table('tenant')->pluck('tenant_id')->toArray();

        // Give the tenants random organisations.
        foreach ($orgs as $orga) {
            $tenantsNum = rand(1, count($tenants));
            $tenantsIdKeys = array_rand($tenants, $tenantsNum);

            if ($tenantsNum > 1) {
                // If an organization belongs to multiple tenants.
                foreach ($tenantsIdKeys as $tenantsIdkey) {
                    DB::table('organisation_tenant')
                        ->insert([
                            'tenant_id' => $tenants[$tenantsIdkey],
                            'organisation_id' => $orga->organisation_id,
                        ]);
                }
            } else {
                // If an organization belongs to one tenant.
                DB::table('organisation_tenant')
                    ->insert([
                        'tenant_id' => $tenants[$tenantsIdKeys],
                        'organisation_id' => $orga->organisation_id,
                    ]);
            }
        }

        if (count($orgs) >= 2) {
            $numSubOrgs = intdiv(count($orgs), 3);
            for ($i = 0; $i < $numSubOrgs; $i++) {
                $pair = $orgs->random(2);
                $org1 = $pair[0];
                $org2 = $pair[1];

                // Make sure to choose the organisation with the lower
                // ID as the parent to avoid creating cycles in the
                // graph.
                if ($org1->organisation_id < $org2->organisation_id) {
                    $parent = $org1;
                    $child = $org2;
                } else {
                    $parent = $org2;
                    $child = $org1;
                }

                $child->parent_id = $parent->organisation_id;
                $child->save();
                // Update the TenantsIds of the child with the TenantsIds of the
                // parents.
                DB::table('organisation_tenant')
                    ->where('organisation_id', '=', $child->organisation_id)
                    ->delete();

                $tenantsIds = DB::table('organisation_tenant')
                    ->where('organisation_id', '=', $child->parent_id)
                    ->pluck('tenant_id');
                foreach ($tenantsIds as $tenantsId) {
                    DB::table('organisation_tenant')
                        ->insert([
                            'tenant_id' => $tenantsId,
                            'organisation_id' => $child->organisation_id,
                        ]);
                }
            }
        }
    }
}
