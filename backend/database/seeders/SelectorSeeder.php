<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2020, 2021 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2020, 2021 Intevation GmbH <https://intevation.de>
 *
 * @author  2020 Bernhard Herzog <bernhard.herzog@intevation.de>
 */

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ClassificationType;
use App\Models\ClassificationTaxonomy;
use App\Models\FeedProvider;
use App\Models\FeedName;

/**
 * Values for classification taxonomy and types taken from:
 * https://github.com/enisaeu/Reference-Security-Incident-Taxonomy-Task-Force/blob/master/working_copy/machinev1
 *
 * Example values for feed providers and names derived from:
 * https://www.cert.at/de/services/daten-feeds/vulnerable/
 */
class SelectorSeeder extends Seeder
{
    /**
     * The keys represent the classification taxonomy "predicate" argument.
     * The array values represent the "entry.value" argument of the incident types.
     *
     *
     * @var array[]
     */
    private static $classificationMap = [
        'abusive-content' => [
            'spam',
            'harmful-speech',
            'violence'
        ],

        'malicious-code' => [
            'infected-system',
            'c2-server',
            'malware-distribution',
            'malware-configuration',
        ],

        'information-gathering' => [
            'scanner',
            'sniffing',
            'social-engineering',
        ],

        'intrusion-attempts' => [
            'ids-alert',
            'brute-force',
            'exploit',
        ],

        'intrusions' => [
            'privileged-account-compromise',
            'unprivileged-account-compromise',
            'application-compromise',
            'system-compromise', // NEW
            'burglary'
        ],

        'availability' => [
            'dos',
            'ddos',
            'misconfiguration', //NEW
            'sabotage',
            'outage',
        ],

        'information-content-security' => [
            'unauthorised-information-access',
            'unauthorised-information-modification',
            'data-loss',
            'data-leak'
        ],

        'fraud' => [
            'unauthorised-use-of-resources', //NED
            'copyright',
            'masquerade',
            'phishing',
        ],

        'vulnerable' => [
            'weak-crypto',
            'ddos-amplifier',
            'potentially-unwanted-accessible',
            'information-disclosure',
            'vulnerable-system',
        ],

        'other' => [
            'other',
            'undetermined' // NEW
        ],

        'test' => [
            'test'
        ],
    ];

    /**
     * The keys are the name of the feed provider.
     * The array values represent a feed with name and code.
     *
     * @var array[]
     */
    private static $feedMap = [
        'ShadowServer' => [
            [
                'name' => 'Accessible FTP',
                'code' => 'accessible-ftp'
            ],
            [
                'name' => 'Open LDAP',
                'code' => 'open-ldap'
            ],
            [
                'name' => 'Vulnerable HTTP',
                'code' => 'vulnerable-http'
            ]
        ],
        'Team Cymru' => [
             [
                 'name' => 'NTP Version',
                 'code' => 'ntp-version'
             ]
        ],
    ];

    public function run()
    {
        // Disable automatic updates of the updated_by column in the seeder.
        // The seeder does not run with a specific Keycloak user, so we cannot
        // meaningfully set that column.
        config(['tuency.set_updated_by' => false]);

        // Disable auditlog. The seeder does not run with a specific Keycloak
        // user, so logging wouldn't work.
        config(['tuency.auditlog' => []]);

        // Updating fixed id "-1" for no specific taxonomy or feed declaration
        ClassificationType::find(-1)->update(['classification_taxonomy_id' => -1]);
        FeedName::find(-1)->update(['feed_provider_id' => -1]);

        // Seed or re-seed classification taxonomies and types by map.
        foreach (static::$classificationMap as $taxonomyKey => $types) {
            $taxonomy = ClassificationTaxonomy::updateOrCreate(['name' => $taxonomyKey]);


            foreach ($types as $type) {
                ClassificationType::updateOrCreate([
                    'name' => $type
                ])->update(['classification_taxonomy_id' => $taxonomy->classification_taxonomy_id]);
            }
        }

        // Seed or re-seed feed provider and names by map.
        foreach (static::$feedMap as $providerKey => $feeds) {
            $provider = FeedProvider::updateOrCreate(['name' => $providerKey]);

            foreach ($feeds as $feed) {
                FeedName::updateOrCreate([
                    'name' => $feed['name'],
                ])->update([
                    'code' => $feed['code'],
                    'feed_provider_id' => $provider->feed_provider_id
                ]);
            }
        }
    }
}
