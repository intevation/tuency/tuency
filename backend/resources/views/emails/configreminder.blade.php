Hello {{ $fullname }},<br>
<br>
in the <b>"{{ $organisationName }}"</b> organization, <b>{{ $expiryCount }} expired contact(s)</b> were identified at the current time.<br>
<br>
This affects the following roles:<br>
<ul>
@foreach($affectedRoles as $affectedRole)
    <li>{{ $affectedRole }}</li>
@endforeach
</ul>
Please visit the Tuency application and check if the information is still correct.<br>
If no changes are required, validate the respective contact by confirming it without change.
