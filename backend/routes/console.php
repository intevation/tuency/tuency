<?php

/**
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2020 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2020 Intevation GmbH <https://intevation.de>
 *
 * @author: 2021 Bernhard Reiter <bernhard.reiter@intevation.de>
 */

use App\Http\Controllers\KeycloakUsersController;
use App\Models\Tenant;
use App\Models\Organisation;
use App\Models\User;
use Faker\Generator;
use Illuminate\Container\Container;
use Illuminate\Support\Facades\Artisan;
use Scito\Laravel\Keycloak\Admin\Facades\KeycloakAdmin;

/**
 * create test accounts
 *
 * Associate each test account with a node, picking the nodes in order
 * from a big list starting with portaladmin + all tenants + all orgs
 * so users are distributed over the exiting nodes
 */
Artisan::command('users:seed {number=10} {emailPattern=dummy}', function ($number, $emailPattern) {
    // Disable the auditlog and automatic updates of the updated_by column.
    // This command does not run with a specific Keycloak user, so logging
    // would fail and we cannot meaningfully set the updated_by column. Since
    // this script is intended for testusers, disabling logging and updated_by
    // should be fine.
    config(['tuency.set_updated_by' => false]);
    config(['tuency.auditlog' => []]);

    // A default value with '}' does not work, nor does making it optional,
    // so we use 'dummy'
    if ($emailPattern == 'dummy') {
        $emailPattern = 'example+t{$i}@tuency-test.ntvtn.de';
    }
    $this->comment("Attempting to create $number test user accounts...");
    $this->comment("with emailPattern='$emailPattern'...");

    $tenants = Tenant::all();
    $numberOfTenants = count($tenants);
    $this->comment("Found $numberOfTenants constituencies.");

    $orgs = Organisation::all();
    $numberOfOrgs = count($orgs);
    $this->comment("Found $numberOfOrgs organisations.");

    /* extracted from
     * App\Http\Controllers\KeycloakUsersController
     * as the parts needed here cannot be easily factored out into common
     * functions
     */

    $realm = KeycloakAdmin::realm(env('KEYCLOAK_ADMIN_TUENCY_REALM'));
    $users = $realm->users();

    $clientId = env('KEYCLOAK_ADMIN_TUENCY_CLIENT_ID');
    $clientUUID = null;
    foreach ($realm->clients()->all()->toArray() as $potentialClient) {
        if ($potentialClient->getClientId() === $clientId) {
            $clientUUID = $potentialClient->getId();
            break;
        }
    }
    // this is different from using $potentialClient from above directly, why?
    $client = $realm->client($clientUUID);
    $this->comment('found client ' . $client->getId());
    $clientRoles = $client->roles();

    /* use the Faker from the container to have the overall language settings */
    $faker = Container::getInstance()->make(Generator::class);

    for ($i = 1; $i <= $number; $i++) {
        $emailAddress = preg_replace('/{\$i}/', $i, $emailPattern);

        $this->comment("creating '$emailAddress'");

        $creator = $users->create(["username" => $emailAddress,
                                   "email" => $emailAddress,
                                   "firstName" => $faker->firstName,
                                   "lastName" => $faker->lastName,
                                   "enabled" => true]);
        $newUser = $creator->save();

        // determine level and node_id
        // we repeately create them in order portaladm, tenantadmins, orgaadmins
        $remainder = ($i - 1) % (1 + $numberOfTenants + $numberOfOrgs);

        $nodeId = null;
        if ($remainder === 0) {
                $level = 'portaladmin';
        } elseif ($remainder <= $numberOfTenants) {
                $level = 'tenantadmin';
                $nodeId = $remainder;
        } else {
                $level = 'orgaadmin';
                $nodeId = $remainder - $numberOfTenants;
        }
        $this->comment("level $level nodeId $nodeId");

        $userClientRoles = $newUser->roles()->client($client->getId());
        $role = $clientRoles->getByName($level);
        $userClientRoles->add($role->toRepresentation());

        $userModel = User::create([
            'keycloak_user_id' => $newUser->getId(),
        ]);
        if ($level === 'tenantadmin') {
            $userModel->tenants()->sync([$nodeId]);
        } elseif ($level === 'orgaadmin') {
            $userModel->organisations()->sync([$nodeId]);
        }
    }
})->purpose('Create test users. \'{$i}\' in email patterns' .
            ' gets expanded.');
