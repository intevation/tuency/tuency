<?php

namespace Tests\Feature;

//use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ExampleTest extends TestCase
{
    /**
     *  A very simple test to an unauthorized development-api endpoint
     */
    public function testBasicTest(): void
    {
        $response = $this->get('/api/uptest');
        $response->assertStatus(200);
    }
}
