<?php

namespace Tests\Unit;

use App\Services\TuencyConfigService;
use Tests\TestCase;

class TuencyConfigTest extends TestCase
{
    private TuencyConfigService $configService;

    public function setUp(): void
    {
        parent::setUp();
        $this->configService = new TuencyConfigService();
    }

    public function testContactRoleConfigByTenantNameMergesSpecificAndGlobalRules() {
        $tenantRoles = $this->configService->getContactRolesByTenantName('cert.mrk');

        $this->assertArrayHasKey('MRK Contact', $tenantRoles);
        $this->assertArrayHasKey('Generic', $tenantRoles);
    }

    public function testContactRoleConfigByTenantNameCanExcludeGlobalRoles()
    {
        $tenantRoles = $this->configService->getContactRolesByTenantName('cert.mrk', true);
        $this->assertTrue(count($tenantRoles) == 1);
        $this->assertArrayHasKey('MRK Contact', $tenantRoles);
    }

    public function testGetGlobalContactRoleConfigDeliversRolesForAllTenants()
    {
        $globalRoles = $this->configService->getGlobalContactRoles();

        $this->assertArrayNotHasKey('MRK Contact', $globalRoles);
        $this->assertArrayHasKey('Generic', $globalRoles);
    }

    public function testGetContactRoleConfigContainsGenericRole()
    {
        $this->assertArrayHasKey('Generic', $this->configService->getContactRoles());
    }

    public function testGetContactRoleConfigDeliversCorrectFilteredResult()
    {
        $filteredRoleConfig = $this->configService->getContactRoles([
            'Non-Existing Contact Role',
            'Single Point of Contact (SpoC)',
            '24/7',
            'Generic'
        ]);

        $this->assertArrayNotHasKey('Non-Existing Contact Role', $filteredRoleConfig);
        $this->assertArrayHasKey('Single Point of Contact (SpoC)', $filteredRoleConfig);
        $this->assertArrayHasKey('24/7', $filteredRoleConfig);
        $this->assertArrayHasKey('Generic', $filteredRoleConfig);
    }

    public function testGetContactRoleConfigReturnsArray()
    {
        $this->assertIsArray($this->configService->getContactRoles());
    }

    public function testGetTenantConfigByTenantName()
    {
        $this->assertTrue(
            $this->configService->getTenantConfigByTenantName('cert.mrk')['name'] === 'cert.mrk'
        );
    }

    public function testGetTenantsReturnsArray()
    {
        $this->assertIsArray($this->configService->getTenantConfigs());
    }

    public function testGetCurrentContactExpiryDateIsInPast()
    {
        $this->assertTrue($this->configService->getCurrentContactExpiryDate()->isPast());
    }

    public function testGetCurrentContactExpiryNotificationDateIsInPast()
    {
        $this->assertTrue($this->configService->getCurrentContactExpiryNotificationDate()->isPast());
    }

    public function testGetContactExpiryIntervalReturnsInt()
    {
        $this->assertIsInt($this->configService->getContactExpiryInterval());
    }

    public function testGetContactExpiryNotificationIntervalReturnsInt()
    {
        $this->assertIsInt($this->configService->getContactExpiryNotificationInterval());
    }

    public function testNetobjectsHaveAValidRuleConfiguration()
    {
        $netObjectConfig = config('tuency.netobjects.rules');

        // Main nodes
        $this->assertArrayHasKey('interval', $netObjectConfig);
        $this->assertArrayHasKey('feed', $netObjectConfig);

        // Interval
        $this->assertArrayHasKey('default', $netObjectConfig['interval']);

        // Interval / Default / Unit
        $this->assertArrayHasKey('unit', $netObjectConfig['interval']['default']);
        $this->assertIsString($netObjectConfig['interval']['default']['unit']);

        // Interval / Default / Length
        $this->assertArrayHasKey('length', $netObjectConfig['interval']['default']);
        $this->assertIsInt($netObjectConfig['interval']['default']['length']);

        // Interval / Units (Available units id enabled)
        if (! empty($netObjectConfig['interval']['enabled'])) {
            $this->assertArrayHasKey('units', $netObjectConfig['interval']);
            $this->assertIsArray($netObjectConfig['interval']['units']);
            $this->assertNotEmpty($netObjectConfig['interval']['units']);

            foreach ($netObjectConfig['interval']['units'] as $unit) {
                $this->assertIsString($unit);
            }
        }

        // Feed / Status / Default
        $this->assertArrayHasKey('default', $netObjectConfig['feed']['status']);
        $this->assertIsString($netObjectConfig['feed']['status']['default']);

        // Feed / Status / Options (Available states if enabled)
        if (! empty($netObjectConfig['feed']['status']['enabled'])) {
            $this->assertArrayHasKey('options', $netObjectConfig['feed']['status']);
            $this->assertNotEmpty($netObjectConfig['feed']['status']['options']);

            foreach ($netObjectConfig['feed']['status']['options'] as $option) {
                $this->assertIsString($option);
            }
        }
    }
}
