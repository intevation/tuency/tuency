import store from "@/store";
import { computed } from "vue";
/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2020 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2020 Intevation GmbH <https://intevation.de>
 *
 * Author(s):
 * * Fadi Abbud <fadi.abbud@intevation.de>
 */

// Set the field icon according to the field name
const getFieldIcon = item => {
  let icon = "mdi-";
  switch (true) {
    case ["first_name", "last_name", "label"].indexOf(item) !== -1: {
      icon += "account";
      break;
    }
    case item === "email": {
      icon += "email";
      break;
    }
    case item === "phone": {
      icon += "phone";
      break;
    }
    case item === "pdf": {
      icon += "pdf";
      break;
    }
    default: {
      icon = "";
    }
  }
  return icon;
};

// Set rules of field according to the field name
const getFieldRule = item => {
  const rules: { (string, Number): boolean | string }[] = [];
  rules.push(v => (item.required && !v ? item.label + " is required" : true));
  switch (true) {
    case item.type === "phone": {
      rules.push(v =>
        v && v.length && !/^\d{2,3}\d{3,4}\d{5,}/.test(v)
          ? "Should match this format: +<country code><prefix><number>"
          : true
      );
      break;
    }
    case item.name === "email": {
      rules.push(v =>
        v && v.length && !/.+@.+\..+/.test(v) ? "E-mail must be valid" : true
      );
      break;
    }
    case item.name === "endpoint": {
      rules.push(v =>
        v &&
        v.length &&
        // eslint-disable-next-line no-useless-escape
        !/^\D[\w\+\.\-]*:[\/\/]?[\/\.\w\[\]\?@:,;+&-=#%~]+/.test(v)
          ? "URI must be valid"
          : true
      );
      break;
    }
  }
  return rules;
};

// Form the title of the contact depending on the existence of
// first/last-name as they have higher priority otherwise use the "label"
const getTitle = item => {
  let title = "";
  if (item.first_name) {
    title += item.first_name;
  }
  if (item.last_name) {
    title += " " + item.last_name;
  }
  if (item.label && !item.last_name && !item.first_name) {
    title = item.label;
  }
  if (! title && item.email) {
    title = item.email;
  }

  return title;
};

const mapRoles = item => {
  return JSON.parse(item).join(", ");
};
// eslint-disable-next-line @typescript-eslint/no-explicit-any
const storeState = store.state as any;
//const store = useStore();
const roles = computed(() => {
  return storeState.application.contactRoles;
});

// Get the used role(s) of a given contact
const getUsedRoles = item => {
  const usedRoles: object[] = [];
  const itemRoles = JSON.parse(item ? item.roles : "");
  itemRoles.forEach(itemRole => {
    roles.value.forEach(role => {
      if (role.name === itemRole) {
        usedRoles.push(role);
      }
    });
  });
  return usedRoles;
};

const getRoleRestrictions = item => {
  return JSON.parse(item ? item.restrict_to_client_roles : "");
}

type FieldType = { [key: string]: string };

// Get the corresponding fields of given role(s)
const getContactFields = roles => {
  const fields: FieldType[] = [];
  if (roles && roles.length) {
    roles.forEach(r => {
      r.fields.forEach(f => {
        const index = fields.map(cf => cf["name"]).indexOf(f.name);
        if (index === -1) {
          fields.push(f);
        } else {
          // Logical union check for "reqruired" field
          if (!fields[index]["required"] && f["required"]) {
            fields[index] = f;
          }
        }
      });
    });
  }
  return fields;
};

export {
  getFieldIcon,
  getFieldRule,
  getTitle,
  mapRoles,
  getUsedRoles,
  getContactFields,
  getRoleRestrictions
};
