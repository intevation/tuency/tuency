/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2020 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2020 Intevation GmbH <https://intevation.de>
 *
 * Author(s):
 * Fadi Abbud <fadi.abbud@intevation.de>
 */
import { ref } from "vue";
import { HTTP } from "../lib/http";
import { getFlatView } from "./use-mapper";
import { useStore } from "@/store";

export function useOrganisation() {
  const organisations = ref();
  const hasOrgaLoadingError = ref(false);
  const isOrgLoading = ref(false);
  const totalOrganisations = ref(0);
  const selectedOrga = ref("");
  const store = useStore();
  // Load Organisations
  const getOrganisations = (
    page?: number,
    row?: number,
    name?: string,
    sortDirection?: string,
    tag?: number,
    organisationId?: number,
    tenant?: number
  ) => {
    return new Promise<void>((resolve, reject) => {
      isOrgLoading.value = true;
      let query = "";
      if (store.state.user.selectedNode.level === "orgaadmin") {
        query = "&ancestor=" + store.state.user.selectedNode.node_id;
      }
      if (store.state.user.selectedNode.level === "tenantadmin") {
        query = "&tenant=" + store.state.user.selectedNode.node_id;
      }
      if (page) {
        query +=
          "&page=" +
          page +
          "&row=" +
          (row ? row : 10) +
          "&sort_direction=" +
          (sortDirection ? sortDirection : "asc") +
          (name ? "&name=" + name : "") +
          (organisationId ? "&organisation_id=" + organisationId : "") +
          (tenant ? "&tenant=" + tenant : "");
      }
      if (tag) {
        query += "&tag=" + tag;
      }
      if (query && query.includes("&")) {
        query = query.replace("&", "?");
      }
      HTTP.get(`/organisations${query}`)
        .then(response => {
          // Store the total available organisations in a variable
          // To let template handle showing of table/search-field according to it.
          if (response.data.meta) {
            totalOrganisations.value = response.data.meta.total;
          } else {
            totalOrganisations.value = 0;
          }
          organisations.value = response.data.data;
          // Reform the parents to represent flat view hierarchy
          organisations.value.forEach(o => {
            o.parents = getFlatView(o.parents);
          });
          store.commit("organisation/setOrganisations", organisations.value);
          resolve();
        })
        .catch(() => {
          hasOrgaLoadingError.value = true;
          reject();
        })
        .finally(() => {
          isOrgLoading.value = false;
        });
    });
  };

  return {
    organisations,
    hasOrgaLoadingError,
    isOrgLoading,
    totalOrganisations,
    selectedOrga,
    getOrganisations
  };
}
