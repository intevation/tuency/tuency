import Vue from "vue";

export default async function (): Promise<string> {
  await Vue.prototype.$keycloak.updateToken(70);
  return Vue.prototype.$keycloak.token as string;
}