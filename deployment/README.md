# Example Production Deployment

This documentation on a sample production deployment focuses on the configuration of the Tuency app stack such as
Keycloak realm and client configuration.  
For the sake of simplicity, the example setup is provided as a set of docker compose projects in which SSL is terminated at the reverse proxy and forwarded to the respective container via http within a separate docker network.  
All services are deployed on a single host, but the division into separate projects and their configuration should provide a deeper insight into the communication of the different services and thus create an understanding of how to deploy them in a distributed way, or even replace them with native (non-docker) installations.
If you prefer a separation of the services on different hosts and/or a completely manual setup of the services,
you are free to do so, but it is not covered in this documentation.

The Dockerfiles and the configurations in the repositories respective */docker/<client|backend>* directory can help you set up the
web server (NGINX) for the client and the backend manually.  
**NOTE**: The configuration files in */docker* are mainly optimized for local development purposes and should therefore
not be used productively without adaptation.

For further information about a manual installation and configuration of Keycloak and Postgres please refer to the
following documentations:
- [Configuring Keycloak for production](https://www.keycloak.org/server/configuration-production)
- [Postgres Documentation](https://www.postgresql.org/docs/16/index.html)

## Preparation
To run the setup you will need a server with docker (Version >= 20.10.24 / latest recommended) installed and Port 80 and 443 accessible
over a public IP.
The following requirements are based on the assumption that you will serve up to three clients:
- 6 GB RAM
- 2 Cores
- 80 GB Disc

You will also need a domain or subdomain for Keycloak and any Tuency client you want to provide.

Examples:
- login.example-domain.tld
- client-one.example-domain.tld
- (client-x.example-domain.tld)

You can either clone the entire repository to the desired host (recommended if you want to use tuency default resources such as theming/logos etc.) or just copy the `/deployment` directory manually.  
The next steps assume that all the conents from the folder `/deployment` are available on the server and its your current working directory.

## Deployment

The following steps assume that all preparation tasks have been completed and you are located in the `<your-tuency-path>/deployment` directory on the desired host.

### Docker networks

Create two new docker networks `tuency-proxy-nw` and `tuency-service-nw` by running the following command:

```bash
docker network create tuency-proxy-nw && docker network create tuency-service-nw
```

### Proxy

>:warning: Please make sure that the A-Records of your zone point to the public IP of the server before you start
the client or keycloak setup for the first time. Since this example uses ACME http-challange the certificate application at Let's Encryot will fail otherwise.

1. Edit the value `<your-mail>` in `./proxy/config/traefik.yml` to an address of your choice.<br>
*New (or existing) ACME-Account mail for certificate expiary warnings.*<br><br>
2. Edit the ip-whitelist middleware under `sourceRange` in `./proxy/config/dynamic/middlewares.yml`.<br>
*List of ip ranges that will be allowed to acces keycloaks master realm and admin console.<br>(Dont forget the backends IP for the account service and/or allow range of docker network tuency-proxy-nw)*<br><br>
3. Ensure the file `./proxy/ssl/acme.json` has the required mod 600.<br>
    ```bash
    chmod 600 ./proxy/ssl/acme.json
    ```

Bring up the service by running:
```bash
docker compose -f ./proxy/docker-compose.yml up -d
```

For futher investigation or troubleshooting you can check the log files under `./proxy/logs`.

### Database

Change the database and user configuration in the `Postgres` section of `./database/.env` to the values of you choice.  

Within the file `./database/docker-compose.yml` two files are mounted to the postgres containers on `/docker-entrypoint-initdb.d`. These scripts will automatically take care about creating the users and databases accoring to the `.env` configuration on the first container start.  

Additioanlly you may want to mount the postgres port to an open port of your host system for external database management. 
Make sure your firewall is setup properly before exposing any ports to the host.

Bring up the service by running one of the following command lines:
```bash
# Start the service
docker compose -f ./database/docker-compose.yml up -d

# Startup migth take a while... watch the composer logs.
docker compose -f ./database/docker-compose.yml logs -f
```

### Keycloak

Edit the following variables in `./keycloak/.env`:

| Variable                  | Description                                                                |
| ------------------------- | -------------------------------------------------------------------------- |
| PROXY_ALLOW_PUBLIC_REALM  | The tuency realm name that will be publicly accessible for authentication. |
| KEYCLOAK_URL              | You public keycloak domain. (login.example-domain.tld)                     |
| KEYCLOAK_ADMIN            | Keyclaok console admin username                                            |
| KEYCLOAK_ADMIN_PASSWORD   | Keyclaok console admin password                                            |
| KC_DB_URL_HOST            | The database hostname. (Keep the default if you havent updated the alias)  |
| KC_DB_URL_DATABASE        | The database name. (Choice of your database setup)                         |
| KC_DB_USERNAME            | The database username. (Choice of you database setup)                      |
| KC_DB_PASSWORD            | The database password. (Choice of you database setup)                      |

You might want to add you own keycloak themes.  
This can be done by copying your theme to the `./keycloak/themes` location.  
If you want to use the default tuency theme you can copy it from the repositories `/keycloak_theme` folder.

Bring up the service by running the following command(s):
```bash
# Start the service
docker compose -f ./keycloak/docker-compose.yml up -d

# Startup migth take a while... watch the composer logs.
docker compose -f ./keycloak/docker-compose.yml logs -f
```

To configure keycloak for the tuency please refer to [Setup Keycloak Tuency](./../docs/SetupKeycloakTuency.md).

### Backend

In `./backend/.env` edit the `TUENCY_BACKEND_DOCKER_IMAGE_TAG` variable to the build version of your choice.

Edit the laravel environment file under `./backend/config/laravel/.env` and apply your database, mail and keycloak credentials.  
The file was prepared for a general production use but if you want to change framework specific configurations please refer to [Laravel Environment Configuration](https://laravel.com/docs/10.x/configuration#environment-configuration).

#### App

| Variable    | Description                                                                      |
|-------------|----------------------------------------------------------------------------------|
| LOG_CHANNEL | The log channel to use. (Keep "stderr" to send errors to containers log output.) |

#### Database

| Variable                  | Description                                                                |
| ------------------------- | -------------------------------------------------------------------------- |
| DB_HOST                   | The database hostname. (Keep the default if you havent updated the alias)  |
| DB_PORT                   | The database port. (Keep the default if you havent updated the config)     |
| DB_DATABASE               | The database name. (Choice of your database setup)                         |
| DB_USERNAME               | The database username. (Choice of you database setup)                      |
| DB_PASSWORD               | The database password. (Choice of you database setup)                      |

#### Mail

| Variable                  | Description                                  |
| ------------------------- |----------------------------------------------|
| MAIL_MAILER               | The laravel mailer to use. (Default: smtp)   |
| MAIL_HOST                 | Hostname or IP of you SMTP Server            |
| MAIL_PORT                 | Mailer port. (Default: 25)                   |
| MAIL_USERNAME             | Mail username.                               |
| MAIL_PASSWORD             | Mail password.                               |
| MAIL_ENCRYPTION           | Mail encryption type.                        |
| MAIL_FROM_ADDRESS         | Mail sender address.                         |
| MAIL_FROM_NAME            | Mail sender name. (Default: APP_NAME=Teuncy) |


#### Keycloak Auth / Guard

| Variable                            | Description                                                                |
| ----------------------------------- | -------------------------------------------------------------------------- |
| KEYCLOAK_REALM_PUBLIC_KEY           | Your keycloak realm public key. (JWT signing key)                          |
| KEYCLOAK_ALLOWED_RESOURCES          | Keycloak clients to accept. (Your keycloak tenant client names)            |

#### Keycloak Service Account

| Variable                            | Description                                                                |
| ----------------------------------- | -------------------------------------------------------------------------- |
| KEYCLOAK_ACCOUNT_MGMT_URL           | Your keycloak url. (Exmaple: login.example-domain.tld)                     |
| KEYCLOAK_ACCOUNT_MGMT_REALM         | Your tuency keycloak realm. (tuency)                                       |
| KEYCLOAK_ACCOUNT_MGMT_CLIENT_ID     | Your tuency service account client id. (tuency_client)                     |
| KEYCLOAK_ACCOUNT_MGMT_CLIENT_SECRET | Your tuency service account client secret.                                 |

#### Tuency
| Variable                           | Description                                                                |
|------------------------------------|----------------------------------------------------------------------------|
| ADMIN_NOTIFICATION_MAIL            | Global admin notification mail address.                                    |
| INTELMQ_QUERY_TOKEN                | Bearer-Token for accessing the intelmq api interface.                      |
| INTELMQ_QUERY_INCLUDE_RIPE         | Inlude local ripe data in lookup queries. (Default: false)                 |
| REMINDER_EXECUTION_CRON            | When and how often the contact expiry check is executed as cron schedule.  |
| MAIL_CONTACT_EXPIRE_INTERVAL       | Day interval when contacts need to be updated. (First expiry notification) | 
| MAIL_CONTACT_NOTIFICATION_INTERVAL | Day interval when contact expiry notifications are resent.                 |

#### Audit Log
| Variable            | Description                                                                             |
|---------------------|-----------------------------------------------------------------------------------------|
| AUDITING_ENABLED    | Enables/disables the laravel audit database log.                                        |
| AUDITING_THRESHOLD  | Amount of records to keep per entity in "audits" table. (Removes the elders if reached) |

Start the backend service using the following command:
```bash
docker compose -f ./backend/docker-compose.yml up -d
```

After the service has started we need to generate an application key and apply it to Laravel's .env file.  
Furthermore, the currently empty app database must be migrated initially.

1. Generate a new application key and apply it to the `APP_KEY` variable in `./backend/config/laravel/.env`:<br>
    ```bash
        # Creates and displays a new application key.
        docker compose -f ./backend/docker-compose.yml exec backend bash -c \
        "php artisan key:generate --show"
   
        # To edit Laravel's .env file, use an editor of your choice.
        # Example: vi
        vi ./backend/config/laravel/.env
    ```

2. After the `APP_KEY` variable has been updated, the container must be restarted:<br>
    ```bash
        docker compose -f ./backend/docker-compose.yml up -d
    ```

3. Migrate the database:
    ```bash
        # Since the app is in production mode you will have
        # to confirm the execution of this command.
        docker compose -f ./backend/docker-compose.yml exec -u www-data backend bash -c \
        "php artisan migrate"
    ```

The backend is now prepared with an empty database and you can add your tenants in the `tenant` table.

### Client

The current setup shows the configuration for one client instance.  
As you may have noticed the config, resource and environment files are prefixed or tagged with `client-one`, `%ONE%` or `%-one%`.
You can rename these values to your tanant/client name for a better readability.
If you already want to serve more than one tenant you can copy and adapt the service configuration as described in `./client/docker-compose.yml`.

1. Edit the `TUENCY_CLIENT_DOCKER_IMAGE_TAG` and `CLIENT_ONE_URL` variable `./client/.env` and set it to the tuency build version and client url of your choice.<br>
   <br>
2. In `./client/config/nginx/client-one.tuency.conf` set the `server_name` to the same url. Further replace `login.example-domain.tld` within the CSP header for `connect-src` and `frame-src` with the domain of your keycloak instance.<br>
   <br>
3. Edit `./client/resources/client-one/client-config.json` and apply your keyclaok-client and tuency-client (Frontend) configuration.<br>
   <br>
4. You can now add you styling and logos to the resource folder `./client/resources/client-one`. For detailed options please check the repositories `/docs/examples/tuency_resources` folder as reference.

Start the client service(s) using the following command:

```bash
docker compose -f ./client/docker-compose.yml up -d
```

## Tuency app configuration
In `./backend/config/tuency.php` you can find a sample tuency app configuration that is mounted
as secret into the backend container.  
For more details about the configuration options, please read the comments above the respective section.  
Keys that are already assigned to an environment variable (`env('MY_ENV_VAR')`) should only be touched with caution  
since they are meant to be resolved over the .env file.

> :warning: You might want to remove the secret mount in `./backend/docker-compose.yml` if you deploy
> a preconfigured version of this file on your own.

### Adding constituencies

#### Common

A complete constituency requires a keycloak client whose root and redirect urls point to the public url of the client/constituency.  
In the database table `tenant`, an entry is required for each constituency whose value for `name` is matched to the respective logo in `/resources/logo/logo-<tenant-name>.svg`.  
The keycloak configuration for the client instance has to be present under `/resources/client-config.json` path and the main constituency logo under `/resources/logo.png`.  
For a more detailed information about the client configuration and theming please see the [client readme](../client/README.md#configuration).  

#### Step by step

- [Create keycloak client](../docs/SetupKeycloakTuency.md#tenant-clients-frontend-authentification).
- Add database entry to the tenant table.
- [Add a client server](#client).
- Put a small svg icon with a corresponding filename. ([client readme](../client/README.md#configuration))

The backend caches the entries in the client table for a few hours, which is why a manual cache clear should be performed after changes.  
This can be done with the following command:

```bash
# Manually clearing the laravel cache
docker compose -f ./backend/docker-compose.yml exec -u www-data backend bash -c "php artisan cache:clear"
```

#### Configure contact roles
Within the sections `contact_fields`, `contact_roles` and `tenant_contact_roles` in `./backend/config/tuency.php` you  
can specify the available contact roles, the respective contact fields and the availability of a role per tenant.  
Declared roles can either be available for all tenants or just specific ones.

#### Create mail templates

To send mail notifications customized to each constituency, add a new mail template in `backend/resources/views/emails`.  
The application will pick the template using the constituency/tenant name,
For example a reminder mail from constituency named `exampleconstituency`, the template `configreminder_exampleconstituency.blade.php` will be used.

## Updating

If you want to update to a higher release version please always follow additional update steps in the [News.md](../NEWS.md).  
A basic update of the client and backend containers can be performed by editing the `TUENCY_<BACKEND|CLIENT>_DOCKER_IMAGE_TAG` within the respective compose setup and running the following commands:

```bash
# Pull client/backend
docker compose -f <client-|backend-path>/docker-compose.yml pull

# Update client/backend
docker compose -f <client-|backend-path>/docker-compose.yml up -d
```
