# Based on https://laravel-news.com/laravel-scheduler-queue-docker
# Keeps tasks' output in docker logs, except for empty runs.
set -e

while [ true ]
do
    php /var/www/artisan schedule:run --no-interaction | grep "\S" | grep -v "No scheduled commands are ready to run" &
    sleep 60
done