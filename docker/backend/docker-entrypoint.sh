#!/bin/bash

if [ -v DISABLE_OPCACHE ] && [ "$DISABLE_OPCACHE" = true ];
then
    echo "Disabling opcache..."
    sed -i "s/\(opcache.enable_cli *= *\).*/\10/" /usr/local/etc/php/conf.d/20-opcache.ini
    sed -i "s/\(opcache.enable *= *\).*/\10/" /usr/local/etc/php/conf.d/20-opcache.ini
fi

exec "$@"
