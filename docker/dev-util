#!/bin/bash

######################################################################################################################
# Variables
######################################################################################################################
# Docker compose settings
DOCKER_COMPOSE="docker compose"
DOCKER_COMPOSE_FILE="docker-compose.dev.yml"
DOCKER_COMPOSE_ENV=".env.example"

# Container settings
CONTAINER_USER=www-data

SERVICE_BACKEND=backend
SERVICE_CLIENT=client
SERVICE_KEYCLOAK=keycloak
SERVICE_DATABASE=database

INSTALL_DIR_BACKEND=./backend
INSTALL_DIR_CLIENT=./client

# RIPE Importer
RIPE_IMPORTER_GIT_URL=https://github.com/certat/intelmq-certbund-contact/archive/refs/heads/wip-import-routes.zip
RIPE_IMPORTER_DATA_DIR=/tmp/ripe_data
RIPE_IMPORTER_RESTRICT_TO_COUNTRY=AT

# Parameters #########################################################################################################
COMMAND=$1
ARGSAT2="${@:2}"

######################################################################################################################
# Functions
######################################################################################################################

# Helper #############################################################################################################

# Docker compose v1 fallback.
if ! command -v docker compose &> /dev/null
then
    echo -e "\n\e[93mCould not find 'docker compose' command. Please consider updating your docker engine." \
    "\nTrying 'docker-compose'.\e[0m"
    DOCKER_COMPOSE="docker-compose"
fi

DOCKER_COMPOSE="${DOCKER_COMPOSE}"

print_help() {
    echo -e "\n \e[36mUsage: ./docker/dev-util <command>\e[0m"

    echo -e "\n\t\e[36mMain commands:\e[0m"
    echo -e "\t + \e[36mbuild\e[0m"
    echo -e "\t + \e[36msetup\e[0m"
    echo -e "\t + \e[36mtest\e[0m"
    echo -e "\t + \e[36mhelp\e[0m\n"
    echo -e "\t\e[36mDev commands:\e[0m"
    echo -e "\t + \e[36martisan <arguments>\e[0m"
    echo -e "\t + \e[36mcomposer <arguments>\e[0m"
    echo -e "\t + \e[36mnpm <arguments>\e[0m"
    echo -e "\t + \e[36myarn <arguments>\e[0m\n"
    echo -e "\t\e[36mHelper commands:\e[0m"
    echo -e "\t + \e[36mripe-import\e[0m\n"
}

install_ripe_importer() {
  echo -e "\n> \e[32mInstalling ripe importer and dependencies...\e[0m"
  $DOCKER_COMPOSE exec ${SERVICE_DATABASE} bash -c "apt update && \
  apt install -y curl unzip python3 python3-setuptools python3-psycopg2 && \
  curl -o /tmp/ripe-importer.zip -OL ${RIPE_IMPORTER_GIT_URL} && \
  unzip /tmp/ripe-importer.zip -d /tmp && \
  cd /tmp/intelmq-certbund-contact-wip-import-routes && \
  python3 setup.py install"
}

import_ripe_data() {
  echo -e "\n> \e[32mDownloading latest ripe data...\e[0m"
  $DOCKER_COMPOSE exec ${SERVICE_DATABASE} bash -c "mkdir ${RIPE_IMPORTER_DATA_DIR} && \
  cd ${RIPE_IMPORTER_DATA_DIR} && \
  ripe_download && \
  mv ./$(date +%F)/* . && rm -r ./$(date +%F)"

  echo -e "\n> \e[32mImporting ripe data with country restriction ${RIPE_IMPORTER_RESTRICT_TO_COUNTRY}...\e[0m"
  $DOCKER_COMPOSE exec ${SERVICE_DATABASE} bash -c "cd ${RIPE_IMPORTER_DATA_DIR} && \
  ripe_import --conninfo 'host=localhost dbname=tuency' \
  --organisation-file=/tmp/ripe_data/ripe.db.organisation.gz \
  --role-file=/tmp/ripe_data/ripe.db.role.gz \
  --asn-file=/tmp/ripe_data/ripe.db.aut-num.gz \
  --ripe-delegated-file=/tmp/ripe_data/delegated-ripencc-latest \
  --restrict-to-country ${RIPE_IMPORTER_RESTRICT_TO_COUNTRY} \
  --verbose"
}

publish_tuency_realm_public_key_to_backend_env() {
    KC_REALM_PUB_KEY=$($DOCKER_COMPOSE exec -u ${CONTAINER_USER} ${SERVICE_BACKEND} bash -c \
    "curl -s keycloak/realms/tuency | grep -o '\"public_key\":\"[^\"]*' | grep -o '[^\"]*$'")

    sed -i -r "s#^(KEYCLOAK_REALM_PUBLIC_KEY=).*#\1'$KC_REALM_PUB_KEY'#" ./backend/.env
}

# Checks #############################################################################################################
pre_setup_check() {
    # Check for existing setup
    if [ -d ./${INSTALL_DIR_BACKEND}/vendor ] || [ -d ./${INSTALL_DIR_CLIENT}/node_modules ]
    then
        if [ "$ARGSAT2" != "--force" ]
        then
            echo -e "\n\n\e[91mSetup detected!\nDo you really want to re-run and remove all data? (y|n) [n]?\e[0m"
            while true; do
                read CONTINUE
                CONTINUE=${CONTINUE:-n}
                case $CONTINUE in
                        [y]* ) break;;
                        [n]* ) echo -e "\e[91mStopping setup on your behalf.\e[0m"; exit 0;;
                        * ) echo -e "\e[91mPlease answer (y)es or (n)o.\e[0m\n"
                esac
            done
        fi

        echo -e "\n> \e[32mRemoving database, storage and installation files.\e[0m"
        $DOCKER_COMPOSE down -v
        rm -rf ./${INSTALL_DIR_BACKEND}/vendor ./${INSTALL_DIR_CLIENT}/node_modules
    fi

    # Prepare and source environment file
    echo -e "\n> \e[32mPrepare and docker-compose environment...\e[0m"
    cp $DOCKER_COMPOSE_FILE docker-compose.yml
    cp $DOCKER_COMPOSE_ENV .env
    source .env

    # Prepare backend and client environment fiels
    echo -e "\n> \e[32mPrepare and client and backend environment...\e[0m"
    cp $INSTALL_DIR_BACKEND/.env.example $INSTALL_DIR_BACKEND/.env
    cp $INSTALL_DIR_CLIENT/.env.example $INSTALL_DIR_CLIENT/.env
}

# Main commands ######################################################################################################
run_build() {
  $DOCKER_COMPOSE build
}

run_setup() {
    # Bring up the project
    echo -e "\n> \e[32mStarting compose project...\e[0m"
    $DOCKER_COMPOSE up -d

    # Updating file permissions
    echo -e "\n> \e[32mUpdating file permissions...\e[0m"
    $DOCKER_COMPOSE exec ${SERVICE_BACKEND} bash -c "chown -R www-data:www-data ."
    $DOCKER_COMPOSE exec ${SERVICE_CLIENT} bash -c "chown -R www-data:www-data /var/www"

    # Composer install
    echo -e "\n> \e[32mInstalling composer vendors...\e[0m"
    $DOCKER_COMPOSE exec -u ${CONTAINER_USER} ${SERVICE_BACKEND} bash -c "composer install"

    # Laravel app key
    echo -e "\n> \e[32mCreating new application key...\e[0m"
    $DOCKER_COMPOSE exec -u ${CONTAINER_USER} ${SERVICE_BACKEND} bash -c "php artisan key:generate"

    # Migrations & Seeder
    echo -e "\n> \e[32mMigrations and Seeder...\e[0m"
    $DOCKER_COMPOSE exec -u ${CONTAINER_USER} ${SERVICE_BACKEND} bash -c "php artisan migrate:fresh --seed"

    # Yarn install and build
    echo -e "\n> \e[32mInstall and build frontend...\e[0m"
    $DOCKER_COMPOSE exec -u ${CONTAINER_USER} ${SERVICE_CLIENT} bash -c "yarn install && yarn build"

    echo -e "\n> \e[32mSeeding keycloak clients...\e[0m"
    $DOCKER_COMPOSE cp ./docker/keycloak ${SERVICE_KEYCLOAK}:/opt/keycloak/setup
    $DOCKER_COMPOSE exec ${SERVICE_KEYCLOAK} bash -c "/opt/keycloak/setup/init-realm.sh"

    echo -e "\n> \e[32mPublishing tuency realms public jwt singing key to backend .env...\e[0m"
    publish_tuency_realm_public_key_to_backend_env

    echo -e "\n> \e[32mMake sure caches and configuration is cleared...\e[0m"
    $DOCKER_COMPOSE exec -u ${CONTAINER_USER} ${SERVICE_BACKEND} bash \
    -c "php artisan cache:clear && php artisan config:clear"

    # Print success message.
    echo -e "\n\e[32mDone.\e[0m"
}

run_test_setup() {
    # Overwrite default compose file
    DOCKER_COMPOSE_FILE="docker-compose.ci-tests.yml"

    # Prepare and source environment file
    echo -e "\n> \e[32mPrepare and docker-compose environment...\e[0m"
    cp $DOCKER_COMPOSE_FILE docker-compose.yml
    cp $DOCKER_COMPOSE_ENV .env
    source .env

    # Prepare backend and client environment fiels
    echo -e "\n> \e[32mPrepare and client and backend environment...\e[0m"
    cp $INSTALL_DIR_BACKEND/.env.example $INSTALL_DIR_BACKEND/.env
    cp $INSTALL_DIR_CLIENT/.env.example $INSTALL_DIR_CLIENT/.env

    # Bring up the project
    echo -e "\n> \e[32mStarting tuency services...\e[0m"
    $DOCKER_COMPOSE pull
    $DOCKER_COMPOSE up -d proxy keycloak backend client mailhog

    # Updating file permissions
    echo -e "\n> \e[32mUpdating file permissions...\e[0m"
    $DOCKER_COMPOSE exec ${SERVICE_BACKEND} bash -c "chown -R www-data:www-data ."
    $DOCKER_COMPOSE exec ${SERVICE_CLIENT} bash -c "chown -R www-data:www-data /var/www"

    # Composer install
    echo -e "\n> \e[32mInstalling composer vendors (missing dev dependencies)...\e[0m"
    $DOCKER_COMPOSE exec -u ${CONTAINER_USER} ${SERVICE_BACKEND} bash -c "composer install"

    # Laravel app key
    echo -e "\n> \e[32mCreating new application key...\e[0m"
    $DOCKER_COMPOSE exec -u ${CONTAINER_USER} ${SERVICE_BACKEND} bash -c "php artisan key:generate"

    # Migrations & Seeder
    echo -e "\n> \e[32mMigrations and Seeder...\e[0m"
    $DOCKER_COMPOSE exec -u ${CONTAINER_USER} ${SERVICE_BACKEND} bash -c "php artisan migrate:fresh --seed"

    echo -e "\n> \e[32mSeeding keycloak clients...\e[0m"
    $DOCKER_COMPOSE cp ./docker/keycloak ${SERVICE_KEYCLOAK}:/opt/keycloak/setup
    $DOCKER_COMPOSE exec ${SERVICE_KEYCLOAK} bash -c "/opt/keycloak/setup/init-realm.sh"

    echo -e "\n> \e[32mPublishing tuency realms public jwt singing key to backend .env...\e[0m"
    publish_tuency_realm_public_key_to_backend_env

    echo -e "\n> \e[32mMake sure caches and configuration is cleared...\e[0m"
    $DOCKER_COMPOSE exec -u ${CONTAINER_USER} ${SERVICE_BACKEND} bash \
    -c "php artisan cache:clear && php artisan config:clear"

    # Print success message.
    echo -e "\n\e[32mDone.\e[0m"
}

run_ripe_import() {
  echo -e "\n\n\e[91mImporting ripe data is a RAM-heavy operation and can take several minutes. Proceed (y|n) [n]?\e[0m"
  while true; do
      read CONTINUE
      CONTINUE=${CONTINUE:-n}
      case $CONTINUE in
              [y]* ) break;;
              [n]* ) echo -e "\e[91mStopping import on your behalf.\e[0m"; exit 0;;
              * ) echo -e "\e[91mPlease answer (y)es or (n)o.\e[0m\n"
      esac
  done

  install_ripe_importer
  import_ripe_data
}

######################################################################################################################
# Command Line
######################################################################################################################
if [ "$COMMAND" == "build" ]
then
  run_build
elif [ "$COMMAND" == "setup" ]
then
    if [ "$ARGSAT2" == "ci-tests" ]
    then
        run_test_setup
    else
        pre_setup_check
        run_setup
    fi
elif [ "$COMMAND" == "composer" ]
then
    $DOCKER_COMPOSE exec -u ${CONTAINER_USER} ${SERVICE_BACKEND} bash -c "composer ${ARGSAT2}"
elif [ "$COMMAND" == "artisan" ] || [ "$COMMAND" == "art" ]
then
    $DOCKER_COMPOSE exec -u ${CONTAINER_USER} ${SERVICE_BACKEND} bash -c "php artisan ${ARGSAT2}"
elif [ "$COMMAND" == "npm" ]
then
    $DOCKER_COMPOSE exec -u ${CONTAINER_USER} ${SERVICE_CLIENT} bash -c "npm ${ARGSAT2}"
elif [ "$COMMAND" == "yarn" ]
then
    $DOCKER_COMPOSE exec -u ${CONTAINER_USER} ${SERVICE_CLIENT} bash -c "yarn ${ARGSAT2}"
elif [ "$COMMAND" == "test" ]
then
    $DOCKER_COMPOSE exec -u ${CONTAINER_USER} ${SERVICE_BACKEND} bash -c "php artisan test --coverage ${ARGSAT2}"
elif [ "$COMMAND" == "ripe-import" ]
then
    run_ripe_import
else
    print_help
fi
