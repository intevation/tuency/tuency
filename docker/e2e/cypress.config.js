const { defineConfig } = require("cypress");

module.exports = defineConfig({
  viewportWidth: 1980,
  viewportHeight: 1080,
  downloadsFolder: 'cypress/downloads',
  defaultCommandTimeout: 10000,
  injectDocumentDomain: true,
  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
      on('before:browser:launch', (browser = {}, launchOptions) => {
        const defaultDownloadDir = config['downloadsFolder'].replace(/\\/g, '\\\\');
        if (browser.name === 'chrome') {
          launchOptions.preferences.default.download = {
            default_directory: defaultDownloadDir,
          }
        }
        if (browser.name === 'firefox') {
          launchOptions.preferences['browser.download.dir'] = defaultDownloadDir;
        }
        return launchOptions;
      });
    },
  },

  env: {
    clientUrl: 'http://one.tuency.localhost',
    keycloakUrl: 'http://keycloak.tuency.localhost',
    adminUsername: 'tuency-admin',
    adminPassword: 'password'
  },
});