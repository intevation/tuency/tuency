let userData;
let asn;

before('Load users.', () => {
    cy.fixture('users').then((users) => {
        userData = users;
    });
});

afterEach('Logout.', () => {
    cy.logout();
});

describe('Portaladmin in "Autonomous System Numbers" tab can', () => {

    beforeEach('Login as portal admin.', () => {
        cy.login(userData.portaladmin.username, userData.portaladmin.password);
    });
    
    it('create asn netobject that is auto-approved', () => {
        cy.clickNavItem('Autonomous Systems');
        
        asn = Math.floor(Math.random() * (10000 - 1) + 1).toString();
        cy.createNewNetobjectForRandomOrga(asn, 'approved');
    });

    it('apply asn netobject for approval again', () => {
        cy.clickNavItem('Autonomous Systems');
        cy.reapplyForClaimResolution(asn, '.mdi-backup-restore');
    });

    it('approve asn netobject', () => {
        cy.clickNavItem('Claim Resolution');
        cy.resolveNetobject(asn, true);
    });

    it('delete asn netobject', () => {
        cy.clickNavItem('Autonomous Systems');
        cy.deleteNetobject(asn);
    });
});