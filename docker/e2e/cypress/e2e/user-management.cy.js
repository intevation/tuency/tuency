let userData;
let tenantData;

before('Load users.', () => {
    cy.fixture('users').then((users) => {
        userData = users;
    });

    cy.fixture('tenants').then((tenants) => {
        tenantData = tenants;
    });
});

afterEach('Logout.', () => {
    cy.logout();
});
describe('Portaladmin on external Keyloak service can', () => {
    beforeEach('Login as portal admin.', () => {
        cy.login(userData.portaladmin.username, userData.portaladmin.password);
    });

    it('edit his profile data in external Keycloak site', () => {
        cy.get('.v-app-bar--fixed button:last-child').click();
        cy.get('.menuable__content__active div:first-child').contains('Profile').click();
        cy.get('main').contains('Personal info');

        cy.fixture('users').then((users) => {
            cy.log('USERS', users);
            cy.log('ADMIN', users.portaladmin);
            cy.get('#email').clear().type(users.portaladmin.email);
            cy.get('#firstName').clear().type(users.portaladmin.firstName);
            cy.get('#lastName').clear().type(users.portaladmin.lastName);
        });

        cy.get('#save-btn').click();
        cy.get('[data-testid="alerts"]').contains('Your account has been updated.');

        cy.get('header').get('[data-testid="referrer-link"]').contains('Back to').click();
        cy.get('.v-main').contains('Manage Netobjects');
        cy.get('#netobjectId table').should('be.visible');
        cy.get('table .v-progress-linear--visible').should('not.exist');
    });
});

describe('Portaladmin in "Organisation Management" tab can', () => {
    beforeEach('Login as portal admin.', () => {
        cy.login(userData.portaladmin.username, userData.portaladmin.password);
    });

    it('invite a colleague with same role in "Organisation Management" tab', () => {
        cy.clickNavItem('Add colleague', 'Organisation Management');

        const colleagueMail = `${Date.now()}@cypress.test`;

        cy.get('form').within(($inviteColleagueForm) => {
            cy.get('input').type(colleagueMail);
            cy.get('button').contains('Invite').click();
        });

        cy.get('div [role="list"]').contains(colleagueMail);

        cy.clickNavItem('My node', 'My Node');

        cy.get('table tbody').contains(colleagueMail).parent().get('td i').should('have.class', 'mdi-alpha-p-box-outline');
    });
});

describe('Portaladmin in tab "My Node" can', () => {
    beforeEach('Login as portal admin.', () => {
        cy.login(userData.portaladmin.username, userData.portaladmin.password);
    });

    it('invite a tenantadmin', () => {
        cy.inviteUser(userData.tenantadmin.email, 'tenantadmin', tenantData.cert_mrk.name);
    });

    it('update a tenantadmin', () => {
        cy.clickNavItem('My node', 'My Node');

        cy.clickTableRowActionIcon(userData.tenantadmin.email, '.mdi-pencil');

        cy.get('.v-dialog--active').within(($userEditForm) => {
            cy.get('.v-card').contains('Edit ' + userData.tenantadmin.email);
            cy.get('.v-progress-linear--visible').should('not.exist');
            cy.get('input:nth-child(1)').click();
        });

        // Apply the second tenant to the tenant admin.
        cy.get('[role="listbox"]').contains(tenantData.h20cert_mrk.name).click();

        cy.get('.v-dialog .v-btn:last-child').contains('save').click();

        cy.get('table .v-progress-linear__indeterminate--active').should('not.exist');
    });

    it('promote a tenantadmin to portaladmin', () => {
        cy.clickNavItem('My node', 'My Node');

        cy.clickTableRowActionIcon(userData.tenantadmin.email, '.mdi-pencil');

        cy.get('.v-dialog').within(($userEditForm) => {
            cy.get('.v-card').contains('Edit ' + userData.tenantadmin.email);
            cy.get('.v-progress-linear--visible').should('not.exist');
            cy.get('button:first-child').contains('Promote to Portal-Admin').click();
        });

        cy.get('.v-dialog .v-btn:last-child').contains('save').click();
        cy.get('.v-card__actions').should('contain.text', 'Please Confirm/Cancel the following change(s):');
        cy.get('.v-dialog .v-btn:last-child').contains('Confirm').click();

        cy.get('table tbody').contains(userData.tenantadmin.email).parent().get('td i').should('have.class', 'mdi-alpha-p-box-outline');
    });

    it('invite an orgaadmin', () => {
        cy.inviteUser(userData.orgaadmin.email, 'orgaadmin', tenantData.cert_mrk.name);
    });

    it('update an orgaadmin', () => {
        cy.clickNavItem('My node', 'My Node');

        cy.clickTableRowActionIcon(userData.orgaadmin.email, '.mdi-pencil');

        cy.get('.v-dialog').within(($userEditForm) => {
            cy.get('.v-card').contains('Edit ' + userData.orgaadmin.email);
            cy.get('.v-progress-linear--visible').should('not.exist');
            cy.get('input:nth-child(2)').click();
        });

        // Apply another random organisation.
        const orgaEntry = Math.floor(Math.random() * (10 - 1) + 1);
        cy.get('[role="listbox"] .v-list-item:nth-child(' + orgaEntry + ')').click();

        cy.get('.v-dialog .v-btn:last-child').contains('save').click();
        cy.get('.v-dialog--active').should('not.exist');
        cy.get('table .v-progress-linear__indeterminate--active').should('not.exist');
    });

    it('delete previous created orga- and tenantadmin', () => {
        cy.clickNavItem('My node', 'My Node');

        cy.clickTableRowActionIcon(userData.orgaadmin.email, '.mdi-delete');
        cy.get('.v-dialog .v-btn:last-child').contains('yes').click();
        cy.get('table tbody').contains(userData.orgaadmin.email).should('not.exist');

        cy.clickTableRowActionIcon(userData.tenantadmin.email, '.mdi-delete');
        cy.get('.v-dialog .v-btn:last-child').contains('yes').click();
        cy.get('table tbody').contains(userData.tenantadmin.email).should('not.exist');
    });
});