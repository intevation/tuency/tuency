/**
 * Netobject - Commands
 */

Cypress.Commands.add('clickNewNetobject', (createButtonText = 'New Netobject') => {
    cy.get('#netobjectId .row button').contains(createButtonText).click();
});

Cypress.Commands.add('typeNetobjectSearch', (searchText) => {
    cy.get('#netobjectId .v-text-field').contains('Search for Netobject(s)').parent().type(searchText);
});

Cypress.Commands.add('createNewNetobjectForRandomOrga', (netobjectIdentifier, expectedApprovalState = 'approved') => {

    if (netobjectIdentifier.includes('CY_RIPE')) {
        cy.clickNewNetobject('New RIPE Handle');
    } else {
        cy.clickNewNetobject();
    }

    cy.get('.v-dialog--active input[autocomplete="off"]').click();

    // Random organisation select number.
    const orgaEntry = Math.floor(Math.random() * (10 - 1) + 1);
    cy.get('[role="listbox"] .v-list-item:nth-child(' + orgaEntry + ')').click();
    cy.get('.v-dialog--active:last-child .row:last-child').within(($netobjectValueWrapper) => {
        cy.get('input').type(netobjectIdentifier);
    });

    cy.get('.v-card:last-child .v-card__actions').contains('Add').click();
    cy.get('.v-alert__content').should('contain.text', 'Netobject created successfully');
    cy.get('#netobjectId .v-progress-linear--visible').should('not.exist');
    cy.typeNetobjectSearch(netobjectIdentifier);
    cy.get('#netobjectId').within(($netobjectOverviewWrapper) => {
        cy.contains(netobjectIdentifier).parent().get('td:nth-child(4)').should('contain.text', expectedApprovalState);
    });
});

Cypress.Commands.add('clickNetobjectTableRowAction', (netobjectIdentifier, iconClass) => {
    cy.get('table tbody').contains(netobjectIdentifier).parent().parent().within(($tr) => {
        cy.get(iconClass).click();
    });
});

Cypress.Commands.add('reapplyForClaimResolution', (netobjectIdentifier) => {
    cy.clickNetobjectTableRowAction(netobjectIdentifier, '.mdi-backup-restore');

    cy.get('.v-dialog--active button:last-child').contains('yes').click();
    cy.contains(netobjectIdentifier).parent().get('td:nth-child(4)').should('contain.text', 'pending');
});

Cypress.Commands.add('resolveNetobject', (netobjectIdentifier, approve = true) => {
    cy.clickTableRowActionIcon(netobjectIdentifier, '.mdi-cogs');

    if (approve) {
        cy.get('.v-dialog--active button:last-child').contains('Approve').click();
    } else {
        cy.get('.v-dialog--active button:nth-child(3)').contains('Deny').click();
    }

    cy.get('table tbody').contains(netobjectIdentifier).should('not.exist');
});

Cypress.Commands.add('deleteNetobject', (netobjectIdentifier) => {
    cy.clickNetobjectTableRowAction(netobjectIdentifier, '.mdi-delete');

    cy.get('.v-dialog--active button:last-child').contains('yes').click();

    cy.get('table tbody').contains(netobjectIdentifier).should('not.exist');
});