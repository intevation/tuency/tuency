#!/bin/bash

######################################################################################################################
# Variables
######################################################################################################################
# Execution dir
DIR=$(dirname $(readlink -f $0))

# Keycloak admin utility script.
kc_adm=/opt/keycloak/bin/kcadm.sh

######################################################################################################################
# Init
######################################################################################################################
# Configure master realm credentials with keycloak admin user.
$kc_adm config credentials \
    --server http://localhost:"${KC_HTTP_PORT}" \
    --realm master \
    --user "${KEYCLOAK_ADMIN}" \
    --password "${KEYCLOAK_ADMIN_PASSWORD}"

# Create new realm.
$kc_adm create realms -s realm=${TUENCY_REALM} -s enabled=true -s loginTheme=tuency
# -s accountTheme=tuency // Account theme currently not working.

# Create new realms admin user.
$kc_adm create users -r ${TUENCY_REALM} -s username=${TUENCY_REALM_ADMIN} -s firstName=Admin -s lastName=Admin -s email='tuency-admin@localhost' -s enabled=true
$kc_adm set-password -r ${TUENCY_REALM} --username ${TUENCY_REALM_ADMIN} --new-password ${TUENCY_REALM_ADMIN_PASSWORD}

# Apply realm-management roles to tuency admin user.
$kc_adm add-roles -r ${TUENCY_REALM} \
    --uusername ${TUENCY_REALM_ADMIN} \
    --cclientid realm-management \
    --rolename realm-admin

# Login with new realms admin user.
$kc_adm config credentials \
    --server http://localhost:"${KC_HTTP_PORT}" \
    --realm ${TUENCY_REALM} \
    --user ${TUENCY_REALM_ADMIN} \
    --password ${TUENCY_REALM_ADMIN_PASSWORD}

# Realm Setup ########################################################################################################

# Update email settings
$kc_adm update realms/${TUENCY_REALM} -x \
    -s 'smtpServer.host=mail.internal' \
    -s 'smtpServer.from=tuency@keycloak.tuency.localhost' \
    -s 'smtpServer.fromDisplayName=KC Tuency' \
    -s 'smtpServer.auth=false' \
    -s 'smtpServer.ssl=false'

# Create clients
for clientjson in "tuency_client.json" "tuencyone.json" "tuencytwo.json" "tuencythree.json"
do
    echo "Creating tuency_client: $TUENCY_REALM->$(grep -o '"clientId": "[^"]*' "${DIR}"/clients/${clientjson} | grep -o '[^"]*$')"
    $kc_adm create clients -r $TUENCY_REALM -f "${DIR}"/clients/${clientjson}

done

# Apply role manage-users and manage-clients from realm-managament client to service account
#$SERVICE_ACCOUNT_ID=$(get users -q username=service-account-tuency_client --fields 'id' | grep -o '"id" : "[^"]*' | grep -o '[^"]*$')
$kc_adm add-roles -r ${TUENCY_REALM} \
    --uusername service-account-tuency_client \
    --cclientid realm-management \
    --rolename manage-users \
    --rolename manage-clients


# Create client roles
TUENCY_CLIENT_ID=$(grep -o '"id": "[^"]*' ${DIR}/clients/tuency_client.json | grep -o '[^"]*$')
TUENCY_ONE_ID=$(grep -o '"id": "[^"]*' ${DIR}/clients/tuencyone.json | grep -o '[^"]*$')
TUENCY_TWO_ID=$(grep -o '"id": "[^"]*' ${DIR}/clients/tuencytwo.json | grep -o '[^"]*$')
TUENCY_THREE_ID=$(grep -o '"id": "[^"]*' ${DIR}/clients/tuencythree.json | grep -o '[^"]*$')

# Create role "portaladmin" for each client and assign it to admin user.
echo "Creating and assigning portaladmin roles"
for clientid in "${TUENCY_CLIENT_ID}" "${TUENCY_ONE_ID}" "${TUENCY_TWO_ID}" "${TUENCY_THREE_ID}"
do
    # Create role.
    $kc_adm create clients/$clientid/roles -r $TUENCY_REALM -s name=portaladmin

    # Assign role to admin user.
    $kc_adm add-roles -r $TUENCY_REALM --cid $clientid --uusername ${TUENCY_REALM_ADMIN} --rolename portaladmin
done

# Create further roles tenantadmin and orgaadmin.
echo "Creating roles tenantadmin and orgaadmin"
for role in "tenantadmin" "orgaadmin";
    do
    $kc_adm create clients/"${TUENCY_CLIENT_ID}"/roles -r $TUENCY_REALM -s name=$role
done

