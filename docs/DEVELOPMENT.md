# Tuency development guide

## Setup

#### Prerequisites

The development setup is provided as docker-compose project and requires a docker installation (latest recommended), the *docker compose plugin* available and port 80 free (not occupied by another service) on your system.

For a desktop environment you might want to [install Docker Desktop](https://docs.docker.com/get-docker/) to easily manage your container over a graphical user interface.
For a server or non graphical environment your might [install Docker Engine and CLI](https://docs.docker.com/engine/install/) manually.

> :warning: The described setup process was designed and tested for UNIX-like systems. If you are working on Windows it is recommended to [install docker in WSL](https://docs.docker.com/desktop/wsl/).

#### Bring up the project

After cloning this repository change the directory into the root folder and run the following command:

````bash
# Setup development
./docker/dev-util setup
````

This builds and starts the docker compose setup and prepares the database, backend, client and keycloak.
Here you can find a [detailed description about the dev-util script](./dev-util.md) and about the installation steps.

After the script has completed the following services should have started:

- [traefik](http://traefik.tuency.localhost/) - Reverse proxy
- [backend](http://backend.tuency.localhost/) - Laravel backend
- [client](http://one.tuency.localhost/) - Tuency Client-One example
- [keycloak](http://keycloak.tuency.localhost/) - Keycloak Identity MGMT
- [maildev](http://mail.tuency.localhost/) - SMTP Mailtrap
- postgres - Database service

## Gitlab CI

### Genral
If you fork this repository to contribute to it or at least have the requirement to build the docker images on your own gitlab instace, you should consider the following things:

The configured Gitlab CI automatically builds an docker image for the client and backend and tags them with "latest" and \<commit-short-sha\> in the following scenarios:
- If a "develop" commit-tag is created
- If a push event on the master branch is triggered
- If a commit was tagged in master (relase tag)

When a commit was tagged in master (release tag) an additional docker image tag \<git-tag\> will be created.
We recommend creating release tags according to the specification of [Semantic Versioning 2.0.0](https://semver.org/).

### CI Variables
The configured Gitlab CI assumes a list of CI-Vriables aleardy exist.
These variables can be setup in your gitlab instance under ``Settings > CI/CD > Variables``
- CLIENT_ENV (Type: File) - *You might want to use the [Client .env example](./client/.env.example) as reference*
- DOCKER_IGNORE_BACKEND (Type: File) - *See [.dockerignore file](https://docs.docker.com/engine/reference/builder/#dockerignore-file) for reference*
- DOCKER_IGNORE_CLIENT (Type: File) - *See [.dockerignore file](https://docs.docker.com/engine/reference/builder/#dockerignore-file) for reference*example) as reference*

### Registry cleanup
Depending on you build frequency you might want to enable auto registry cleanup for the images tagged by the commit short-SHA.
This can be done in the gitlab repository under `Settings > Packages and registires > [Set cleanup Rules]`.

Change the switch `Enabled ` to active and select a schedule of your choice for `Run cleanup`.
In the section `Keep these tags` apply the following values:

| Field                | Value                   |
|----------------------|-------------------------|
| Keep the most recent | 100 tags per image name |
| Keep tags matching   | (v.+\|latest)           |

In the section `Remove these tags` apply the following values:

| Field                  | Value  |
|------------------------|--------|
| Remove tags older than | 7 days |
| Keep tags matching     | .*     |
