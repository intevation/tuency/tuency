# Tuency Keycloak Setup

This guide assumes that you already have a running Keycloak instance available and are logged in with your administration credentials.
The documentation is separated in different headings and sections but can be read from top to bottom for a complete tuency keycloak configuration.

## Preparation

### Realm

It is highly recommended NOT to connect the tuency app to Keycloak's master realm, but to create a separate "tuency" realm instead.

Use the realm-select on the left hand menu bar and click `Create Realm`.
If you already have a resource file available you can drag-and-drop into the upload section.
In field `Realm name` type "tuency" or a name of your choice. Please keep in mind that all examples use `tuency` as realm name and you will have to update this accordingly if you change it.
Click `Create`.

![Create the tuency realm.](./assets/kc-create-realm.png)

The next steps assume that the new tuency client has been created and is selected as current realm.

### Realm public key

The tuency backend trusts keycloak as user authority but needs the realms public key in order to decrypt the JWT's sent from an authenticated user.
To get the public key click on `Realm settings` in the left menu bar.
In the `Keys` tab locate the key entry with `Algorythm=RS256, Type=RSA, Provider=rsa-generated` and click on `Public key`.
This key is required for the backend configuration.

NOTE: The JWT signing key is also present in the JSON response of \<keycloak-url\>realms/\<realm-name\> but is not automatically fetched to avoid MitM-Attacks.

### Mail config

To set the SMTP configuration for you realm click on `Realm settings` in the left side menu bar.
In the `Email` tab you can provide your template and connection settings.

## Clients

### Tuency Cleint (Backend managament)

#### General

The tuency clients (frontend) authentication works via OIDC and trusts Keycloak as user authority.
However, the backend application requires a service account in order to create and manage users and the rolemapping within the tuency realm.
This is done over a separate client (tuency_client) that only allows service accounts to log in via client credential grant.

On the left menu bar Select `Clients` and click `Create client` on top of the client overview table.
Make sure the client type is `OpenID Connect` and set the client id to `tuency_client` or a name of your choice.
The name and description field may stay empty but setting it to something recognizable is recommended to keep a better overview.

![Client create - General settings](./assets/kc-create-client-general.png)

After clicking on `Next` you find you self in the `Capability settings`.
Enable the switches `Client authentication` and `Authorization` and disable all authentication flows but the preselected `Service accounts roles`.

![Client create - Capability settings](./assets/kc-create-client-capabilities.png)

After clicking on `Next` you can leave the `Root URL` and `Home url` settings blanc because this client wont be contacted with any flow that requires redirection.
Click on `Save` to create the client.

#### Service account

Since we allowed service account roles to this client we can now apply user managament permissions to this service account.
In the `Service accounts role` tab click on `Assign role`.
In the dialog switch the filter selector to `Filter by clients` and search for `manage-`.
Select the following roles from client `realm-managament` and Click `Assign`.

 - `manage-users`
 - `manage-clients`

![Assign user management role to service account](./assets/kc-service-account-assign-role.png)

#### Client Roles (Tuency)

Now we add the tuency role set to the client roles.
Repeat the next step for the following role names:

- portaladmin
- tenantadmin
- orgaadmin

In the clients `Roles` tab click on `Create role` and copy the respective `<rolename>` in the filed `Role name`.
The `Description` field is optional but might help other keycloak realm admins to better understand the functionality of the role.
If you have more roles to add you can click `Client details` the top breadcrumbs menu to get back to the role overview.

When all roles are added your overview should look as follows:

![Tuency client - Assign tuency roles](./assets/kc-client-assign-tuency-roles.png)

NOTE: The role [uma_protection](https://www.keycloak.org/docs/latest/authorization_services/#_service_user_managed_access) will be automatically added from keycloak when a new client is created but the underlying functionality is not active by default.
It is intended for fine-grained, user-specific resource access which is not supported by the tuency backend. The role can therefore be ignored or simply deleted.

#### Mappers (Client scope)

The tuency backend requires some additinal client mappers added to the id token.

- username
- client roles
- realm roles

To apply them select the `Client scopes` tab in your client details.
Click on `tuency_one-dedicated` or your respective `<your-client>-dedicated` on top of your scope list.
Within the mappers tab click on `Add mapper > From predefined mappers`. Select the three mappers from the list and click `Add`.
After the mappers have been added, click through each ones details and make sure the switch `Add to ID token` is enabled and the `Token Claim Name`.

![Client - Add mappers](./assets/kc-client-set-mappers.png)

Further set the `Token Claim Name` to the lowercase mapper name without spaces. (Example: `client roles` => `clientroles`)

![Client - Add mappers](./assets/kc-client-adapt-mappers.png)

#### Client secret

Save the name of the new client and the client secret in a secure location, as it will be needed later for the backend configuration. 
The client secret ist located in the `Credentials` tab `Client secret`.

### Tenant Clients (Frontend Authentification)

#### General

For each tenant you want to serve with your tuency app you will need an own client.
On the left menu bar Select `Clients` and click `Create client` on top of the client overview table.

In the general settings, make sure the client type is `OpenID Connect` and set the client id to a name of your choice.
Click on `Next`.

The settings in `Capability config` can be left as they are as long you dont wont to change something in the authentication and authorization settings.
Click on `Next`.

In the `Login settings` set the fields `Root URL`, `Valid redirect URIs` and `Web origins` to your client domain including the protocol. (Example: https://client-one.example-domain.tld)
In `Valid redirect URIs` apply a `*` at the end of your value to allow the frontend of you tenant to send any app path for "redirection after login" to the keycloak client.

![Tenant Client - Set login urls](./assets/kc-client-set-login-urls.png)

Click on `Save`.

#### Client Roles (Tuency)

For tenant clients we only have to provide the `portaladmin` role since the most part of the role resolution is done in the backends database.
Please refer to the section [Client Roles (Tuency)](#client-roles-tuency) and perform the described steps in your new tenant client only for the `portaladmin` role.

#### Themes

If you have installed your own or the tuency standard theme during the setup of the keycloak instance, this can be applied to the new client as follows.
Scroll down to `Login settings` in the `Settings` tab of your client.
In the `Login theme` select, select the theme of your choice and click `Save`.

## Users

### Add keycloak user

In the left menu bar click on `Users` and then `Add user` on top of the user overview.
Type in your desired user information and you might want to check the `Email verified` switch to skip the email link verification.
Click on `Create`.

If you want to set the password for a user manually click on the `Credentails tab` within the user details.
Depending if the user already has a password set, click either `Set Password` or `Reset Password`.
In the dialog fill in the password and confirmation. Disable the switch `Temporary` if you dont want the user to be prompted for a password reset on the next login.

### Keycloak

You might want to create a separate user than the keycloak admin to manage the tuency realm.
Please refer to [Add keycloak user](#add-keycloak-user) in order to create the new user.

Within the user details navigate to the tab `Role mapping`.
Click on `Assign role` and change the filter within the dialog to `Filter by clients`.
Search for `manage` and select the desired user role(s) from `realm-management` (Example: `manage-realm`).
Click `Assign` and the newly created user has the given permissions assigned for keycloaks admin ui.

### Tuency

#### Portal Admins

Please refer to [Add keycloak user](#add-keycloak-user) in order to create the new user.
Within the `User details` select the tab `Role mappings` and click on `Assign role`.
In the dialog switch the filter selector to `Filter by clients` and search for `portaladmin`.
Select all `portaladmin` roles from each tuency client and click `Assign`.

#### Others

Please do not create application users with other roles than `portaladmin` in the keycloak web ui.
Since the roles `tenantadmin` and `orgaadmin` currently require further settings in the database that are not yet covered on the data sync over the JWT this will lead to errors. You should therefore create them directly in the tuency app.
