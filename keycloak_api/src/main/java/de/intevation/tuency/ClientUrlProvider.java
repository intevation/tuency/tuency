package de.intevation.tuency;

import javax.json.Json;
import javax.json.JsonObject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.keycloak.models.ClientModel;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.services.resource.RealmResourceProvider;

/**
 * RealmResourceProvider providing a rest interface that can be used to obtain the root url of a KeyCloak client
 * 
 * @author Alexander Woestmann <awoestmann@intevation.de>
 */
public class ClientUrlProvider implements RealmResourceProvider {

    private KeycloakSession session;

    public ClientUrlProvider(KeycloakSession session) {
        this.session = session;
    }

    @Override
    public Object getResource() {
        return this;
    }

    /**
     * Get the root url of a KeyCloak client by id.
     *
     * Returns an error 400 if no or an invalid client id is given.
     * <br>
     * URL Parameters: <br>
     *   * clientid [String, mandatory]: KeyCloak client id to get the URL for
     * <br>
     *
     * Response format: <br>
     * <pre>
     * <code>
     * {
     *     "rootUrl": [string]
     * }
     * </code>
     * </pre>
     *
     * Request example:
     * http://myKeycloak.org/auth/realms/master/clienturlprovider/clientid=myExampleClient
     *
     * @param info URI information
     * @param request Request object
     * @return JSON Object containing the baseUrl
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(
            @Context UriInfo info,
            @Context HttpServletRequest request) {
        MultivaluedMap<String, String> params = info.getQueryParameters();

        if (!params.containsKey("clientid")) {
            return Response.status(400, "No client id given").build();
        }
        String clientId = params.getFirst("clientid");
        RealmModel realm = session.getContext().getRealm();

        ClientModel client = realm.getClientByClientId(clientId);
        if (client == null) {
            return Response.status(400, "Unknown client id").build();
        }

        JsonObject responseJson = Json.createObjectBuilder()
            .add("rootUrl", client.getRootUrl())
            .build();
        return Response.ok(responseJson).build();

    }

    @Override
    public void close() {
    }

}
